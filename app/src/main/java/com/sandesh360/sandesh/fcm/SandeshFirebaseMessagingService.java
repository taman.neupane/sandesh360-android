package com.sandesh360.sandesh.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.model.FcmNotification;
import com.sandesh360.sandesh.data.network.Endpoints;
import com.sandesh360.sandesh.ui.navdrawer.MainActivity;
import com.sandesh360.sandesh.utils.Constants;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;


public class SandeshFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = SandeshFirebaseMessagingService.class.getSimpleName();

    private static ArrayList<Long> alreadyNotifiedTimestamps = new ArrayList<>();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "onMessageReceived: " + remoteMessage.getMessageId());
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if (!isDuplicate(remoteMessage.getSentTime())) {
                FcmNotification notification = new FcmNotification()
                        .setId(remoteMessage.getData().get(Endpoints.ID))
                        .setTitle(remoteMessage.getData().get(Endpoints.TITLE))
                        .setDescription(remoteMessage.getData().get(Endpoints.DESCRIPTION))
                        .setUrl(remoteMessage.getData().get(Endpoints.IMAGE_URL))
                        .setClickAction(remoteMessage.getData().get(Endpoints.CLICK_ACTION));

                if (notification != null) {
                    sendNotification(notification);
                }
            }

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }
    // [END on_new_token]

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notification FCM message body received.
     */
    private void sendNotification(FcmNotification notification) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(Constants.BUNDLE_KEY_NOTIFICATION, notification.getId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        int uniqueInt = (int) (System.currentTimeMillis() & 0xfffffff);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueInt /* Request code */, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.layout_notification);
        notificationLayout.setTextViewText(R.id.tv_title, notification.getTitle());
        notificationLayout.setTextViewText(R.id.tv_description, notification.getDescription());
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setCustomContentView(notificationLayout)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Sandesh360",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify((int) System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());
    }

    // Workaround for Firebase duplicate pushes
    private boolean isDuplicate(long timestamp) {
        if (alreadyNotifiedTimestamps.contains(timestamp)) {
            alreadyNotifiedTimestamps.remove(timestamp);
            return true;
        } else {
            alreadyNotifiedTimestamps.add(timestamp);
        }

        return false;
    }
}