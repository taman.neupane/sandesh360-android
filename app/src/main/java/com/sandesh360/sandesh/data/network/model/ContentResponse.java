package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.NativeAds;
import com.sandesh360.sandesh.data.network.Endpoints;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mobarak on November 04, 2018
 *
 * @author Sandesh360
 */
public class ContentResponse extends BaseResponse {

    @SerializedName(Endpoints.CONTENTS)
    private List<Content> contents;

    @SerializedName(Endpoints.ADS)
    private List<NativeAds> ads;

    @SerializedName(Endpoints.NEWSBOTTOMADS)
    private List<NewsBottomAds> newsBottomAds = new ArrayList<>();

    @SerializedName(Endpoints.FREQUENCY)
    private int frequency;

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public List<NativeAds> getAds() {
        return ads;
    }

    public void setAds(List<NativeAds> ads) {
        this.ads = ads;
    }

    public List<NewsBottomAds> getNewsBottomAds() {
        return newsBottomAds;
    }

    public void setNewsBottomAds(List<NewsBottomAds> newsBottomAds) {
        this.newsBottomAds = newsBottomAds;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
}
