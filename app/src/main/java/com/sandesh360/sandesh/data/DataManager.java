package com.sandesh360.sandesh.data;

import androidx.annotation.NonNull;

import com.sandesh360.sandesh.data.model.User;
import com.sandesh360.sandesh.data.network.ApiManager;
import com.sandesh360.sandesh.data.network.IApiHelper;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.ContentRequest;
import com.sandesh360.sandesh.data.network.model.FeedbackRequest;
import com.sandesh360.sandesh.data.network.model.PollRequestBody;
import com.sandesh360.sandesh.data.network.model.ReferralRequest;
import com.sandesh360.sandesh.data.prefs.IPreferenceHelper;
import com.sandesh360.sandesh.data.prefs.PreferenceManager;

/**
 * This is singleton DataManager class and used to manipulate all kinds of data
 * Created by Mobarak on 09-Feb-18.
 *
 * @author Atom AP Ltd.
 */

public class DataManager implements IPreferenceHelper, IApiHelper {

    /**
     * IPreferenceHelper instance;
     */
    private IPreferenceHelper mPreferenceHelper;
    /**
     * IApiHelper instance;
     */
    private IApiHelper mApiHelper;
//    /**
//     * IDbHelper instance;
//     */
//    private IDbHelper mDbHelper;
    /**
     * DataManager instance;
     */
    private static DataManager instance;

    /**
     * Default contsructor
     */
    private DataManager() {
        mPreferenceHelper = PreferenceManager.getInstance();
        mApiHelper = ApiManager.getInstance();
//        mDbHelper = DbManager.getInstance();
    }

    /**
     * Invoke to get DataManager instance. If instance is null,
     * then create new object otherwise return previous one
     *
     * @return mInstance
     */
    public static DataManager getInstance() {
        if (instance == null) {
            synchronized (DataManager.class) {
                if (instance == null)
                    instance = new DataManager();
            }
        }
        return instance;
    }


    @Override
    public void setAuthToken(@NonNull String token) {
        mPreferenceHelper.setAuthToken(token);
    }

    @Override
    public String getAuthToken() {
        return mPreferenceHelper.getAuthToken();
    }

    @Override
    public void setTerminalId(@NonNull String terminalId) {
        mPreferenceHelper.setTerminalId(terminalId);
    }

    @Override
    public String getTerminalId() {
        return mPreferenceHelper.getTerminalId();
    }

    @Override
    public void setFcmToken(@NonNull String token) {
        mPreferenceHelper.setFcmToken(token);
    }

    @Override
    public String getFcmToken() {
        return mPreferenceHelper.getFcmToken();
    }

    @Override
    public void setLocalePersistData(String language) {
        mPreferenceHelper.setLocalePersistData(language);
    }

    @Override
    public String getLocalePersistedData() {
        return mPreferenceHelper.getLocalePersistedData();
    }

    @Override
    public void setUserInfo(User user, boolean isFbLogin) {
        mPreferenceHelper.setUserInfo(user, isFbLogin);
    }

    @Override
    public String getUserId() {
        return mPreferenceHelper.getUserId();
    }

    @Override
    public void setFirstLaunch(boolean isFirstLaunch) {
        mPreferenceHelper.setFirstLaunch(isFirstLaunch);
    }

    @Override
    public Boolean isFirstLaunch() {
        return mPreferenceHelper.isFirstLaunch();
    }

    @Override
    public String getImageUrl() {
        return mPreferenceHelper.getImageUrl();
    }

    @Override
    public boolean isLoggedin() {
        return mPreferenceHelper.isLoggedin();
    }

    @Override
    public boolean isFbLogin() {
        return mPreferenceHelper.isFbLogin();
    }

    @Override
    public void removeUserInfo() {
        mPreferenceHelper.removeUserInfo();
    }

    @Override
    public void setLaunchFirstTime(boolean isFirstTime) {
        mPreferenceHelper.setLaunchFirstTime(isFirstTime);
    }

    @Override
    public boolean isLaunchFirstTime() {
        return mPreferenceHelper.isLaunchFirstTime();
    }

    @Override
    public void setReferralCode(@NonNull String token) {
        mPreferenceHelper.setReferralCode(token);
    }

    @Override
    public String getReferralCode() {
        return mPreferenceHelper.getReferralCode();
    }


    @Override
    public void invokeActiveContentApi(String url, ContentRequest body, IResponseCallback callback) {
        mApiHelper.invokeActiveContentApi(url, body, callback);
    }

    @Override
    public void invokeActiveCategoriesApi(String url, IResponseCallback callback) {
        mApiHelper.invokeActiveCategoriesApi(url, callback);
    }

    @Override
    public void invokeActivePreferencesApi(String url, IResponseCallback callback) {
        mApiHelper.invokeActivePreferencesApi(url, callback);
    }

    @Override
    public void invokeLanguageApi(String url, IResponseCallback callback) {
        mApiHelper.invokeLanguageApi(url, callback);
    }

    @Override
    public void invokeFeedbackApi(String url, FeedbackRequest body, IResponseCallback callback) {
        mApiHelper.invokeFeedbackApi(url, body, callback);
    }

    @Override
    public void invokePollApi(String url, PollRequestBody body, IResponseCallback callback) {
        mApiHelper.invokePollApi(url, body, callback);
    }

    @Override
    public void invokeReferralApi(String url, ReferralRequest body, IResponseCallback callback) {
        mApiHelper.invokeReferralApi(url, body, callback);
    }

//    @Override
//    public Completable insertLanguage(List<Language> languages) {
//        return mDbHelper.insertLanguage(languages);
//    }
//
//    @Override
//    public Completable insertPreference(List<Preference> preferences) {
//        return mDbHelper.insertPreference(preferences);
//    }
//
//    @Override
//    public Flowable<List<Language>> getLanguages() {
//        return mDbHelper.getLanguages();
//    }
//
//    @Override
//    public Flowable<List<Preference>> getPreferences() {
//        return mDbHelper.getPreferences();
//    }
//
//    @Override
//    public Flowable<Preference> getPreferenceByName(final String name) {
//        return mDbHelper.getPreferenceByName(name);
//    }
//
//    @Override
//    public Completable insertCategories(List<Category> categories) {
//        return mDbHelper.insertCategories(categories);
//    }
//
//    @Override
//    public Flowable<List<Category>> getCategories() {
//        return mDbHelper.getCategories();
//    }
//
//    @Override
//    public Completable insertContents(List<Content> contents) {
//        return mDbHelper.insertContents(contents);
//    }
//
//
//    @Override
//    public Completable updateBookmarkStatusByContentId(long contentId, boolean isBookmark) {
//        return mDbHelper.updateBookmarkStatusByContentId(contentId, isBookmark);
//    }
//
//    @Override
//    public Completable updateReadStatusByContentId(long contentId, boolean isRead) {
//        return mDbHelper.updateReadStatusByContentId(contentId, isRead);
//    }
//
//    @Override
//    public Flowable<Boolean> isBookmarked(final long contentId) {
//        return mDbHelper.isBookmarked(contentId);
//    }
//
//    @Override
//    public List<Content> getAllContentWithImage(final ContentType type) {
//        return mDbHelper.getAllContentWithImage(type);
//    }
}
