package com.sandesh360.sandesh.data.network;

import com.sandesh360.sandesh.data.network.model.ContentRequest;
import com.sandesh360.sandesh.data.network.model.FeedbackRequest;
import com.sandesh360.sandesh.data.network.model.PollRequestBody;
import com.sandesh360.sandesh.data.network.model.ReferralRequest;

/**
 * This is singleton ApiManager class and used to manage all API calling
 * Created by Mobarak on 26-Sep-17.
 *
 * @author Atom AP Ltd.
 */

public class ApiManager implements IApiHelper {

    /**
     * Global instance of ApiManager
     */
    private static ApiManager mInstance;

    private ApiService apiService;

    /**
     * Default private constructor
     */
    private ApiManager() {
        apiService = ApiClient.createApiClient(ApiService.class);
    }

    /**
     * Invoke to get ApiManager instance. If instance is null,
     * then create new object otherwise return previous one
     *
     * @return mInstance
     */

    public static ApiManager getInstance() {
        if (mInstance == null) {
            synchronized (ApiManager.class) {
                if (mInstance == null) {
                    mInstance = new ApiManager();
                }
            }
        }
        return mInstance;
    }


    @Override
    public void invokeActiveContentApi(String url, ContentRequest body, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.getActiveContents(url, body));
    }

    @Override
    public void invokeActiveCategoriesApi(String url, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.getActiveCategories(url));
    }

    @Override
    public void invokeActivePreferencesApi(String url, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.getActivePreferences(url));
    }

    @Override
    public void invokeLanguageApi(String url, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.getLanguages(url));
    }

    @Override
    public void invokeFeedbackApi(String url, FeedbackRequest body, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.invokeFeedbackApi(url, body));
    }

    @Override
    public void invokePollApi(String url, PollRequestBody body, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.invokePollApi(url, body));
    }

    @Override
    public void invokeReferralApi(String url, ReferralRequest body, IResponseCallback callback) {
        new ResponseParser(callback).getResponse(apiService.invokeReferralApi(url, body));
    }
}
