package com.sandesh360.sandesh.data.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.sandesh360.sandesh.data.db.entity.Language;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Mobarak on November 07, 2018
 *
 * @author Sandesh360
 */
@Dao
public abstract class LanguageDao {

    private static final String TAG = LanguageDao.class.getSimpleName();

    /**
     * Insert a user in the database. If the user already exists, replace it.
     *
     * @param languages the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertLanguage(Language languages);

    @Query("SELECT * FROM language n WHERE n.id = :id")
    public abstract Language getLanguageById(final int id);

    /**
     * Update the read status in the table content
     */
    @Query("UPDATE language SET name = :name, locale = :locale WHERE id = :id")
    public abstract void updateLanguageById(String name, String locale, int id);


    /**
     * Get the all name and meaning in english from the table.
     *
     * @return the user from the table
     */
    @Query("SELECT * FROM language")
    public abstract Flowable<List<Language>> getLanguages();

    public void insertLanguages(List<Language> languages) {
        for (int i = 0; i < languages.size(); i++) {
            Language newLang = languages.get(i);
            Language oldLang = getLanguageById(newLang.getId());
            if (oldLang == null) {
                insertLanguage(newLang);
            } else {
                updateLanguageById(newLang.getName(), newLang.getLocale(), newLang.getId());
            }
        }
    }

}
