package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.network.Endpoints;

/**
 * Created by Mobarak on November 04, 2018
 *
 * @author Sandesh360
 */
public class ContentRequest extends BaseRequest {

    @SerializedName(Endpoints.CONTENT)
    private RequestBody body;

    public ContentRequest() {
        body = new RequestBody();
    }

    public RequestBody getBody() {
        return body;
    }

    public void setLanguageId(int languageId) {
        this.body.languageId = languageId;
    }

    public void setCategoryId(Long categoryId) {
        this.body.categoryId = categoryId;
    }

    public void setPreferenceId(Long preferenceId) {
        this.body.preferenceId = preferenceId;
    }

    public void setUserId(Long userId) {
        this.body.userId = userId;
    }

    public void setSearchKey(String searchKey) {
        this.body.searchKey = searchKey;
    }

    public void setLastUpdated(long lastUpdated) {
        this.body.lastUpdated = lastUpdated;
    }

    protected class RequestBody {
        @SerializedName(Endpoints.LANGUAGE_ID)
        private int languageId;
        @SerializedName(Endpoints.CATEGORY_ID)
        private Long categoryId;
        @SerializedName(Endpoints.PREFERENCE_ID)
        private Long preferenceId;
        @SerializedName(Endpoints.USER_ID)
        private Long userId;
        @SerializedName(Endpoints.SEARCH_KEY)
        private String searchKey;

        @SerializedName(Endpoints.LAST_UPDATED)
        private long lastUpdated;
    }
}


