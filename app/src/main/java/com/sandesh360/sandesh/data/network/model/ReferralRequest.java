package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.network.Endpoints;

public class ReferralRequest {

    @SerializedName(Endpoints.REFERRAL)
    private RequestBody body;

    public ReferralRequest() {
        body = new RequestBody();
    }

    public RequestBody getBody() {
        return body;
    }

    public ReferralRequest setReferralDeviceType(String device) {
        this.body.device = device;
        return this;
    }

    public ReferralRequest setReferralDeviceOSVersion(String version) {
        this.body.version = version;
        return this;
    }

    public ReferralRequest setReferralCode(String code) {
        this.body.referralCode = code;
        return this;
    }


    public ReferralRequest setReferralDeviceName(String deviceName) {
        this.body.deviceName = deviceName;
        return this;
    }

    protected class RequestBody {
        @SerializedName(Endpoints.REFERRAL_DEVICE)
        private String device;

        @SerializedName(Endpoints.REFERRAL_VERSION)
        private String version;

        @SerializedName(Endpoints.REFERRAL_CODE)
        private String referralCode;

        @SerializedName(Endpoints.REFERRAL_DEVICE_NAME)
        private String deviceName;

    }
}
