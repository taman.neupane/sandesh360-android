package com.sandesh360.sandesh.data.network.model;

import com.sandesh360.sandesh.data.db.entity.Language;

import java.util.List;

/**
 * Created by Mobarak on November 08, 2018
 *
 * @author Sandesh360
 */
public class LanguageResponse extends BaseResponse {

    private List<Language> languages;

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }
}
