package com.sandesh360.sandesh.data.model;

import java.math.BigInteger;

/**
 * Created by Mobarak on January 03, 2019
 *
 * @author Sandesh360
 */
public class FcmNotification {
    private String id;
    private String title;
    private String description;
    private String url;
    private String clickAction;

    public String getId() {
        return id == null ? "0" : id;
    }

    public FcmNotification setId(String id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title == null ? "" : title;
    }

    public FcmNotification setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description == null ? "" : description;
    }

    public FcmNotification setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getUrl() {
        return url == null ? "" : url;
    }

    public FcmNotification setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getClickAction() {
        return clickAction == null ? "" : clickAction;
    }

    public FcmNotification setClickAction(String clickAction) {
        this.clickAction = clickAction;
        return this;
    }
}
