package com.sandesh360.sandesh.data.network;

import com.sandesh360.sandesh.data.network.model.BaseResponse;
import com.sandesh360.sandesh.data.network.model.CategoryResponse;
import com.sandesh360.sandesh.data.network.model.ContentRequest;
import com.sandesh360.sandesh.data.network.model.ContentResponse;
import com.sandesh360.sandesh.data.network.model.FeedbackRequest;
import com.sandesh360.sandesh.data.network.model.LanguageResponse;
import com.sandesh360.sandesh.data.network.model.PollRequestBody;
import com.sandesh360.sandesh.data.network.model.PollResponse;
import com.sandesh360.sandesh.data.network.model.PreferenceResponse;
import com.sandesh360.sandesh.data.network.model.ReferralRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by Mobarak on November 04, 2018
 *
 * @author Sandesh360
 */
public interface ApiService {

    @POST
    Call<ContentResponse> getActiveContents(@Url String url, @Body ContentRequest body);

    @GET
    Call<CategoryResponse> getActiveCategories(@Url String url);

    @GET
    Call<PreferenceResponse> getActivePreferences(@Url String url);

    @GET
    Call<LanguageResponse> getLanguages(@Url String url);

    @POST
    Call<BaseResponse> invokeFeedbackApi(@Url String url, @Body FeedbackRequest body);

    @POST
    Call<PollResponse> invokePollApi(@Url String url, @Body PollRequestBody body);

    @POST
    Call<BaseResponse> invokeReferralApi(@Url String url, @Body ReferralRequest body);
}
