package com.sandesh360.sandesh.data.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import android.database.Cursor;

import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.Image;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.db.entity.Question;
import com.sandesh360.sandesh.ui.content.container.ContentType;
import com.sandesh360.sandesh.utils.LocaleUtil;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Mobarak on November 09, 2018
 *
 * @author Sandesh360
 */
@Dao
public abstract class ContentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertContent(Content content);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertImage(Image... images);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertQuestion(Question question);

    @Query("SELECT * FROM content c WHERE c.language_id = :languageId ORDER BY c.updated_at DESC")
    public abstract List<Content> getAllContents(int languageId);

    @Query("SELECT * FROM content c WHERE c.is_bookmark = 1 AND c.language_id = :languageId ORDER BY c.updated_at DESC")
    public abstract List<Content> getBookmarkContents(int languageId);

    @Query("SELECT * FROM content c WHERE c.is_read = 0 AND c.language_id = :languageId ORDER BY c.updated_at DESC")
    public abstract List<Content> getUnreadContents(int languageId);

    @Query("SELECT * FROM content c WHERE c.id = :contentId")
    public abstract Content getContentById(final long contentId);

    @Query("SELECT * FROM image i WHERE content_id = :contentId")
    public abstract Image getImagesByContentId(final long contentId);

    @Query("SELECT * FROM question q WHERE content_id = :contentId")
    public abstract Question getQuestionByContentId(final long contentId);


    /**
     * Update the bookmark status in the table content
     */
    @Query("UPDATE content SET is_bookmark = :isBookmark WHERE id = :id")
    public abstract void updateBookmarkStatusByContentId(final long id, boolean isBookmark);

    /**
     * Update the read status in the table content
     */
    @Query("UPDATE content SET is_read = :isRead WHERE id = :id")
    public abstract void updateReadStatusByContentId(final long id, boolean isRead);

    @Query("SELECT is_bookmark FROM content WHERE id = :contentId")
    public abstract Flowable<Boolean> isBookmarked(final long contentId);


    public void insertContentWithImages(List<Content> contents) {
        for (int i = 0; i < contents.size(); i++) {
            Content newCont = contents.get(i);
            Content oldCont = getContentById(newCont.getId());
            if (oldCont != null) {
                newCont.setBookmark(oldCont.isBookmark());
                newCont.setRead(oldCont.isRead());
            }
            insertContent(newCont);
            if (newCont.getImage() != null)
                insertImage(newCont.getImage());
            if (newCont.getQuestion() != null)
                insertQuestion(newCont.getQuestion());
        }
    }

    /**
     * Invoke to get content according to content type
     *
     * @param type
     * @return
     */
    public List<Content> getAllContentWithImage(ContentType type) {
        List<Content> contents;
        switch (type) {
            case BOOKMARKS:
                contents = getBookmarkContents(LocaleUtil.getLanguageId());
                break;
            case UNREAD:
                contents = getUnreadContents(LocaleUtil.getLanguageId());
                break;
            default:
                contents = getAllContents(LocaleUtil.getLanguageId());
                break;
        }
        for (int i = 0; i < contents.size(); i++) {
            Content content = contents.get(i);
            // Find image
            Image images = getImagesByContentId(content.getId());
            content.setImage(images);

            // Find questions
            Question question = getQuestionByContentId(content.getId());
            content.setQuestion(question);

            // Update list
            contents.set(i, content);
        }
        return contents;
    }

    @Query("SELECT title FROM content")
    public abstract String[] getContentTitle();

    @Query("SELECT title FROM content WHERE title like :query")
    public abstract Cursor getCursor(String query);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertPoll(Poll poll);

    @Query("SELECT * FROM poll p WHERE p.question_id = :id")
    public abstract Flowable<Poll> getPollByQuestionId(Integer id);
}
