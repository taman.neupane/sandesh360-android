package com.sandesh360.sandesh.data.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.sandesh360.sandesh.data.db.entity.NativeAds;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Mobarak on December 03, 2018
 *
 * @author Sandesh360
 */
@Dao
public interface NativeAdsDao {

    /**
     * Insert a category in the database. If the user already exists, replace it.
     *
     * @param nativeAds the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAds(List<NativeAds> nativeAds);

    /**
     * Get the all name and meaning in english from the table.
     *
     * @return the user from the table
     */
    @Query("SELECT * FROM nativeads a ORDER BY a.id DESC")
    Flowable<List<NativeAds>> getAds();
}
