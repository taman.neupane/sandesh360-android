package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.data.network.Endpoints;

import java.util.List;

/**
 * Created by Mobarak on November 04, 2018
 *
 * @author Sandesh360
 */
public class CategoryResponse extends BaseResponse {

    @SerializedName(Endpoints.CATEGORIES)
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
