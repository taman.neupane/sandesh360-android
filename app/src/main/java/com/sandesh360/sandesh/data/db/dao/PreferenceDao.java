package com.sandesh360.sandesh.data.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.sandesh360.sandesh.data.db.entity.Preference;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Mobarak on November 07, 2018
 *
 * @author Sandesh360
 */
@Dao
public abstract class PreferenceDao {

    private static final String TAG = PreferenceDao.class.getSimpleName();

    /**
     * Insert a user in the database. If the user already exists, replace it.
     *
     * @param preferences the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertPreference(Preference preferences);

    /**
     * Get the all name and meaning in english from the table.
     *
     * @return the user from the table
     */
    @Query("SELECT * FROM preference")
    public abstract Flowable<List<Preference>> getPreferences();

    @Query("SELECT * FROM preference n WHERE n.preference_name = :name")
    public abstract Flowable<Preference> getPreferenceByName(final String name);

    @Query("SELECT * FROM preference n WHERE n.id = :id")
    public abstract Preference getPreferenceById(final int id);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updatePref(Preference preference);


    public void insertPreferences(List<Preference> preferences) {
        for (int i = 0; i < preferences.size(); i++) {
            Preference newPref = preferences.get(i);
            Preference oldPref = getPreferenceById(newPref.getId());
            if (oldPref == null) {
                insertPreference(newPref);
            } else {
                updatePref(newPref);
            }

        }

    }
}
