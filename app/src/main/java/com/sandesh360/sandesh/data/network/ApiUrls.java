package com.sandesh360.sandesh.data.network;

/**
 * Created by Mobarak on 26-Sep-17.
 */

public class ApiUrls {

    /**
     * Base API URL
     */
    public static final String BASE_API_URL = "http://www.sandesh360.com:8080/sandesh360/";
    public static final String BASE_IMAGE_URL = "http://www.sandesh360.com:8080/sandesh360";
//    public static final String BASE_API_URL = "http://192.168.0.111:7012/sandesh360/";

    // CONTENT APIs
    public static final String CONTENT_ICON_URL = BASE_API_URL + "icon/content/";
    public static final String CONTENT_URL = "api/content/by/";
    public static final String CONTENT_BY_CATEGORY_ID = CONTENT_URL + "categoryid";
    public static final String CONTENT_BY_PREFERENCE_ID = CONTENT_URL + "preferenceid";
    public static final String BOOKMARK_CONTENT = CONTENT_URL + "userid";
    public static final String POLL_CONTENT = CONTENT_URL + "withquestion";
    public static final String SEARCH_CONTENT = BASE_API_URL + "api/content/search";

    // Category APIs

    public static final String CATEGORY_ICON_URL = "http://www.sandesh360.com:8080/sandesh360";
    public static final String ACTIVE_CATEGORY_URL = "api/categories/active/";

    // Preference APIs
    public static final String ACTIVE_PREFERENCE_URL = "api/preferences/active/";

    public static final String LANGUAGE_URL = "api/language/all/asc";
    public static final String FEEDBACK_API_URL = "api/feedback/add";
    public static final String POLL_API_URL = "api/poll/add";
    public static final String PRIVACY_POLICY = "http://sandesh360.com/terms.html";

    public static final String SEND_REFERRAL_CODE = "api/referral/add";


}
