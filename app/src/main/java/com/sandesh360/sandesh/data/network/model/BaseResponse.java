package com.sandesh360.sandesh.data.network.model;


import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.network.Endpoints;

/**
 * Created by Mobarak on 26-Feb-18.
 */

public class BaseResponse {

    @SerializedName(Endpoints.STATUS)
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
