package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.network.Endpoints;

/**
 * Created by Mobarak on December 03, 2018
 *
 * @author Sandesh360
 */
public class FeedbackRequest extends BaseRequest {

    @SerializedName(Endpoints.FEEDBACK)
    private RequestBody body;

    public FeedbackRequest() {
        body = new RequestBody();
    }

    public RequestBody getBody() {
        return body;
    }

    public FeedbackRequest setFrom(String from) {
        this.body.from = from;
        return this;
    }

    public FeedbackRequest setComment(String comment) {
        this.body.comment = comment;
        return this;
    }

    protected class RequestBody {
        @SerializedName(Endpoints.FROM)
        private String from;

        @SerializedName(Endpoints.COMMENT)
        private String comment;

    }
}
