package com.sandesh360.sandesh.data.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "content",
        foreignKeys = {
                @ForeignKey(entity = Language.class,
                        parentColumns = "id",
                        childColumns = "language_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE
                )
        },
        indices = {
                @Index(value = "language_id")
        }
)
public class Content implements Serializable {

    public Content() {

    }

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private long id;

    @ColumnInfo(name = "title")
    @SerializedName("title")
    private String title;

    @ColumnInfo(name = "short_description")
    @SerializedName("short_description")
    private String shortDescription;

    @ColumnInfo(name = "description")
    @SerializedName("description")
    private String description;

    @ColumnInfo(name = "content_url")
    @SerializedName("content_url")
    private String contentUrl;

    @ColumnInfo(name = "site_url")
    @SerializedName("site_url")
    private String siteUrl;

    @ColumnInfo(name = "language_id")
    @SerializedName("language_id")
    private int languageId;

    @ColumnInfo(name = "category_id")
    @SerializedName("category_id")
    private long categoryId;

    @ColumnInfo(name = "preference_id")
    @SerializedName("preference_id")
    private long preferenceId;

    @ColumnInfo(name = "user_id")
    @SerializedName("user_id")
    private long userId;

    @Ignore
    @SerializedName("image")
    private Image image;

    @Ignore
    @SerializedName("question")
    private Question question;

    @ColumnInfo(name = "is_active")
    @SerializedName("is_active")
    private boolean isActive;

    @ColumnInfo(name = "created_by")
    @SerializedName("created_by")
    private String createdBy;

    @ColumnInfo(name = "created_at")
    @SerializedName("createdAt")
    private Long createdAt;

    @ColumnInfo(name = "updated_at")
    @SerializedName("updatedAt")
    private Long updatedAt;

    @ColumnInfo(name = "is_bookmark")
    private boolean isBookmark = false;

    @ColumnInfo(name = "is_read")
    private boolean isRead = false;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContentUrl() {
        return contentUrl;
    }

    public void setContentUrl(String contentUrl) {
        this.contentUrl = contentUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getPreferenceId() {
        return preferenceId;
    }

    public void setPreferenceId(long preferenceId) {
        this.preferenceId = preferenceId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public boolean isBookmark() {
        return isBookmark;
    }

    public void setBookmark(boolean bookmark) {
        isBookmark = bookmark;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public Content(Content content) {
        setId(content.getId());
        setTitle(content.getTitle());
        setShortDescription(content.getShortDescription());
        setDescription(content.getDescription());
        setContentUrl(content.getSiteUrl());
        setSiteUrl(content.getSiteUrl());
        setLanguageId(content.getLanguageId());
        setActive(content.isActive());
        setCreatedBy(content.getCreatedBy());
        setCreatedAt(content.getCreatedAt());
        setUpdatedAt(content.getUpdatedAt());
        setImage(content.getImage());
        setQuestion(content.question);
    }
}
