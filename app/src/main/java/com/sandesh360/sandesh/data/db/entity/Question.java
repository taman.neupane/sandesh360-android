package com.sandesh360.sandesh.data.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@Entity(tableName = "question",
        foreignKeys = {
                @ForeignKey(entity = Content.class,
                        parentColumns = "id",
                        childColumns = "content_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE
                )
        },
        indices = {
                @Index(value = "content_id")
        }
)
public class Question implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    private Integer id;
    @ColumnInfo(name = "is_active")
    @SerializedName("is_active")
    private boolean isActive;
    @ColumnInfo(name = "is_delete")
    @SerializedName("is_delete")
    private boolean isDelete;

    @ColumnInfo(name = "question_text")
    @SerializedName("question_text")
    private String questionText;

    @ColumnInfo(name = "content_id")
    @SerializedName("content_id")
    private Integer contentId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Integer getContentId() {
        return contentId;
    }

    public void setContentId(Integer contentId) {
        this.contentId = contentId;
    }

}