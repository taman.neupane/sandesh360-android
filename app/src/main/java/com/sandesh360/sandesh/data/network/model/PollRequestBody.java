package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.network.Endpoints;

/**
 * Created by Mobarak on December 07, 2018
 *
 * @author Sandesh360
 */
public class PollRequestBody extends BaseRequest {

    @SerializedName(Endpoints.POLL)
    private RequestBody body;

    public PollRequestBody() {
        body = new RequestBody();
    }

    public RequestBody getBody() {
        return body;
    }

    public PollRequestBody setQuestionId(Integer questionId) {
        this.body.questionId = questionId;
        return this;
    }

    public PollRequestBody setYes(boolean yes) {
        this.body.isYes = yes;
        return this;
    }

    public PollRequestBody setVoter(String voter) {
        this.body.voter = voter;
        return this;
    }

    protected class RequestBody {
        @SerializedName(Endpoints.QUESTION_ID)
        private Integer questionId;
        @SerializedName(Endpoints.IS_YES)
        private boolean isYes;
        @SerializedName(Endpoints.VOTER)
        private String voter;

    }
}
