package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.data.network.Endpoints;

import java.util.List;

/**
 * Created by Mobarak on November 04, 2018
 *
 * @author Sandesh360
 */
public class PreferenceResponse extends BaseResponse {

    @SerializedName(Endpoints.PREFERENCES)
    private List<Preference> preferences;

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }
}
