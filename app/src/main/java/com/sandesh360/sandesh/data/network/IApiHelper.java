package com.sandesh360.sandesh.data.network;


import com.sandesh360.sandesh.data.network.model.ContentRequest;
import com.sandesh360.sandesh.data.network.model.FeedbackRequest;
import com.sandesh360.sandesh.data.network.model.PollRequestBody;
import com.sandesh360.sandesh.data.network.model.ReferralRequest;

/**
 * This is a API helper interface
 * Created by Mobarak on 26-Sep-17.
 *
 * @author Mobarak.
 */

public interface IApiHelper {

    void invokeActiveContentApi(String url, ContentRequest body, IResponseCallback callback);

    void invokeActiveCategoriesApi(String url, IResponseCallback callback);

    void invokeActivePreferencesApi(String url, IResponseCallback callback);

    void invokeLanguageApi(String url, IResponseCallback callback);

    void invokeFeedbackApi(String url, FeedbackRequest body, IResponseCallback callback);

    void invokePollApi(String url, PollRequestBody body, IResponseCallback callback);

    void invokeReferralApi(String url, ReferralRequest body, IResponseCallback callback);

}
