package com.sandesh360.sandesh.data.network;

/**
 * Created by Mobarak on 26-Feb-18.
 */

public interface IResponseCallback<T> {

    void onSuccessResponse(T response);

    void onErrorResponse(ApiError error);
}
