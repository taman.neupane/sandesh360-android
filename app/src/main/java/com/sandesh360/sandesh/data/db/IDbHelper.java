package com.sandesh360.sandesh.data.db;

import android.database.Cursor;

import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.Language;
import com.sandesh360.sandesh.data.db.entity.NativeAds;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.ui.content.container.ContentType;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * This is a DbHelper helper interface
 * Created by Mobarak on 09-Feb-18.
 *
 * @author Atom AP Ltd.
 */

public interface IDbHelper {


    // Language table
    Completable insertLanguage(List<Language> languages);

    Flowable<List<Language>> getLanguages();

    // Preferences
    Completable insertPreference(List<Preference> preferences);

    Flowable<List<Preference>> getPreferences();

    Flowable<Preference> getPreferenceByName(final String name);

    // Categories
    Completable insertCategories(List<Category> categories);

    Flowable<List<Category>> getCategories(final long languageId);

    // Contents
    Completable insertContents(List<Content> contents);

    Completable updateBookmarkStatusByContentId(final long contentId, boolean isBookmark);

    Completable updateReadStatusByContentId(final long contentId, boolean isRead);

    Flowable<Boolean> isBookmarked(final long contentId);


    List<Content> getAllContentWithImage(final ContentType type);

    // Categories
    Completable insertNativeAds(List<NativeAds> ads);

    Flowable<List<NativeAds>> getAds();

    String[] getContentTitle();

    Cursor getCursor(String query);

    Flowable<Poll> getPollByQuestionId(final Integer questionId);

    // Categories
    Completable insertPoll(Poll poll);


}
