package com.sandesh360.sandesh.data.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "image",
        foreignKeys = {
                @ForeignKey(entity = Content.class,
                        parentColumns = "id",
                        childColumns = "content_id",
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE
                )
        },
        indices = {
                @Index(value = "content_id")
        }
)
public class Image implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private long id;

    @ColumnInfo(name = "image_url")
    @SerializedName("image_url")
    private String imageUrl;

    @ColumnInfo(name = "video_url")
    @SerializedName("video_url")
    private String videoUrl;

    @ColumnInfo(name = "content_id")
    @SerializedName("content_id")
    private long contentId;

    @ColumnInfo(name = "uploaded_at")
    @SerializedName("uploaded_at")
    private Long uploadedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public long getContentId() {
        return contentId;
    }

    public void setContentId(long contentId) {
        this.contentId = contentId;
    }

    public Long getUploadedAt() {
        return uploadedAt;
    }

    public void setUploadedAt(Long uploadedAt) {
        this.uploadedAt = uploadedAt;
    }

    public boolean hasVideo() {
        return this.videoUrl != null && videoUrl.startsWith("https");
    }

    public String getYouTubeUrl() {
        String[] url = this.videoUrl.split("=");
        if (url != null && url.length == 2) {
            return url[1];
        }
        return "unsupported url";
    }

}
