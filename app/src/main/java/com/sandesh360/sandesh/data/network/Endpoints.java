package com.sandesh360.sandesh.data.network;

public interface Endpoints {
    String STATUS = "status";
    String CODE = "code";
    String MESSAGE = "message";
    /**
     * End point Terminal Id
     */
    String TERMINAL_ID = "terminal_id";
    /**
     * End point authentication token
     */
    String TOKEN = "token";
    String LANGUAGE_ID = "language_id";
    String FCM_TOKEN = "fcm_token";
    String USER_ID = "user_id";
    String CATEGORY_ID = "category_id";
    String PREFERENCE_ID = "preference_id";
    String CONTENT = "content";
    String CONTENTS = "contents";
    String CATEGORIES = "categories";
    String PREFERENCES = "preferences";
    String ADS = "adds";
    String NEWSBOTTOMADS = "newsBottomAds";
    String FREQUENCY = "frequency";
    String FROM = "from";
    String COMMENT = "comment";
    String FEEDBACK = "feedback";
    String REFERRAL = "marketerReferral";
    String SEARCH_KEY = "search_key";
    String LAST_UPDATED = "last_updated";
    String QUESTION_ID = "question_id";
    String IS_YES = "is_yes";
    String VOTER = "voter";
    String YES = "yes";
    String NO = "no";
    String POLL = "poll";
    String PERCENTAGE = "percentage";

    // FcmKey
    String DATA = "data";
    String ID = "id";
    String TITLE = "title";
    String DESCRIPTION = "description";
    String IMAGE_URL = "url";
    String CLICK_ACTION = "click_action";

    //Refreeal
    String REFERRAL_DEVICE = "device";
    String REFERRAL_VERSION = "version";
    String REFERRAL_CODE = "referralCode";
    String REFERRAL_DEVICE_NAME = "deviceName";
}
