package com.sandesh360.sandesh.data.network;


import com.google.gson.annotations.SerializedName;

/**
 * Created by mobarak on 26-Feb-18.
 */

public class ApiError {

    @SerializedName(Endpoints.STATUS)
    private String status;
    @SerializedName(Endpoints.CODE)
    private int errorCode;
    @SerializedName(Endpoints.MESSAGE)
    private String message;

    public String getStatus() {
        return status;
    }

    public ApiError setStatus(String status) {
        this.status = status;
        return this;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public ApiError setErrorCode(int errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ApiError setMessage(String message) {
        this.message = message;
        return this;
    }
}
