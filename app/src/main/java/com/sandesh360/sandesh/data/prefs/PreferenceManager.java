package com.sandesh360.sandesh.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import com.sandesh360.sandesh.data.model.User;
import com.sandesh360.sandesh.ui.base.Sandesh360App;
import com.sandesh360.sandesh.utils.Constants;


/**
 * This is a sharedpreference singleton class and used for save,
 * retrive and removing small amount of data locally
 * Created by Mobarak on 26-Sep-17.
 *
 * @author Atom AP Ltd.
 */

public class PreferenceManager implements IPreferenceHelper {


    private static final String APP_LAUNCH_FIRST_TIME = "app_launch_first_time";
    /**
     * Global instance of PreferenceManager
     */
    private static PreferenceManager mInstance;

    // SharedPreference name key
    private final String PREFERENCE_NAME = "android_bas_preference";


    // Authentication token key
    private final String FOODLLEE_TOKEN = "android_bas_auth_token";

    // FCM token key
    private final String FCM_TOKEN = "fcm_token";

    private static final String SELECTED_LANGUAGE = "Locale_Helper_Selected_Language";

    private final String USER_NAME = "user_name";
    private final String USER_EMAIL = "user_email";
    private final String USER_ID = "user_id";
    private final String IMAGE_URL = "image_url";
    private final String HAS_LOGIN = "has_login";
    private final String FB_LOGIN = "fb_login";
    private final String FIRST_LAUNCH = "first_launch";


    // SharedPreference and Editor instance
    private SharedPreferences sharedPreferences;

    /**
     * Default constactor
     */
    private PreferenceManager() {
        sharedPreferences = Sandesh360App.getAppContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Invoke to get/retrieve PreferenceManager instance. If instance is null
     * then create new instance otherwise return previous one
     *
     * @return mInstance
     */
    public static synchronized PreferenceManager getInstance() {
        if (mInstance == null) {
            mInstance = new PreferenceManager();
        }
        return mInstance;
    }


    /**
     * Invoke to set/save authentication token after login
     *
     * @param token String
     */
    @Override
    public void setAuthToken(String token) {
        sharedPreferences.edit().putString(FOODLLEE_TOKEN, token).commit();
    }


    /**
     * Invoke to get authentication token
     *
     * @return string
     */
    @Override
    public String getAuthToken() {
        return sharedPreferences.getString(FOODLLEE_TOKEN, null);
    }

    /**
     * Invoke to save deviceId/terminalId/MacAddress
     *
     * @param terminalId String
     */
    @Override
    public void setTerminalId(String terminalId) {
        sharedPreferences.edit().putString(Constants.TERMINAL_ID, terminalId).commit();
    }

    /**
     * Invoke to retrieve deviceId/terminalId/MacAddress
     *
     * @return string
     */
    @Override
    public String getTerminalId() {
        return sharedPreferences.getString(Constants.TERMINAL_ID, null);
    }

    /**
     * Invoke to save FirebaseToken
     *
     * @param token string
     */
    @Override
    public void setFcmToken(@NonNull String token) {
        sharedPreferences.edit().putString(FCM_TOKEN, token).commit();
    }

    /**
     * Invoke to retrieve FCM Token
     *
     * @return string
     */
    @Override
    public String getFcmToken() {
        return sharedPreferences.getString(FCM_TOKEN, null);
    }


    /**
     * invoke locale data
     *
     * @param language
     */
    @Override
    public void setLocalePersistData(String language) {
        sharedPreferences.edit().putString(SELECTED_LANGUAGE, language).commit();
    }

    /**
     * To retrieve locale data
     *
     * @return
     */
    @Override
    public String getLocalePersistedData() {
        return sharedPreferences.getString(SELECTED_LANGUAGE, Constants.LOCALE_BN);
    }

    @Override
    public void setUserInfo(User user, boolean isFbLogin) {
        if (user != null)
            sharedPreferences.edit()
                    .putString(USER_NAME, user.getName())
                    .putString(USER_EMAIL, user.getEmail())
                    .putString(USER_ID, user.getUserId())
                    .putString(IMAGE_URL, user.getImageUrl())
                    .putBoolean(HAS_LOGIN, true)
                    .putBoolean(FB_LOGIN, isFbLogin)
                    .apply();
    }

    @Override
    public String getUserId() {
        return sharedPreferences.getString(USER_ID, "");
    }

    @Override
    public void setFirstLaunch(boolean isFirstLaunch) {
        sharedPreferences.edit().putBoolean(FIRST_LAUNCH, isFirstLaunch).apply();
    }

    @Override
    public Boolean isFirstLaunch() {
        return sharedPreferences.getBoolean(FIRST_LAUNCH, true);
    }

    @Override
    public String getImageUrl() {
        return sharedPreferences.getString(IMAGE_URL, "");
    }

    @Override
    public boolean isLoggedin() {
        return sharedPreferences.getBoolean(HAS_LOGIN, false);
    }

    @Override
    public boolean isFbLogin() {
        return sharedPreferences.getBoolean(FB_LOGIN, false);
    }

    @Override
    public void setLaunchFirstTime(boolean isFirstTime) {
        sharedPreferences.edit()
                .putBoolean(APP_LAUNCH_FIRST_TIME, isFirstTime)
                .apply();
    }

    @Override
    public boolean isLaunchFirstTime() {
        return sharedPreferences.getBoolean(APP_LAUNCH_FIRST_TIME, true);
    }

    @Override
    public void setReferralCode(@NonNull String token) {
        sharedPreferences.edit().putString(Constants.REFERRAL_CODE, token).commit();
    }

    @Override
    public String getReferralCode() {
         return sharedPreferences.getString(Constants.REFERRAL_CODE, "");
    }

    @Override
    public void removeUserInfo() {
        sharedPreferences.edit()
                .remove(USER_ID)
                .remove(USER_NAME)
                .remove(USER_EMAIL)
                .remove(IMAGE_URL)
                .remove(FB_LOGIN)
                .remove(HAS_LOGIN)
                .apply();
    }


}