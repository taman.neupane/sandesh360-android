package com.sandesh360.sandesh.data.network.model;

import com.google.gson.annotations.SerializedName;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.network.Endpoints;

/**
 * Created by Mobarak on December 07, 2018
 *
 * @author Sandesh360
 */
public class PollResponse extends BaseResponse {

    @SerializedName(Endpoints.PERCENTAGE)
    private Poll percentage;

    public Poll getPercentage() {
        return percentage;
    }

    public void setPercentage(Poll percentage) {
        this.percentage = percentage;
    }
}
