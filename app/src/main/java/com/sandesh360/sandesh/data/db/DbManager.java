package com.sandesh360.sandesh.data.db;

import android.database.Cursor;

import com.sandesh360.sandesh.data.db.dao.CategoryDao;
import com.sandesh360.sandesh.data.db.dao.ContentDao;
import com.sandesh360.sandesh.data.db.dao.LanguageDao;
import com.sandesh360.sandesh.data.db.dao.NativeAdsDao;
import com.sandesh360.sandesh.data.db.dao.PreferenceDao;
import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.Language;
import com.sandesh360.sandesh.data.db.entity.NativeAds;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.ui.content.container.ContentType;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

/**
 * This is singleton DbManager class and used to manage all kinds of database operation
 * Created by Mobarak on 09-Feb-18.
 */

public class DbManager implements IDbHelper {

    private final PreferenceDao preferenceDao;
    private final LanguageDao languageDao;
    private final CategoryDao categoryDao;
    private final ContentDao contentDao;
    private final NativeAdsDao adsDao;

    public DbManager(LanguageDao languageDao, PreferenceDao preferenceDao,
                     CategoryDao categoryDao, ContentDao contentDao, NativeAdsDao adsDao) {
        this.languageDao = languageDao;
        this.preferenceDao = preferenceDao;
        this.categoryDao = categoryDao;
        this.contentDao = contentDao;
        this.adsDao = adsDao;
    }


    @Override
    public Completable insertLanguage(List<Language> languages) {
        return Completable.fromAction(() -> languageDao.insertLanguages(languages));
    }

    @Override
    public Completable insertPreference(List<Preference> preferences) {
        return Completable.fromAction(() -> preferenceDao.insertPreferences(preferences));
    }

    @Override
    public Flowable<List<Language>> getLanguages() {
        return languageDao.getLanguages();
    }

    @Override
    public Flowable<List<Preference>> getPreferences() {
        return preferenceDao.getPreferences();
    }

    @Override
    public Flowable<Preference> getPreferenceByName(final String name) {
        return preferenceDao.getPreferenceByName(name);
    }

    @Override
    public Completable insertCategories(List<Category> categories) {
        return Completable.fromAction(() -> categoryDao.insertCategories(categories));
    }

    @Override
    public Flowable<List<Category>> getCategories(final long languageId) {
        return categoryDao.getCategories(languageId);
    }

    @Override
    public Completable insertContents(List<Content> contents) {
        return Completable.fromAction(() -> contentDao.insertContentWithImages(contents));
    }


    @Override
    public Completable updateBookmarkStatusByContentId(long contentId, boolean isBookmark) {
        return Completable.fromAction(() -> contentDao.updateBookmarkStatusByContentId(contentId, isBookmark));
    }

    @Override
    public Completable updateReadStatusByContentId(long contentId, boolean isRead) {
        return Completable.fromAction(() -> contentDao.updateReadStatusByContentId(contentId, isRead));
    }

    @Override
    public Flowable<Boolean> isBookmarked(long contentId) {
        return contentDao.isBookmarked(contentId);
    }

    @Override
    public List<Content> getAllContentWithImage(final ContentType type) {
        return contentDao.getAllContentWithImage(type);
    }

    @Override
    public Completable insertNativeAds(List<NativeAds> ads) {
        return Completable.fromAction(() -> adsDao.insertAds(ads));
    }

    @Override
    public Flowable<List<NativeAds>> getAds() {
        return adsDao.getAds();
    }

    @Override
    public String[] getContentTitle() {
        return contentDao.getContentTitle();
    }

    @Override
    public Cursor getCursor(String query) {
        return contentDao.getCursor(query);
    }

    @Override
    public Flowable<Poll> getPollByQuestionId(Integer questionId) {
        return contentDao.getPollByQuestionId(questionId);
    }

    @Override
    public Completable insertPoll(Poll poll) {
        return Completable.fromAction(() -> contentDao.insertPoll(poll));
    }
}
