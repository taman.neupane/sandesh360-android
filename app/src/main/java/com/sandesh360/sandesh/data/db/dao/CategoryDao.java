package com.sandesh360.sandesh.data.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.sandesh360.sandesh.data.db.entity.Category;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by Mobarak on November 09, 2018
 *
 * @author Sandesh360
 */
@Dao
public interface CategoryDao {

    /**
     * Insert a category in the database. If the user already exists, replace it.
     *
     * @param categories the user to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCategories(List<Category> categories);

    /**
     * Get the all name and meaning in english from the table.
     *
     * @return the user from the table
     */
    @Query("SELECT * FROM category c WHERE c.language_id = :languageId ORDER BY c.updated_at DESC")
    Flowable<List<Category>> getCategories(final long languageId);
}
