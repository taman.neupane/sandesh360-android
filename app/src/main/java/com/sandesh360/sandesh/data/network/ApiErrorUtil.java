package com.sandesh360.sandesh.data.network;

import android.content.Context;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by Mobarak on 26-Feb-18.
 */

public class ApiErrorUtil {

    public static ApiError parseError(Response<?> response) {
        ApiError error;
        try {
            Converter<ResponseBody, ApiError> converter = ApiClient.getRetrofit()
                    .responseBodyConverter(ApiError.class, new Annotation[0]);
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            error = new ApiError();
        }
        return error;
    }

    public static ApiError handleError(Context context, Throwable throwable) {
        String errorMessage = "";// context.getString(R.string.retrofit_request_fail);
        try {
            if (throwable != null) {
                errorMessage = errorMessage + " " + throwable.getLocalizedMessage().trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiError().setMessage(errorMessage);
    }

    public static void handleError(Context context, Response<?> response) {
        String errorMessage = "";//context.getString(R.string.api_handler_error);
        if (response.errorBody() != null) try {
            JSONObject jsonObject = new JSONObject(response.errorBody().string());
            switch (response.code()) {
                case 400:

                    break;
                case 500:

                    break;
                case 401:

            }
        } catch (JsonSyntaxException | JsonIOException | IOException | JSONException e) {
            e.printStackTrace();
        }

    }

}
