package com.sandesh360.sandesh.data.db;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;
import androidx.annotation.VisibleForTesting;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.sandesh360.sandesh.data.db.dao.CategoryDao;
import com.sandesh360.sandesh.data.db.dao.ContentDao;
import com.sandesh360.sandesh.data.db.dao.LanguageDao;
import com.sandesh360.sandesh.data.db.dao.NativeAdsDao;
import com.sandesh360.sandesh.data.db.dao.PreferenceDao;
import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.Image;
import com.sandesh360.sandesh.data.db.entity.Language;
import com.sandesh360.sandesh.data.db.entity.NativeAds;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.data.db.entity.Question;


@Database(entities = {
        Language.class,
        Preference.class,
        Category.class,
        Content.class,
        Image.class,
        Question.class,
        NativeAds.class,
        Poll.class
}, version = 7)
public abstract class AppDatabase extends RoomDatabase {

    private static volatile AppDatabase mInstance;

    @VisibleForTesting
    public static final String DATABASE_NAME = "sandesh360.db";

    public abstract LanguageDao languageDao();

    public abstract PreferenceDao preferenceDao();

    public abstract CategoryDao categoryDao();

    public abstract ContentDao contentDao();

    public abstract NativeAdsDao adsDao();


    public static AppDatabase getInstance(Context context) {
        if (mInstance == null) {
            synchronized (AppDatabase.class) {
                if (mInstance == null) {
                    mInstance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DATABASE_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return mInstance;
    }
}


