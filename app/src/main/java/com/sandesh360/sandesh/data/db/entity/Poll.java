package com.sandesh360.sandesh.data.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mobarak on December 07, 2018
 *
 * @author Sandesh360
 */
@Entity(tableName = "poll")
public class Poll {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private Integer id;

    @SerializedName("question_id")
    @ColumnInfo(name = "question_id")
    private Integer questionId;

    @ColumnInfo(name = "is_yes")
    private boolean isYes;

    @SerializedName("yes")
    private double yes;

    @SerializedName("no")
    private double no;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public boolean isYes() {
        return isYes;
    }

    public void setIsYes(boolean yes) {
        isYes = yes;
    }

    public double getYes() {
        return yes;
    }

    public void setYes(double yes) {
        this.yes = yes;
    }

    public double getNo() {
        return no;
    }

    public void setNo(double no) {
        this.no = no;
    }
}
