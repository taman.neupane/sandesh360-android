package com.sandesh360.sandesh.data.network;


import android.util.Log;

import com.sandesh360.sandesh.ui.base.Sandesh360App;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 26-Feb-18.
 */

public class ResponseParser {

    private static final String TAG = ResponseParser.class.getSimpleName();

    private IResponseCallback callback;

    public ResponseParser(IResponseCallback callback) {
        this.callback = callback;
    }

    public synchronized <T> void getResponse(Call<T> call) {
        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                if (response.isSuccessful()) {
                    Log.i(TAG, "onResponse: Successful");
                    callback.onSuccessResponse(response.body());
                } else {
                    Log.i(TAG, "onResponse: Error");
                    callback.onErrorResponse(ApiErrorUtil.parseError(response));
                }
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                Log.i(TAG, "onFailure: Failed" + t.getMessage());
                callback.onErrorResponse(ApiErrorUtil.handleError(Sandesh360App.getAppContext(), t));
                t.printStackTrace();
            }
        });
    }
}
