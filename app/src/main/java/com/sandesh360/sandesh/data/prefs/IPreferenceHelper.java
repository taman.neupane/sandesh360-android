package com.sandesh360.sandesh.data.prefs;

import androidx.annotation.NonNull;

import com.sandesh360.sandesh.data.model.User;

/**
 * This is SharedPreference helper interface
 * Created by Mobarak. on 26-Sep-17.
 *
 * @author Atom AP Ltd.
 */

public interface IPreferenceHelper {

    void setAuthToken(@NonNull String token);

    String getAuthToken();

    void setTerminalId(@NonNull String terminalId);

    String getTerminalId();

    void setFcmToken(@NonNull String token);

    String getFcmToken();

    void setLocalePersistData(String language);

    String getLocalePersistedData();

    void setUserInfo(User user, boolean isFbLogin);

    String getUserId();

    void setFirstLaunch(boolean isFirstLaunch);

    Boolean isFirstLaunch();

    String getImageUrl();

    boolean isLoggedin();

    boolean isFbLogin();

    void removeUserInfo();

    void setLaunchFirstTime(boolean isFirstTime);

    boolean isLaunchFirstTime();

    void setReferralCode(@NonNull String token);

    String getReferralCode();
}
