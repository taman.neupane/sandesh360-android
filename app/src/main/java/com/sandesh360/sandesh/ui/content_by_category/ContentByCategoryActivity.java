package com.sandesh360.sandesh.ui.content_by_category;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseActivity;
import com.sandesh360.sandesh.ui.base.IWindowBarController;
import com.sandesh360.sandesh.ui.content.container.ContentContainerFragment;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;
import com.sandesh360.sandesh.utils.AnimationUtil;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.LocaleUtil;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ContentByCategoryActivity extends BaseActivity<ContentByCategoryViewModel> implements ContentByCategoryNavigator, IWindowBarController {
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private Unbinder unbinder;
    private Long categoryId;
    private String categoryTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            LocaleUtil.onAttach(this);
        } catch (Exception e) {

        }

        setContentView(R.layout.activity_content_by_category);

        if (getIntent() != null) {
            categoryId = getIntent().getLongExtra(Constants.BUNDLE_KEY_CATEGORY_ID, Constants.DEFAULT_CATEGORY_ID);
            categoryTitle = getIntent().getStringExtra(Constants.BUNDLE_KEY_CATEGORY_TITLE);
        }
        unbinder = ButterKnife.bind(this);
        initializedToolbar();
        setTitleText(categoryTitle);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, ContentContainerFragment.newInstance(PreferenceType.CATEGORY, categoryId), Constants.CURRENT_FRAGMENT)
                    .commit();
        }
//        Slidr.attach(this);
    }


    @Override
    public ContentByCategoryViewModel getViewModel() {
        return null;
    }

    @Override
    public void setToolbarVisibility(boolean isVisible) {
        if (appBar != null) {
            if (isVisible) {
                AnimationUtil.appBarShow(appBar);
            } else {
                AnimationUtil.appBarHide(appBar);
            }
        }
    }

    @Override
    public boolean isToolbarVisible() {
        return appBar != null ? (appBar.getVisibility() == View.VISIBLE) : false;
    }

    @Override
    public void setTitleText(@NonNull String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void initializedToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setBackArrowVisibility(boolean isVisible) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
