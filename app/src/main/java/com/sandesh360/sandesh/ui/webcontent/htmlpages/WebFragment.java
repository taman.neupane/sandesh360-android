package com.sandesh360.sandesh.ui.webcontent.htmlpages;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.utils.Constants;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;


/**
 * A simple {@link Fragment} subclass.
 */
public class WebFragment extends BaseFragment<WebPageViewModel> implements WebPageNavigator {

    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.p_bar)
    ProgressBar progressBar;
    @BindView(R.id.tv_error)
    TextView tvError;
    private WebPageViewModel mViewModel;
    private String contentUrl;
    private boolean fromPrivacy;


    public WebFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            contentUrl = getArguments().getString(Constants.BUNDLE_KEY_CONTENT_URL);
            fromPrivacy = getArguments().getBoolean(Constants.BUNDLE_KEY_FROM_PRIVACY);
        }

        if (fromPrivacy) {
            setHasOptionsMenu(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (fromPrivacy) {
            setToolbarVisibility(true);
            setTitleText(getString(R.string.nav_text_privacy_policy));
        }
        webView.onResume();
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (fromPrivacy)
            menu.clear();
    }

    public static WebFragment newInstance(String contentUrl) {
        WebFragment fragment = new WebFragment();
        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_KEY_CONTENT_URL, contentUrl);
        fragment.setArguments(args);
        return fragment;
    }

    public static WebFragment newInstance(String contentUrl, boolean fromPrivacy) {
        WebFragment fragment = new WebFragment();
        Bundle args = new Bundle();
        args.putString(Constants.BUNDLE_KEY_CONTENT_URL, contentUrl);
        args.putBoolean(Constants.BUNDLE_KEY_FROM_PRIVACY, fromPrivacy);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.web_page_fragment, container, false);
        initializedView(view);
        initializedWebPage();
        return view;
    }


    private void initializedWebPage() {
        setWebViewVisibility(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int progress) {
                if (progressBar != null)
                    progressBar.setProgress(progress);

                if (progress == 100) {
                    setProgressBarVisibility(false);
                }
            }
        });
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        webView.loadUrl(contentUrl);
    }

    private void setErrorVisibility(boolean isVisible) {
        if (tvError != null) {
            tvError.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    private void setProgressBarVisibility(boolean isVisible) {
        if (progressBar != null) {
            progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }


    private void setWebViewVisibility(boolean isVisible) {
        if (webView != null) {
            webView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }


    @Override
    public WebPageViewModel getViewModel() {
//        mViewModel = new WebPageViewModel(getActivity(), this);
//        mViewModel = (WebPageViewModel) new ViewModelFactory(mViewModel).create(WebPageViewModel.class);
        return mViewModel;
    }

    @Override
    public void onPause() {
        super.onPause();
        webView.onPause();
    }
}
