package com.sandesh360.sandesh.ui.content.container.adapter;

import android.os.Parcelable;
import android.util.Log;
import android.view.ViewGroup;

import com.sandesh360.sandesh.ui.content.ContentFragment;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by Mobarak on October 31, 2018
 *
 * @author Sandesh360
 */
public class ContentPagerAdapter extends FragmentStatePagerAdapter  {

    private static final String TAG = ContentPagerAdapter.class.getSimpleName();


    private List<Fragment> fragments;

    public ContentPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        Log.i(TAG, position + "");
        return fragments.get(position);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {}


    @Override
    public int getCount() {
        return fragments.size();
    }

}
