/*
 * Copyright 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sandesh360.sandesh.ui.search;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.IWindowBarController;
import com.sandesh360.sandesh.utils.AnimationUtil;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.LocaleUtil;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * This Activity class is used to display a grid of search results for recipe searches.
 */
public class SearchActivity extends AppCompatActivity implements IWindowBarController {

    private String mQuery;

    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            LocaleUtil.onAttach(this);
        } catch (Exception e) {

        }
        setContentView(R.layout.activity_search);
        onNewIntent(getIntent());

        unbinder = ButterKnife.bind(this);
        initializedToolbar();
        setTitleText(mQuery);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, SearchFragment.newInstance(mQuery), Constants.CURRENT_FRAGMENT)
                    .commit();
        }
//        Slidr.attach(this);
    }

    protected void onNewIntent(Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_SEARCH)) {
            mQuery = intent.getStringExtra(SearchManager.QUERY);
        }
    }

    @Override
    public void setToolbarVisibility(boolean isVisible) {
        if (appBar != null) {
            if (isVisible) {
                AnimationUtil.appBarShow(appBar);
            } else {
                AnimationUtil.appBarHide(appBar);
            }
        }
    }

    @Override
    public boolean isToolbarVisible() {
        return appBar != null ? (appBar.getVisibility() == View.VISIBLE) : false;
    }

    @Override
    public void setTitleText(@NonNull String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public void initializedToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setBackArrowVisibility(boolean isVisible) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

}
