package com.sandesh360.sandesh.ui.category.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.ui.base.OnItemClickListener;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mobarak on November 05, 2018
 *
 * @author Sandesh360
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private Context context;
    private List<Category> categories;
    private OnItemClickListener itemClickListener;

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public CategoryAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.categories = categories;

    }


    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.category_row, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = categories.get(position);
        holder.tvTitle.setText(category.getCategoryName());
        UtilityFunctions.loadImage(context, ApiUrls.CATEGORY_ICON_URL + category.getIcon(), R.drawable.category_placeholder, holder.ivIcon);
        holder.row.setOnClickListener(v -> {
            if (itemClickListener != null) {
                itemClickListener.onItemClicked(category);
            }
        });


    }

    public void addCategories(List<Category> categoryList) {
        if (categoryList == null) return;
        this.categories = categoryList;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return categories == null ? 0 : categories.size();
    }

    protected static class CategoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_category_name)
        TextView tvTitle;
        @BindView(R.id.iv_category_icon)
        ImageView ivIcon;
        @BindView(R.id.row)
        ConstraintLayout row;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
