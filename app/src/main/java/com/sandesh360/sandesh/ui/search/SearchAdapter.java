package com.sandesh360.sandesh.ui.search;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.ui.base.OnItemClickListener;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mobarak on December 06, 2018
 *
 * @author Sandesh360
 */
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private Context context;
    private List<Content> contents;

    private OnItemClickListener listener;

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public SearchAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_row, parent, false);
        return new SearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {

        Content content = contents.get(position);
        holder.tvTitle.setText(content.getTitle());
        holder.tvTime.setVisibility(View.GONE);
        if (content.getImage() != null) {
            UtilityFunctions.loadImage(context,
                    ApiUrls.CONTENT_ICON_URL + content.getImage().getImageUrl(),
                    R.drawable.category_placeholder, holder.ivIcon);
        }

        holder.row.setOnClickListener(v -> {
            if (listener != null) listener.onItemClicked(content);
        });

    }

    public void addItems(List<Content> contentList) {
        this.contents = contentList;
        this.notifyDataSetChanged();
    }

    public List<Content> getContents() {
        return contents;
    }

    @Override
    public int getItemCount() {
        return contents == null ? 0 : contents.size();
    }

    protected static class SearchViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_content_image)
        ImageView ivIcon;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.row)
        LinearLayout row;

        public SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
