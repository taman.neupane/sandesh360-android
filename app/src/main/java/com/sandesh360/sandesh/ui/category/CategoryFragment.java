package com.sandesh360.sandesh.ui.category;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.OnItemClickListener;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;
import com.sandesh360.sandesh.ui.category.adapter.CategoryAdapter;
import com.sandesh360.sandesh.ui.category.adapter.GridSpacingItemDecoration;
import com.sandesh360.sandesh.ui.content.container.ContentContainerFragment;
import com.sandesh360.sandesh.ui.content_by_category.ContentByCategoryActivity;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;



public class CategoryFragment extends BaseFragment<CategoryViewModel> implements CategoryNavigator, OnItemClickListener<Category> {

    private static final String TAG =CategoryFragment.class.getSimpleName();
    @BindView(R.id.recycle_view)
    RecyclerView recyclerView;

    @BindView(R.id.card_poll)
    CardView cardPoll;

    CategoryAdapter adapter;
    private CategoryViewModel mViewModel;

    private final CompositeDisposable mDisposable = new CompositeDisposable();

    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.category_fragment, container, false);
        initializedView(view);
        setClickListener();
        setUpRecyclerView();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem refresh = menu.findItem(R.id.item_refresh);
        MenuItem goUp = menu.findItem(R.id.item_go_up);
        if(refresh != null) refresh.setVisible(false);
        if(goUp != null) goUp.setVisible(false);
    }


    @Override
    protected void setClickListener() {
        cardPoll.setOnClickListener(v -> {
            UtilityFunctions.changeFragment(getActivity(), ContentContainerFragment.newInstance(PreferenceType.POLL, Constants.DEFAULT_CATEGORY_ID), true, true);
            setBackArrowVisibility(true);
        });
    }

    private void setUpRecyclerView() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3,
                UtilityFunctions.dpToPx(8, getActivity()), false));
        adapter = new CategoryAdapter(getActivity(), null);
        adapter.setItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel().invokeActiveCategoryApi();
        mDisposable.add(mViewModel.getCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categories -> {
                    if (categories.isEmpty()) {
                        UtilityFunctions.showProgressBar(getActivity());
                    } else {
                        loadCategories(categories);
                        UtilityFunctions.stopProgressBar(getActivity());
                    }
                }, throwable -> Log.e(TAG, "No data found", throwable)));
    }

    @Override
    public CategoryViewModel getViewModel() {
        mViewModelFactory = Injection.provideViewModelFactory(getActivity(), this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(CategoryViewModel.class);
        return mViewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleText(getString(R.string.nav_text_categories));
        setToolbarVisibility(true);
        setBackArrowVisibility(false);
    }

    @Override
    public void onItemClicked(Category item) {
        if (item != null) {
            Intent intent = new Intent(getActivity(), ContentByCategoryActivity.class);
            intent.putExtra(Constants.BUNDLE_KEY_CATEGORY_ID, item.getId());
            intent.putExtra(Constants.BUNDLE_KEY_CATEGORY_TITLE, item.getCategoryName());
            getActivity().startActivity(intent);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mDisposable != null) {
            mDisposable.clear();
        }
    }

    @Override
    public void loadCategories(List<Category> categories) {
        adapter.addCategories(categories);
    }

    @Override
    public void insertCategories(List<Category> categories) {
        mDisposable.add(mViewModel.insertCategories(categories)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.e(TAG, "Successfully inserted."),
                        throwable -> Log.e(TAG, "Insertion failed", throwable)));
    }
}

