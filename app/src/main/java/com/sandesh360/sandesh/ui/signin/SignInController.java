package com.sandesh360.sandesh.ui.signin;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.sandesh360.sandesh.data.model.User;
import com.sandesh360.sandesh.ui.base.ILoginCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import static com.sandesh360.sandesh.utils.Constants.RC_SIGN_IN;

/**
 * Created by Mobarak on November 16, 2018
 *
 * @author Sandesh360
 */
public class SignInController {
    private static final String TAG = SignInController.class.getSimpleName();
    private static SignInController mInstance;
    private FragmentActivity context;
    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInAccount account;

    private CallbackManager callbackManager;
    private User user;
    private List<String> userPermissions = Arrays.asList("public_profile", "email", "user_birthday", "user_location");
    private ILoginCallback listener;


    private SignInController(FragmentActivity context) {
        this.context = context;
        googleSignInSetup();
        facebookSdkSetup();
    }


    public static synchronized SignInController getInstance(FragmentActivity context) {
        if (mInstance == null) {
            mInstance = new SignInController(context);
        }
        return mInstance;
    }


    private void googleSignInSetup() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(context, gso);
    }


    public void googleSignIn(ILoginCallback listener) {
        this.listener = listener;
        if (context != null && mGoogleSignInClient != null) {
            if (!getLastGoogleSignIn()) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                context.startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        }
    }

    public void googleSignOut(ILoginCallback listener) {
        this.listener = listener;
        if (context != null && mGoogleSignInClient != null) {
            mGoogleSignInClient.signOut()
                    .addOnCompleteListener(context, task -> {
                        if (listener != null) {
                            if (task.isSuccessful()) {
                                listener.onLogoutSuccess();
                            } else {
                                listener.onFail(false);
                            }
                        }
                    });
        }
    }

    private void revokeAccess() {
        if (context != null && mGoogleSignInClient != null) {
            mGoogleSignInClient.revokeAccess()
                    .addOnCompleteListener(context, task -> {
                    });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (callbackManager != null && requestCode != RC_SIGN_IN) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            if (task.isSuccessful()) {
                handleGoogleSignInResult(task);
            } else {
                if (listener != null)
                    listener.onFail(true);
            }
        }

    }

    private boolean getLastGoogleSignIn() {
        account = GoogleSignIn.getLastSignedInAccount(context);
        return account != null;
    }

    private void handleGoogleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            account = completedTask.getResult(ApiException.class);
            if (account != null) {
//                Log.i(TAG, "user info: " + account.zab());
                user = new User()
                        .setUserId(account.getId())
                        .setName(account.getDisplayName())
                        .setEmail(account.getEmail())
                        .setImageUrl(account.getPhotoUrl().toString())
                        .setPassword(account.getIdToken());
                if (listener != null) listener.onLoginSuccess(user);

            }
        } catch (ApiException e) {
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            if (listener != null) listener.onFail(false);
        }
    }

    private User parseUserInfoFbLoginResult(JSONObject object) {
        User user = new User();
        try {
            if (object != null) {
                if (object.has("id")) {
                    user.setUserId(object.getString("id"));
                }
                if (object.has("name")) {
                    user.setName(object.getString("name"));
                }
                if (object.has("email")) {
                    user.setEmail(object.getString("email"));
                }
                if (object.has("location")) {
                    JSONObject location = object.getJSONObject("location");
                    user.setAddress(location != null && location.has("name") ? location.getString("name") : "");
                }
                user.setImageUrl("https://graph.facebook.com/" + user.getUserId() + "/picture?type=large");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return user;
    }

    private void facebookSdkSetup() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.i(TAG, "LoginResult: " + object.toString());
                            user = parseUserInfoFbLoginResult(object);
                            if (listener != null) listener.onLoginSuccess(user);

                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, name, email, link, gender, birthday, location");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                if (listener != null) listener.onFail(true);
            }

            @Override
            public void onError(FacebookException exception) {
                if (listener != null) listener.onFail(true);
            }

        });
        fbLogoutCallback();
    }

    public void fbSignIn(ILoginCallback listener) {
        this.listener = listener;
        if (context != null && !isFbLoggedIn())
            LoginManager.getInstance().logInWithReadPermissions(context, userPermissions);
    }

    public void fbLogout(ILoginCallback listener) {
        this.listener = listener;
        if (context != null && isFbLoggedIn())
            LoginManager.getInstance().logOut();
    }

    private boolean isFbLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null && !accessToken.isExpired();
    }

    private void fbLogoutCallback() {
        new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                       AccessToken currentAccessToken) {
                if (listener != null) {
                    if (currentAccessToken == null)
                        listener.onLogoutSuccess();
                    else
                        listener.onFail(false);
                }

            }
        };
    }


}
