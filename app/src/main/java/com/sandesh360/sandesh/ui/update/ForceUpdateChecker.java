package com.sandesh360.sandesh.ui.update;

import android.content.Context;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import androidx.annotation.NonNull;

public class ForceUpdateChecker {
    private static final String TAG = ForceUpdateChecker.class.getSimpleName();

    public static final String KEY_UPDATE_REQUIRED = "force_update_required";
    public static final String KEY_CURRENT_VERSION = "update_current_version";
    public static final String KEY_UPDATE_URL = "update_store_url";
    public static final String KEY_UPDATE_DESCRIPTION = "update_description";
    public static final String KEY_UPDATE_TITLE = "update_title";

    private OnUpdateNeededListener onUpdateNeededListener;
    private Context context;

    public interface OnUpdateNeededListener {
        void onUpdateNeeded(String updateTitle, String updateDescription, boolean isForceUpdate, String updateUrl);

        void onUpdateNotNeeded();
    }

    public static Builder with(@NonNull Context context) {
        return new Builder(context);
    }

    private ForceUpdateChecker(@NonNull Context context,
                               OnUpdateNeededListener onUpdateNeededListener) {
        this.context = context;
        this.onUpdateNeededListener = onUpdateNeededListener;
    }

    private void check() {
        final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();

//        if (remoteConfig.getBoolean(KEY_UPDATE_REQUIRED)) {
            String currentVersion = remoteConfig.getString(KEY_CURRENT_VERSION);
            String appVersion = getAppVersion(context);
            String updateUrl = remoteConfig.getString(KEY_UPDATE_URL);
            String updateTitle = remoteConfig.getString(KEY_UPDATE_TITLE);
            String updateDescription = remoteConfig.getString(KEY_UPDATE_DESCRIPTION);
            boolean isForceUpdateRequired = remoteConfig.getBoolean(KEY_UPDATE_REQUIRED);

            if(onUpdateNeededListener != null) {
                Log.d("AppVersion", appVersion);
                if (!TextUtils.equals(currentVersion, appVersion)) {
                    onUpdateNeededListener.onUpdateNeeded(updateTitle,updateDescription,isForceUpdateRequired,updateUrl);
                } else {
                    onUpdateNeededListener.onUpdateNotNeeded();
                }
            }
//        }
    }

    private String getAppVersion(Context context) {
        String result = "";

        try {
            result = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0)
                    .versionName;
            result = result.replaceAll("[a-zA-Z]|-", "");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, e.getMessage());
        }

        return result;
    }

    public static class Builder {

        private Context context;
        private OnUpdateNeededListener onUpdateNeededListener;

        Builder(Context context) {
            this.context = context;
        }

        public Builder onUpdateNeeded(OnUpdateNeededListener onUpdateNeededListener) {
            this.onUpdateNeededListener = onUpdateNeededListener;
            return this;
        }

        public ForceUpdateChecker build() {
            return new ForceUpdateChecker(context, onUpdateNeededListener);
        }

        public ForceUpdateChecker check() {
            ForceUpdateChecker forceUpdateChecker = build();
            forceUpdateChecker.check();

            return forceUpdateChecker;
        }
    }
}
