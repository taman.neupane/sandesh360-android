package com.sandesh360.sandesh.ui.introduction.splash;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.entity.Language;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;
import com.sandesh360.sandesh.ui.introduction.IntroContainerFragment;
import com.sandesh360.sandesh.ui.navdrawer.MainActivity;
import com.sandesh360.sandesh.ui.update.ForceUpdateChecker;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SplashFragment extends BaseFragment<SplashViewModel> implements SplashNavigator, ForceUpdateChecker.OnUpdateNeededListener {
    private static final String TAG = SplashFragment.class.getSimpleName();

    private static final long REGULAR_SPLASH_TIME = 3000;
    private SplashViewModel mViewModel;

    private final CompositeDisposable mDisposable = new CompositeDisposable();


    public static SplashFragment newInstance() {
        return new SplashFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splash_fragment, container, false);
        initializedView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public SplashViewModel getViewModel() {
        mViewModelFactory = Injection.provideViewModelFactory(getActivity(), this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SplashViewModel.class);
        return mViewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        ForceUpdateChecker.with(this.requireContext()).onUpdateNeeded(this).check();
    }

    @Override
    public void onStop() {
        super.onStop();
        mDisposable.clear();
    }

    @Override
    public void gotoMainScreen() {
        new Handler().postDelayed(() -> {
            if (getActivity() != null) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        }, REGULAR_SPLASH_TIME);
    }

    @Override
    public void gotoIntroScreen() {
        new Handler().postDelayed(() -> {
            if (getActivity() != null) {
                UtilityFunctions.changeFragment(getActivity(),
                        new IntroContainerFragment(),
                        false, true);
            }
        }, REGULAR_SPLASH_TIME);
    }

    @Override
    public void screenNavigation() {
//        if (DataManager.getInstance().isLaunchFirstTime()) {
//            gotoIntroScreen();
//        } else {
//            gotoMainScreen();
//        }
        gotoMainScreen();

    }

    @Override
    public void insertLanguages(List<Language> languages) {
        // Subscribe to updating the user name.
        // Re-enable the button once the user name has been updated

        if (languages.size() > 0)
            mDisposable.add(mViewModel.insertLanguage(languages)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                                Log.i(TAG, "All Language Inserted");
                                mViewModel.invokePreferencesApi();
                            },
                            throwable -> Log.e(TAG, "Insertion failed", throwable)));

    }

    @Override
    public void insertPreferences(List<Preference> preferences) {
        // Subscribe to updating the user name.
        // Re-enable the button once the user name has been updated
        if (preferences.size() > 0)
            mDisposable.add(mViewModel.insertPreference(preferences)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                                Log.i(TAG, "All Preference Inserted");
                                screenNavigation();
                            },
                            throwable -> {
                                Log.e(TAG, "Insertion failed", throwable);
                                screenNavigation();
                            }));
        else {
            screenNavigation();
        }
    }

    @Override
    public void onUpdateNeeded(String updateTitle, String updateDescription, boolean isForceUpdate, String updateUrl) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.requireContext())
                .setTitle(updateTitle)
                .setCancelable(false)
                .setMessage(updateDescription)
                .setPositiveButton("Update",
                        (dialog12, which) -> redirectStore(updateUrl));


        if (!isForceUpdate) {
            dialog.setNegativeButton("No, thanks",
                    (dialog1, which) -> startDoingNormalWork());
        }

        if (DataManager.getInstance().isFirstLaunch()) {
            startDoingNormalWork();
        } else {
            dialog.show();
        }
    }

    @Override
    public void onUpdateNotNeeded() {
        startDoingNormalWork();
    }


    private void startDoingNormalWork() {
        getViewModel().invokeLanguageApi();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
