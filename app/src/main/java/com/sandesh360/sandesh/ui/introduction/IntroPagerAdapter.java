package com.sandesh360.sandesh.ui.introduction;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.sandesh360.sandesh.ui.base.Sandesh360App;
import com.sandesh360.sandesh.ui.introduction.language.LanguageSettingFragment;
import com.sandesh360.sandesh.ui.introduction.moto.MotoFragment;
import com.sandesh360.sandesh.ui.introduction.no_intenet.NoInternetFragment;
import com.sandesh360.sandesh.utils.UtilityFunctions;

/**
 * Created by Mobarak on October 31, 2018
 *
 * @author Sandesh360
 */
public class IntroPagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = IntroPagerAdapter.class.getSimpleName();

    private static final int NUMBER_OF_PAGES = 2;

    public IntroPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = LanguageSettingFragment.newInstance();
                break;
            case 1:
                fragment = MotoFragment.newInstance();
                break;
            default:
                fragment = NoInternetFragment.newInstance();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return getSize();
    }

    private int getSize() {
        return UtilityFunctions.isNetworkAvailable(Sandesh360App.getAppContext()) ? NUMBER_OF_PAGES : NUMBER_OF_PAGES + 1;
    }
}
