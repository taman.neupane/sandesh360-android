package com.sandesh360.sandesh.ui.feedback;


import com.sandesh360.sandesh.ui.base.IBaseNavigator;

/**
 * Created by Mobarak on October 19, 2018
 *
 * @author Sandesh360
 */
public interface FeedbackNavigator extends IBaseNavigator {

    void onEmailError(String error);

    void onCommentError(String error);

    void showSuccessMessage(String message);

    void showFailedMessage(String message);

    void successFailVisibility(String message);

}
