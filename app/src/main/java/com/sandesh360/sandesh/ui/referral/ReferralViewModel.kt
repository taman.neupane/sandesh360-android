package com.sandesh360.sandesh.ui.referral


import androidx.fragment.app.FragmentActivity
import com.sandesh360.sandesh.data.DataManager
import com.sandesh360.sandesh.data.db.IDbHelper
import com.sandesh360.sandesh.data.network.ApiError
import com.sandesh360.sandesh.data.network.ApiUrls
import com.sandesh360.sandesh.data.network.IResponseCallback
import com.sandesh360.sandesh.data.network.model.BaseResponse
import com.sandesh360.sandesh.data.network.model.ReferralRequest
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel
import com.sandesh360.sandesh.utils.UtilityFunctions
import android.os.Build


class ReferralViewModel(private var mContext: FragmentActivity, private val navigator: ReferralNavigator, private val dataSource: IDbHelper) : BaseViewModel<ReferralNavigator>(mContext, navigator, dataSource), IResponseCallback<BaseResponse> {

    private lateinit var prefReferralCode: String


    override fun onSuccessResponse(response: BaseResponse?) {
        UtilityFunctions.stopProgressBar(context)
        DataManager.getInstance().referralCode = prefReferralCode
        getNavigator().showSuccessMessage("Successfully added referral code")
    }

    override fun onErrorResponse(error: ApiError?) {
        UtilityFunctions.stopProgressBar(context)
        getNavigator().showFailedMessage(error?.message)
    }

    fun submitReferral(referralCode: String) {
        this.prefReferralCode = referralCode
        invokeAPI(getBody("ANDROID", Build.VERSION.RELEASE, referralCode, getDeviceName()))
    }


    private fun getBody(deviceType: String, deviceOSVersion: String, code: String, device: String): ReferralRequest {
        return ReferralRequest()
                .setReferralDeviceType(deviceType)
                .setReferralDeviceOSVersion(deviceOSVersion)
                .setReferralCode(code)
                .setReferralDeviceName(device)
    }

    private fun invokeAPI(body: ReferralRequest) {
        UtilityFunctions.showProgressBar(context)
        DataManager.getInstance().invokeReferralApi(ApiUrls.SEND_REFERRAL_CODE, body, this)
    }

    fun getDeviceName(): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else {
            capitalize(manufacturer) + " " + model
        }
    }


    private fun capitalize(s: String?): String {
        if (s == null || s.isEmpty()) {
            return ""
        }
        val first = s[0]
        return if (Character.isUpperCase(first)) {
            s
        } else {
            Character.toUpperCase(first) + s.substring(1)
        }
    }
}
