package com.sandesh360.sandesh.ui.content.container;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.NativeAds;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.data.network.ApiError;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.ContentRequest;
import com.sandesh360.sandesh.data.network.model.ContentResponse;
import com.sandesh360.sandesh.data.network.model.NewsBottomAds;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;
import com.sandesh360.sandesh.utils.LocaleUtil;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class ContentContainerViewModel extends BaseViewModel<ContentContainerNavigator> implements IResponseCallback<ContentResponse> {
    public ContentContainerViewModel(FragmentActivity context, ContentContainerNavigator navigator, IDbHelper dataSource) {
        super(context, navigator, dataSource);
    }

    private ContentResponse contentResponse;
    private ContentType contentType = ContentType.PREFERENCE;
    private List<NativeAds> adsList = new ArrayList<>();
    private List<NewsBottomAds> newsBottomAds = new ArrayList<>();
    private int frequency;
    private long lastUpdated = 0;

    public int getFrequency() {
        return frequency;
    }

    public List<NativeAds> getAdsList() {
        return adsList;
    }

    public void setAdsList(List<NativeAds> adsList) {
        this.adsList = adsList;
    }

    public List<NewsBottomAds> getNewsBottomAds() {
        return newsBottomAds;
    }

    private void setBottomAds(List<NewsBottomAds> newsBottomAds) {
        this.newsBottomAds = newsBottomAds;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String setTitleByPreferenceType(PreferenceType type) {
        String title = "";
        switch (type) {
            case MY_FEED:
                title = context.getString(R.string.nav_text_my_feed);
                break;
            case ALL_NEWS:
                title = context.getString(R.string.nav_text_all_news);
                break;
            case TOP_STORIES:
                title = context.getString(R.string.nav_text_top_stories);
                break;
            case TRENDING:
                title = context.getString(R.string.nav_text_trending);
                break;
            case BOOKMARKS:
                title = context.getString(R.string.nav_text_bookmarks);
                break;
            case UNREAD:
                title = context.getString(R.string.nav_text_unread);
                break;
            case CATEGORY:
                title = context.getString(R.string.nav_text_categories);
                break;
            case POLL:
                title = context.getString(R.string.nav_text_poll);
                break;
            default:
                title = context.getString(R.string.nav_text_search);
                break;
        }
        return title;
    }

    public Flowable<Preference> getPreferenceByName(String name) {
        return dataSource.getPreferenceByName(name);
    }

    public Completable insertContent(List<Content> contents) {
        return dataSource.insertContents(contents);
    }


    public boolean isCategory() {
        return getNavigator().getCategoryId() != null
                && getNavigator().getCategoryId() > 0
                && PreferenceType.CATEGORY == getNavigator().getPreferenceType();
    }

    public void invokeAPI() {
        if (isCategory()) {
            invokeContentApi(getUrl(ContentType.CATEGORY),
                    getRequestBody(getNavigator().getCategoryId(), ContentType.CATEGORY));
        } else if (PreferenceType.MY_FEED == getNavigator().getPreferenceType()
                || PreferenceType.ALL_NEWS == getNavigator().getPreferenceType()
                || PreferenceType.TOP_STORIES == getNavigator().getPreferenceType()
                || PreferenceType.TRENDING == getNavigator().getPreferenceType()) {
            getNavigator().searchPreference();
        } else if (PreferenceType.BOOKMARKS == getNavigator().getPreferenceType()) {
            contentType = ContentType.BOOKMARKS;
            getAllContentFromDb();
        } else if (PreferenceType.UNREAD == getNavigator().getPreferenceType()) {
            contentType = ContentType.UNREAD;
            getAllContentFromDb();
        } else if (PreferenceType.POLL == getNavigator().getPreferenceType()) {
            invokeContentApi(getUrl(ContentType.POLL),
                    getRequestBody(getNavigator().getCategoryId(), ContentType.POLL));
        }
    }

    public String getUrl(ContentType type) {
        String url = "";
        switch (type) {
            case PREFERENCE:
                url = ApiUrls.CONTENT_BY_PREFERENCE_ID;
                break;
            case CATEGORY:
                url = ApiUrls.CONTENT_BY_CATEGORY_ID;
                break;
            case BOOKMARKS:
                url = ApiUrls.BOOKMARK_CONTENT;
                break;
            case POLL:
                url = ApiUrls.POLL_CONTENT;
                break;
        }
        return url;
    }

    public void invokeContentApi(String url, ContentRequest body) {
        UtilityFunctions.showProgressBar(context);
        DataManager.getInstance().invokeActiveContentApi(url, body, this);
    }

    public ContentRequest getRequestBody(long id, ContentType type) {
        ContentRequest body = new ContentRequest();
        body.setLanguageId(LocaleUtil.getLanguageId());
        body.setLastUpdated(lastUpdated);
        switch (type) {
            case PREFERENCE:
                body.setPreferenceId(id);
                break;
            case CATEGORY:
                body.setCategoryId(id);
                break;
            case BOOKMARKS:
                body.setUserId(id);
                break;
            case POLL:
                break;
        }
        return body;
    }

    @Override

    public void onSuccessResponse(ContentResponse response) {
        this.contentResponse = response;
//        getNavigator().insertContent(contentResponse.getContents());
        if (response.getAds() != null) {
            setAdsList(response.getAds());
            frequency = response.getFrequency();
        }
        setBottomAds(response.getNewsBottomAds());
        if (getNavigator().isLoadFromDb()) {
            getNavigator().insertContent(contentResponse.getContents());
        } else {
            getNavigator().loadPager(response.getContents());
            UtilityFunctions.stopProgressBar(context);
        }
    }

    @Override
    public void onErrorResponse(ApiError error) {
        UtilityFunctions.stopProgressBar(context);

    }

    public ContentResponse getContentResponse() {
        return contentResponse;
    }

    public void getAllContentFromDb() {
        new Thread(() -> {
            List<Content> contents = dataSource.getAllContentWithImage(getContentType());
            context.runOnUiThread(() -> getNavigator().loadPager(contents));
        }).start();
    }

    public ContentType getContentType() {
        return contentType;
    }
}
