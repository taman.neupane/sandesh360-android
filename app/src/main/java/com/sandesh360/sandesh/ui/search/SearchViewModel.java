package com.sandesh360.sandesh.ui.search;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.network.ApiError;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.ContentRequest;
import com.sandesh360.sandesh.data.network.model.ContentResponse;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;
import com.sandesh360.sandesh.utils.LocaleUtil;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import io.reactivex.Completable;

public class SearchViewModel extends BaseViewModel<SearchNavigator> implements IResponseCallback<ContentResponse> {

    public SearchViewModel(FragmentActivity context, SearchNavigator navigator, IDbHelper dbHelper) {
        super(context, navigator, dbHelper);
    }

    public Completable insertContent(List<Content> contents) {
        return dataSource.insertContents(contents);
    }

    public void invokeSearchAPI(String query) {
        UtilityFunctions.showProgressBar(context);
        DataManager.getInstance().invokeActiveContentApi(ApiUrls.SEARCH_CONTENT, getRequestBody(query), this);

    }

    public ContentRequest getRequestBody(String query) {
        ContentRequest body = new ContentRequest();
        body.setLanguageId(LocaleUtil.getLanguageId());
        body.setSearchKey(query);
        return body;
    }


    @Override
    public void onSuccessResponse(ContentResponse response) {

        getNavigator().updateUI(response.getContents());
        UtilityFunctions.stopProgressBar(context);

    }

    @Override
    public void onErrorResponse(ApiError error) {
        UtilityFunctions.stopProgressBar(context);
        getNavigator().onError(error.getMessage());
    }
}
