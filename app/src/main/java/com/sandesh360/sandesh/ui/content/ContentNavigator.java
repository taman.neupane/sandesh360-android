package com.sandesh360.sandesh.ui.content;


import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;

/**
 * Created by Mobarak on October 19, 2018
 *
 * @author Sandesh360
 */
public interface ContentNavigator extends IBaseNavigator {

    void setBottomBarVisibility(boolean isVisible);

    boolean isBottomBarVisible();

    boolean isToolbarBottomBarVisible();

    void setToolbarBottomBarVisibility(boolean isVisible);

    void goWebActivity();

    void updateUIAccordingToBookmarkStatus();

    void updateReadStatus();

    void setBookmarkStatus(boolean isBookmark);

    boolean isBookmarked();

    void updateBookmarkStatusByContentId();

    void setVisibityOfPollProgress(boolean isVisible);

    void initPoll();

    void insertPoll(Poll poll);

}
