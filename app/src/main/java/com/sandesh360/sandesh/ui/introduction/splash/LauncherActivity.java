package com.sandesh360.sandesh.ui.introduction.splash;

import android.os.Bundle;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.utils.LocaleUtil;

import androidx.appcompat.app.AppCompatActivity;

public class LauncherActivity extends AppCompatActivity {

    private static final String TAG = LauncherActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            LocaleUtil.onAttach(this);
        } catch (Exception e) {

        }

        setContentView(R.layout.activity_launcher);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, SplashFragment.newInstance())
                    .commitNow();
        }
    }
}
