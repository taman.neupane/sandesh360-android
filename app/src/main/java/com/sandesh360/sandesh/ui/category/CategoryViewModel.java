package com.sandesh360.sandesh.ui.category;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.data.network.ApiError;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.CategoryResponse;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;
import com.sandesh360.sandesh.utils.LocaleUtil;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class CategoryViewModel extends BaseViewModel<CategoryNavigator> implements IResponseCallback<CategoryResponse> {

    private CategoryResponse categoryResponse;

    public CategoryViewModel(FragmentActivity context, CategoryNavigator navigator, IDbHelper dbHelper) {
        super(context, navigator, dbHelper);
    }

    public void invokeActiveCategoryApi() {
        UtilityFunctions.showProgressBar(context);
        DataManager.getInstance().invokeActiveCategoriesApi(ApiUrls.ACTIVE_CATEGORY_URL + LocaleUtil.getLanguageId(), this);
    }


    @Override
    public void onSuccessResponse(CategoryResponse response) {
        this.categoryResponse = response;
        if (response != null)
            getNavigator().insertCategories(response.getCategories());
//        UtilityFunctions.stopProgressBar(context);
    }

    @Override
    public void onErrorResponse(ApiError error) {
        UtilityFunctions.stopProgressBar(context);
    }

    public Completable insertCategories(List<Category> categories) {
        return dataSource.insertCategories(categories);
    }

    public Flowable<List<Category>> getCategories() {
        return dataSource.getCategories(LocaleUtil.getLanguageId());
    }
}
