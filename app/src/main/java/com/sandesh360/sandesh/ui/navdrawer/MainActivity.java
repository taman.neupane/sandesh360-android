package com.sandesh360.sandesh.ui.navdrawer;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.cursoradapter.widget.CursorAdapter;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.model.User;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.ui.base.BaseActivity;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.ILoginCallback;
import com.sandesh360.sandesh.ui.base.IWindowBarController;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;
import com.sandesh360.sandesh.ui.base.viewmodel.ViewModelFactory;
import com.sandesh360.sandesh.ui.category.CategoryFragment;
import com.sandesh360.sandesh.ui.content.container.ContentContainerFragment;
import com.sandesh360.sandesh.ui.dialog.BottomSheet;
import com.sandesh360.sandesh.ui.dialog.SheetType;
import com.sandesh360.sandesh.ui.feedback.FeedbackFragment;
import com.sandesh360.sandesh.ui.privacy_terms.PrivacyAndTermsFragment;
import com.sandesh360.sandesh.ui.referral.ReferralFragment;
import com.sandesh360.sandesh.ui.search.SearchActivity;
import com.sandesh360.sandesh.ui.search.SearchFragment;
import com.sandesh360.sandesh.ui.setting.SettingsFragment;
import com.sandesh360.sandesh.ui.signin.SignInFragment;
import com.sandesh360.sandesh.ui.webcontent.htmlpages.WebFragment;
import com.sandesh360.sandesh.utils.AnimationUtil;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.LocaleUtil;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.sandesh360.sandesh.ui.navdrawer.MenuType.ALL_NEWS;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.BOOKMARKS;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.CATEGORY;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.FEEDBACK;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.MY_FEED;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.OPTIONS;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.POLL;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.PRIVACY;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.SEARCH;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.TERMS_CONDITION;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.TOP_STORIES;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.TRENDING;
import static com.sandesh360.sandesh.ui.navdrawer.MenuType.UNREAD;
import static com.sandesh360.sandesh.utils.Constants.BUNDLE_KEY_NOTIFICATION;
import static com.sandesh360.sandesh.utils.Constants.DEFAULT_CATEGORY_ID;

public class MainActivity extends BaseActivity<NavDrawerViewModel> implements NavigationView.OnNavigationItemSelectedListener, IWindowBarController, NavDrawerNavigator, ILoginCallback, OnMenuVisibilityListener {

    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    private HeaderViewHolder headerViewHolder;
    private ActionBarDrawerToggle toggle;
    private boolean isToolbarNavigationListener;
    private MenuType type;

    public MenuType getType() {
        return type;
    }

    public void setType(MenuType type) {
        this.type = type;
    }

    private Unbinder unbinder;

    private static final String TAG = MainActivity.class.getSimpleName();
    /**
     * Declare search view for character sequence
     */
//    private SearchView searchView;
    private SimpleCursorAdapter mAdapter;
    private OptionMenuType optionMenuType = OptionMenuType.REFRESH;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            LocaleUtil.onAttach(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);


        getViewModel();
        unbinder = ButterKnife.bind(this);
        initializedToolbar();
        initializedDrawer();
        checkLoginStatus();

        if (savedInstanceState == null) {
            Fragment initialFragment;
            if(getIntent().getStringExtra(Constants.BUNDLE_KEY_NOTIFICATION) == null) {
                 initialFragment = ContentContainerFragment.newInstance(PreferenceType.ALL_NEWS, DEFAULT_CATEGORY_ID);
            }else {
                initialFragment = ContentContainerFragment.newInstance(PreferenceType.ALL_NEWS, DEFAULT_CATEGORY_ID,getIntent().getStringExtra(BUNDLE_KEY_NOTIFICATION));
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, initialFragment, Constants.CURRENT_FRAGMENT)
                    .commit();
            setNavItemSelection(navigationView.getMenu().findItem(R.id.nav_all_news));
        } else {
            resolveBackArrowWithFragmentStack();
        }
        DataManager.getInstance().setLaunchFirstTime(false);
        cursorAdapter();

        FirebaseMessaging.getInstance().subscribeToTopic("NotificationTest")
                .addOnCompleteListener(task -> Log.d(TAG, task.isSuccessful() ?
                        "Subscribed" : "subscription failed"));

        // Subscribe a topic
//        FirebaseMessaging.getInstance().subscribeToTopic(LocaleUtil.isEnglish() ? "english" : "bangla")
//                .addOnCompleteListener(task -> Log.d(TAG, task.isSuccessful() ?
//                        "Subscribed" : "subscription failed"));

        // Subscribe a topic
//        FirebaseMessaging.getInstance().unsubscribeFromTopic(LocaleUtil.isEnglish() ? "bangla" : "english")
//                .addOnCompleteListener(task -> Log.d(TAG, task.isSuccessful() ?
//                        "unsubscribed" : "unsubscribed failed"));
    }

    /**
     * Invoke to changed selection state of an drawer item
     *  @param item       drawer menu item
     *
     */
    private void setNavItemSelection(MenuItem item) {
        if (item != null) {
            item.setCheckable(true);
        }
    }

    /**
     * Invoke to initialized drawer
     */
    private void initializedDrawer() {
        navigationView.setCheckedItem(R.id.nav_all_news);
        toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        headerViewHolder = new HeaderViewHolder(navigationView.getHeaderView(0));
    }

    //    @Override
    public void setBackArrowVisibility(boolean isVisible) {
        if (getSupportActionBar() == null) return;

        if (isVisible) {
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (!isToolbarNavigationListener) {
                toggle.setToolbarNavigationClickListener(v -> onBackPressed());
                isToolbarNavigationListener = true;
            }
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            toggle.setDrawerIndicatorEnabled(true);
            toggle.setToolbarNavigationClickListener(null);
            isToolbarNavigationListener = false;
        }

    }


    private void resolveBackArrowWithFragmentStack() {
        setBackArrowVisibility(getSupportFragmentManager().getBackStackEntryCount() > 0);
    }


    /**
     * Set OnClickListener
     */
//    private void setClickListener() {
//        headerViewHolder.ivHeaderBack.setOnClickListener(view -> {
//            onBackPressed();
//        });
//    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getCurrentFragment();
            if (((BaseFragment) fragment).popFragment()) {
                setBackArrowVisibility(false);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.i("MainActivity", "back arrow");
            onBackPressed();
        } else if (item.getItemId() == R.id.item_go_up) {
            onOptionMenuClick(OptionMenuType.GO_UP);

        } else if (item.getItemId() == R.id.item_refresh) {
            onOptionMenuClick(OptionMenuType.REFRESH);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onOptionMenuClick(OptionMenuType type) {
        if (getCurrentFragment() instanceof OnOptionMenuClickListener) {
            ((OnOptionMenuClickListener) getCurrentFragment()).onClickMenu(type);
        }
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentByTag(Constants.CURRENT_FRAGMENT);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_my_feed:
                if (MY_FEED != getType()) {
                    UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.MY_FEED, DEFAULT_CATEGORY_ID), false, false);
                    setType(MY_FEED);
                }
                break;
            case R.id.nav_all_news:
                if (ALL_NEWS != getType()) {
                    if(getIntent().getStringExtra(Constants.BUNDLE_KEY_NOTIFICATION) == null) {
                        UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.ALL_NEWS, DEFAULT_CATEGORY_ID), false, false);
                    }else {
                        UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.ALL_NEWS, DEFAULT_CATEGORY_ID,getIntent().getStringExtra(BUNDLE_KEY_NOTIFICATION)), false, false);
                    }
                    setType(ALL_NEWS);
                }
                break;
            case R.id.nav_top_stories:
                if (TOP_STORIES != getType()) {
                    UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.TOP_STORIES, DEFAULT_CATEGORY_ID), false, false);
                    setType(TOP_STORIES);
                }
                break;
            case R.id.nav_trending:
                if (TRENDING != getType()) {
                    UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.TRENDING, DEFAULT_CATEGORY_ID), false, false);
                    setType(TRENDING);
                }
                break;
            case R.id.nav_bookmarks:
                if (BOOKMARKS != getType()) {
                    UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.BOOKMARKS, DEFAULT_CATEGORY_ID), false, false);
                    setType(BOOKMARKS);
                }
                break;
            case R.id.nav_unread:
                if (UNREAD != getType()) {
                    UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.UNREAD, DEFAULT_CATEGORY_ID), false, false);
                    setType(UNREAD);
                }
                break;
            case R.id.nav_categories:
                if (CATEGORY != type) {
                    UtilityFunctions.changeFragment(this, new CategoryFragment(), false, false);
                    setType(CATEGORY);
                }
                break;
            case R.id.nav_search:
                if (SEARCH != type) {
                    UtilityFunctions.changeFragment(this, new SearchFragment(), false, false);
                    setType(SEARCH);
                }
                break;
            case R.id.nav_poll:
                if (POLL != type) {
                    UtilityFunctions.changeFragment(this, ContentContainerFragment.newInstance(PreferenceType.POLL, DEFAULT_CATEGORY_ID), false, false);
                    setType(POLL);
                }
                break;
            case R.id.nav_feedback:
                if (FEEDBACK != type) {
                    UtilityFunctions.changeFragment(this, new FeedbackFragment(), false, false);
                    setType(FEEDBACK);
                }
                break;
            case R.id.nav_share_app:
                UtilityFunctions.shareApp(this);
                break;
            case R.id.nav_rate_app:
                UtilityFunctions.rateApp(this);
                break;
            case R.id.nav_change_language:
                BottomSheet.newInstance(SheetType.LANGUAGE_CHANGE).show(getSupportFragmentManager(), getString(R.string.nav_text_change_lang));
                break;
            case R.id.nav_terms_conditions:
                if (TERMS_CONDITION != type) {
                    UtilityFunctions.changeFragment(this, PrivacyAndTermsFragment.newInstance(false), false, false);
                    setType(TERMS_CONDITION);
                }
                break;
            case R.id.nav_privacy:
                if (PRIVACY != type) {
                    UtilityFunctions.changeFragment(this, WebFragment.newInstance(ApiUrls.PRIVACY_POLICY, true), false, false);
                    setType(PRIVACY);
                }
                break;
            case R.id.nav_options:
                if (OPTIONS != type) {
                    UtilityFunctions.changeFragment(this, new SettingsFragment(), false, false);
                    setType(OPTIONS);
                }
                break;
            case R.id.nav_logout:
                BottomSheet.newInstance(SheetType.LOGOUT).show(getSupportFragmentManager(), getString(R.string.nav_text_change_lang));
                break;
            case R.id.nav_referral:
                UtilityFunctions.changeFragment(this, ReferralFragment.Companion.newInstance(), true, false);
                break;
//            default:
//                showAppExitDialog();
//                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void cursorAdapter() {
        final String[] from = new String[]{"title"};
        final int[] to = new int[]{android.R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                R.layout.layout_suggestion,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
    }

//    private void showAppExitDialog() {
//        AlertDialog.Builder builder = DialogUtil.getAlertDialogBuilder(MainActivity.this, getString(R.string.label_text_confirm), getString(R.string.label_text_confirm_exit_msg));
//        builder.setPositiveButton(R.string.label_text_yes, (dialogInterface, i) -> {
//            this.finish();
//            dialogInterface.dismiss();
//
//        }).setNegativeButton(R.string.label_text_no, (dialogInterface, i) -> {
//            dialogInterface.dismiss();
//
//        }).create().show();
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);
        searchView.setSuggestionsAdapter(mAdapter);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                intent.setAction(Intent.ACTION_SEARCH);
                intent.putExtra(SearchManager.QUERY, s);
                startActivity(intent);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                populateAdapter(s);
                return false;
            }
        });

        MenuItem refresh = menu.findItem(R.id.item_refresh);
        MenuItem goUp = menu.findItem(R.id.item_go_up);

        if (OptionMenuType.REFRESH == optionMenuType && refresh != null) {
            refresh.setVisible(true);
            goUp.setVisible(false);
        } else if (OptionMenuType.GO_UP == optionMenuType && goUp != null) {
            refresh.setVisible(false);
            goUp.setVisible(true);
        }

        return true;
    }

    private void populateAdapter(String query) {
        new Thread(() -> {
            final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "title"});
            String[] suggestions = getViewModel().getSuggestionList();
            for (int i = 0; i < suggestions.length; i++) {
                if (suggestions[i].toLowerCase().startsWith(query.toLowerCase()))
                    c.addRow(new Object[]{i, suggestions[i]});
            }
            this.runOnUiThread(() -> mAdapter.changeCursor(c));
        }).start();


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }


    @Override
    public void setToolbarVisibility(boolean isVisible) {
        if (appBar != null) {
            if (isVisible) {
                AnimationUtil.appBarShow(appBar);
            } else {
                AnimationUtil.appBarHide(appBar);
            }
        }
    }

    @Override
    public boolean isToolbarVisible() {
        return appBar != null && (appBar.getVisibility() == View.VISIBLE);
    }

    @Override
    public void setTitleText(@NonNull String title) {
        Objects.requireNonNull(getSupportActionBar()).setTitle(title);
    }

    @Override
    public void initializedToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

    }

    @Override
    public NavDrawerViewModel getViewModel() {
        ViewModelFactory mViewModelFactory = Injection.provideViewModelFactory(this, this);
        return ViewModelProviders.of(this, mViewModelFactory).get(NavDrawerViewModel.class);
    }

    @Override
    public void onLoginSuccess(User user) {
        Log.i(TAG, "onLoginSuccess");
        setLogoutVisibility(true);
        UtilityFunctions.loadCircleImage(this, user.getImageUrl(), R.drawable.ic_newspaper_white, headerViewHolder.ivHeaderBack);

    }

    private void checkLoginStatus() {
        setLogoutVisibility(DataManager.getInstance().isLoggedin());
        profilePictureVisibility();

    }

    private void profilePictureVisibility() {
        UtilityFunctions.loadCircleImage(this,
                DataManager.getInstance().getImageUrl(),
                R.drawable.ic_logo,
                headerViewHolder.ivHeaderBack);

    }

    private void setLogoutVisibility(boolean isVisible) {
        if (navigationView != null) {
            MenuItem item = navigationView.getMenu().findItem(R.id.nav_logout);
            if (item != null) {
                item.setVisible(isVisible);
            }
        }
    }

    @Override
    public void onLogoutSuccess() {
        Log.i(TAG, "onLogoutSuccess");
        DataManager.getInstance().removeUserInfo();
        checkLoginStatus();
        if (getCurrentFragment() instanceof SettingsFragment) {
            getCurrentFragment().onResume();
        }

    }

    @Override
    public void onFail(boolean isLogin) {
        Log.i(TAG, "onFail");
        setLogoutVisibility(true);
    }

    @Override
    public void onOptionMenuVisibility(OptionMenuType menuType) {
        if (optionMenuType != null && optionMenuType != menuType) {
            this.optionMenuType = menuType;
            this.invalidateOptionsMenu();
        }
    }

    static class HeaderViewHolder {

        @BindView(R.id.iv_header_back)
        ImageView ivHeaderBack;

        HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment fragment = getCurrentFragment();
        if (fragment instanceof SignInFragment) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
