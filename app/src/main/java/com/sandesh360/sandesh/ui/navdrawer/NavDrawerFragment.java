package com.sandesh360.sandesh.ui.navdrawer;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;

public class NavDrawerFragment extends BaseFragment<NavDrawerViewModel> implements NavDrawerNavigator{

    private NavDrawerViewModel mViewModel;

    public static NavDrawerFragment newInstance() {
        return new NavDrawerFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.nav_drawer_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public NavDrawerViewModel getViewModel() {
//        mViewModel = new NavDrawerViewModel(getActivity(), this);
//        mViewModel = (NavDrawerViewModel) new ViewModelFactory(mViewModel).create(NavDrawerViewModel.class);
        return mViewModel;
    }
}
