package com.sandesh360.sandesh.ui.navdrawer;

import java.io.Serializable;

public enum MenuType implements Serializable {
    MY_FEED,
    ALL_NEWS,
    TOP_STORIES,
    TRENDING,
    BOOKMARKS,
    UNREAD,
    CATEGORY,
    POLL,
    SEARCH,
    FEEDBACK,
    SHARE_APP,
    RATE_APP,
    TERMS_CONDITION,
    PRIVACY,
    OPTIONS
}
