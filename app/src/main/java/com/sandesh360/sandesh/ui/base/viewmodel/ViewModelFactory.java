package com.sandesh360.sandesh.ui.base.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;
import com.sandesh360.sandesh.ui.category.CategoryNavigator;
import com.sandesh360.sandesh.ui.category.CategoryViewModel;
import com.sandesh360.sandesh.ui.content.ContentNavigator;
import com.sandesh360.sandesh.ui.content.ContentViewModel;
import com.sandesh360.sandesh.ui.content.container.ContentContainerNavigator;
import com.sandesh360.sandesh.ui.content.container.ContentContainerViewModel;
import com.sandesh360.sandesh.ui.feedback.FeedbackNavigator;
import com.sandesh360.sandesh.ui.feedback.FeedbackViewModel;
import com.sandesh360.sandesh.ui.introduction.splash.SplashViewModel;
import com.sandesh360.sandesh.ui.navdrawer.NavDrawerNavigator;
import com.sandesh360.sandesh.ui.navdrawer.NavDrawerViewModel;
import com.sandesh360.sandesh.ui.referral.ReferralNavigator;
import com.sandesh360.sandesh.ui.referral.ReferralViewModel;
import com.sandesh360.sandesh.ui.search.SearchNavigator;
import com.sandesh360.sandesh.ui.search.SearchViewModel;

/**
 * Created by RV on 19/07/17.
 * <p>
 * A provider factory that persists ViewModels {@link ViewModel}.
 * Used if the view model has a parameterized constructor.
 */
public class ViewModelFactory<V> implements ViewModelProvider.Factory {

//    private V viewModel;
//
//    public ViewModelFactory(V viewModel) {
//        this.viewModel = viewModel;
//    }
//
//    @Override
//    public <T extends ViewModel> T create(Class<T> modelClass) {
//        if (modelClass.isAssignableFrom(viewModel.getClass())) {
//            return (T) viewModel;
//        }
//        throw new IllegalArgumentException("Unknown class name");
//    }

    private final IDbHelper mDataSource;
    private IBaseNavigator navigator;
    private FragmentActivity context;

    public ViewModelFactory(FragmentActivity context, IBaseNavigator navigator, IDbHelper dataSource) {
        this.context = context;
        this.navigator = navigator;
        this.mDataSource = dataSource;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SplashViewModel.class)) {
            return (T) new SplashViewModel(context, navigator, mDataSource);
        } else if (modelClass.isAssignableFrom(ContentContainerViewModel.class)) {
            return (T) new ContentContainerViewModel(context, (ContentContainerNavigator) navigator, mDataSource);
        } else if (modelClass.isAssignableFrom(ContentViewModel.class)) {
            return (T) new ContentViewModel(context, (ContentNavigator) navigator, mDataSource);
        } else if (modelClass.isAssignableFrom(CategoryViewModel.class)) {
            return (T) new CategoryViewModel(context, (CategoryNavigator) navigator, mDataSource);
        }else if (modelClass.isAssignableFrom(FeedbackViewModel.class)) {
            return (T) new FeedbackViewModel(context, (FeedbackNavigator) navigator, mDataSource);
        }else if (modelClass.isAssignableFrom(NavDrawerViewModel.class)) {
            return (T) new NavDrawerViewModel(context, (NavDrawerNavigator) navigator, mDataSource);
        }else if (modelClass.isAssignableFrom(SearchViewModel.class)) {
            return (T) new SearchViewModel(context, (SearchNavigator) navigator, mDataSource);
        }else if (modelClass.isAssignableFrom(ReferralViewModel.class)) {
            return (T) new ReferralViewModel(context, (ReferralNavigator) navigator, mDataSource);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
