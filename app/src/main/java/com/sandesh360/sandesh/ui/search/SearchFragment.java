package com.sandesh360.sandesh.ui.search;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.OnItemClickListener;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;
import com.sandesh360.sandesh.ui.content.container.ContentContainerFragment;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class SearchFragment extends BaseFragment<SearchViewModel> implements SearchNavigator {
    private static final String TAG = SearchFragment.class.getSimpleName();
    private static final String BUNDLE_KEY_QUERY = "bundle_key_query";
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private SearchViewModel mViewModel;
    private String query;
    @BindView(R.id.recycle_view)
    RecyclerView recyclerView;
    @BindView(R.id.tv_not_found)
    TextView tvNoContentFound;
    private SearchAdapter adapter;

    public static SearchFragment newInstance(String query) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_KEY_QUERY, query);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);
        initializedView(view);
        setUpRecyclerView();
        return view;
    }

    private void setUpRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        adapter = new SearchAdapter(getActivity());
        adapter.setListener(listener);
        recyclerView.setAdapter(adapter);
    }

    private OnItemClickListener<Content> listener = item -> {
        UtilityFunctions.changeFragment(getActivity(),
                ContentContainerFragment.newInstance(PreferenceType.SEARCH, adapter.getContents()),
                true, false);
    };


    @Override
    public SearchViewModel getViewModel() {
        mViewModelFactory = Injection.provideViewModelFactory(getActivity(), this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(SearchViewModel.class);
        return mViewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleText(query);
        setToolbarVisibility(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        mDisposable.clear();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            query = getArguments().getString(BUNDLE_KEY_QUERY);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel();
        if (UtilityFunctions.isEmpty(query)) {
            tvNoContentFound.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            mViewModel.invokeSearchAPI(query);
        }

    }


    @Override
    public void updateUI(List<Content> contents) {
        try {
            if (contents == null || contents.size() <= 0) {
                tvNoContentFound.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            } else {
                tvNoContentFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter.addItems(contents);
                insertContent(contents);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onError(String error) {
        tvNoContentFound.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void insertContent(List<Content> contents) {
        mDisposable.add(mViewModel.insertContent(contents)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.e(TAG, "Successfully inserted.");
                        },
                        throwable -> {
                            Log.e(TAG, "Insertion failed", throwable);
                        }));
    }
}
