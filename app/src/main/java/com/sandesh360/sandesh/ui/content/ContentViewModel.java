package com.sandesh360.sandesh.ui.content;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.sandesh360.sandesh.BuildConfig;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.network.ApiError;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.PollRequestBody;
import com.sandesh360.sandesh.data.network.model.PollResponse;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

import java.io.File;
import java.io.FileOutputStream;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class ContentViewModel extends BaseViewModel<ContentNavigator> implements IResponseCallback<PollResponse> {

    private static final String TAG = ContentViewModel.class.getSimpleName();

    public ContentViewModel(FragmentActivity context, ContentNavigator navigator, IDbHelper dbHelper) {
        super(context, navigator, dbHelper);
    }

    public Flowable<Boolean> getBookmarkedStatus(long contentId) {
        return dataSource.isBookmarked(contentId);
    }

    public Flowable<Poll> getPollByQuestionId(Integer questionId) {
        return dataSource.getPollByQuestionId(questionId);
    }

    public Completable insertPoll(Poll poll) {
        return dataSource.insertPoll(poll);
    }


    public Completable updateBookmarkStatusByContentId(long contentId, boolean isBookmark) {
        return dataSource.updateBookmarkStatusByContentId(contentId, isBookmark);
    }

    public Completable updateReadStatusByContentId(long contentId, boolean isRead) {
        return dataSource.updateReadStatusByContentId(contentId, isRead);
    }

    public void takeAndShareScreenshot(Bitmap bitmap, String contentTitle) {
        try {
            File shareImagePath = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),"sandesh360_share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream outputStream = new FileOutputStream(shareImagePath);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.close();
            shareScreenshot(contentTitle,shareImagePath);
        } catch (Throwable e) {
            Toast.makeText(context, "Catch from Creating and saving file", Toast.LENGTH_LONG).show();
            // Several error may come out with file handling or DOM
            Log.e(TAG, "Couldn't take screenshot. Something went wrong");
            Toast.makeText(context, context.getString(R.string.share_error_message), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void shareScreenshot(String contentTitle, File shareImagePath) {
        try {
            Uri contentURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", shareImagePath);
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/*");
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentURI);
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
            shareIntent.putExtra(Intent.EXTRA_TEXT, contentTitle);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Intent chooser = Intent.createChooser(shareIntent, context.getString(R.string.share_chooser_title));
            if (shareIntent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(chooser);
            }
        } catch (Exception e) {
            Log.e(TAG, "Couldn't take screenshot. Something went wrong");
            Toast.makeText(context, context.getString(R.string.share_error_message), Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    public void invokePollApi(Integer qid, boolean isYes) {
        getNavigator().setVisibityOfPollProgress(true);
        DataManager.getInstance().invokePollApi(ApiUrls.POLL_API_URL, getRequestBody(qid, isYes), this);

    }

    public PollRequestBody getRequestBody(Integer qid, boolean isYes) {
        return new PollRequestBody().setQuestionId(qid).setYes(isYes).setVoter("mobarak");
    }

    @Override
    public void onSuccessResponse(PollResponse response) {
        if (response.getPercentage() != null) {
            getNavigator().insertPoll(response.getPercentage());
        }

        getNavigator().setVisibityOfPollProgress(false);

    }

    @Override
    public void onErrorResponse(ApiError error) {
        getNavigator().setVisibityOfPollProgress(false);

    }
}
