package com.sandesh360.sandesh.ui.search;

import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;

import java.util.List;

public interface SearchNavigator extends IBaseNavigator {
    void updateUI(List<Content> contents);

    void onError(String error);

    void insertContent(List<Content> contents);
}
