package com.sandesh360.sandesh.ui.navdrawer;

import android.database.Cursor;
import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class NavDrawerViewModel extends BaseViewModel<NavDrawerNavigator> {
    public NavDrawerViewModel(FragmentActivity context, NavDrawerNavigator navigator, IDbHelper dbHelper) {
        super(context, navigator, dbHelper);
    }

    public String[] getSuggestionList() {
        return dataSource.getContentTitle();
    }

    public Cursor getCursor(String query) {
        return dataSource.getCursor(query);
    }
}
