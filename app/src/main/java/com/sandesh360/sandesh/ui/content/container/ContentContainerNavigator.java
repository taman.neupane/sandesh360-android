package com.sandesh360.sandesh.ui.content.container;


import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;

import java.util.List;

/**
 * Created by Mobarak on October 19, 2018
 *
 * @author Sandesh360
 */
public interface ContentContainerNavigator extends IBaseNavigator {

    void loadPager(List<Content> contents);

    Long getCategoryId();

    PreferenceType getPreferenceType();

    void searchPreference();

    void insertContent(List<Content> contents);

    boolean isLoadFromDb();

}
