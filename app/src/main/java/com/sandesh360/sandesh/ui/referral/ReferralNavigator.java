package com.sandesh360.sandesh.ui.referral;

import com.sandesh360.sandesh.ui.base.IBaseNavigator;

public interface ReferralNavigator  extends IBaseNavigator {

    void showSuccessMessage(String message);

    void showFailedMessage(String message);
}
