package com.sandesh360.sandesh.ui.webcontent;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.appbar.AppBarLayout;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.IWindowBarController;
import com.sandesh360.sandesh.ui.webcontent.htmlpages.WebFragment;
import com.sandesh360.sandesh.utils.AnimationUtil;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.LocaleUtil;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class WebActivity extends AppCompatActivity implements WebNavigator, IWindowBarController {

    private static final String TAG = WebActivity.class.getSimpleName();
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tv_site_url)
    TextView tvTitle;
    private Unbinder unbinder;
    private String siteUrl, contentUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            LocaleUtil.onAttach(this);
        } catch (Exception e) {

        }

        setContentView(R.layout.activity_web);
        if (getIntent() != null) {
            siteUrl = getIntent().getStringExtra(Constants.BUNDLE_KEY_SITE_URL);
            contentUrl = getIntent().getStringExtra(Constants.BUNDLE_KEY_CONTENT_URL);
        }
        unbinder = ButterKnife.bind(this);
        initializedToolbar();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, WebFragment.newInstance(contentUrl), Constants.CURRENT_FRAGMENT)
                    .commit();
        }
//        Slidr.attach(this);
    }

//    @Override
//    public WebViewModel getViewModel() {
//        return null;
//    }

    @Override
    public void setToolbarVisibility(boolean isVisible) {
        if (appBar != null) {
            if (isVisible) {
                AnimationUtil.appBarShow(appBar);
            } else {
                AnimationUtil.appBarHide(appBar);
            }
        }
    }

    @Override
    public boolean isToolbarVisible() {
        return appBar != null ? (appBar.getVisibility() == View.VISIBLE) : false;
    }

    @Override
    public void setTitleText(@NonNull String title) {
        if (tvTitle != null) tvTitle.setText(title);
    }

    @Override
    public void initializedToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_previous);
        }
    }

    @Override
    public void setBackArrowVisibility(boolean isVisible) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitleText(UtilityFunctions.getDomainName(siteUrl));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.web_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_copy_link:
                UtilityFunctions.copyToClipboard(this, contentUrl);
                break;
            case R.id.item_open_browser:
                UtilityFunctions.openLinkInBrowser(this, contentUrl);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
//        overridePendingTransition(R.anim.pop_enter, R.anim.pop_exit);
    }
}
