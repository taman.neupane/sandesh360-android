package com.sandesh360.sandesh.ui.category;


import com.sandesh360.sandesh.data.db.entity.Category;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;

import java.util.List;

/**
 * Created by Mobarak on October 19, 2018
 *
 * @author Sandesh360
 */
public interface CategoryNavigator extends IBaseNavigator {

    void loadCategories(List<Category> categories);

    void insertCategories(List<Category> categories);

}
