package com.sandesh360.sandesh.ui.privacy_terms;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyAndTermsFragment extends BaseFragment<PrivacyTermsViewModel> implements PrivacyTermsNavigator {

    private static final String TAG = PrivacyAndTermsFragment.class.getSimpleName();
    private static final String BUNDLE_KEY_PAGE_TITLE = "bundle_key_page_title";
    @BindView(R.id.web_view)
    WebView webView;
    private boolean isFromPrivacy = false;
    private PrivacyTermsViewModel mViewModel;


    public PrivacyAndTermsFragment() {
        // Required empty public constructor
    }

    public static PrivacyAndTermsFragment newInstance(boolean isFromPrivacy) {
        PrivacyAndTermsFragment fragment = new PrivacyAndTermsFragment();
        Bundle args = new Bundle();
        args.putBoolean(BUNDLE_KEY_PAGE_TITLE, isFromPrivacy);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isFromPrivacy = getArguments().getBoolean(BUNDLE_KEY_PAGE_TITLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        setTitleText(isFromPrivacy ? getString(R.string.nav_text_privacy_policy) : getString(R.string.nav_text_terms_conditions));
        //setBackArrowVisibility(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.web_page_fragment, container, false);
        initializedView(view);
        //webView.loadUrl("file:///android_asset/" + getPath() + ".html");
        return view;
    }

    @Override
    public PrivacyTermsViewModel getViewModel() {
//        mViewModel = new PrivacyTermsViewModel(getActivity(), this);
//        mViewModel = (PrivacyTermsViewModel) new ViewModelFactory(mViewModel).create(PrivacyTermsViewModel.class);
        return mViewModel;
    }

}
