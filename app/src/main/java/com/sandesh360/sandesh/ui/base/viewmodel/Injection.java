package com.sandesh360.sandesh.ui.base.viewmodel;


import android.content.Context;
import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.db.AppDatabase;
import com.sandesh360.sandesh.data.db.DbManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;

/**
 * Enables injection of data sources.
 */
public class Injection {


    public static IDbHelper provideDataSource(Context context) {
        AppDatabase database = AppDatabase.getInstance(context);
        return new DbManager(
                database.languageDao(),
                database.preferenceDao(),
                database.categoryDao(),
                database.contentDao(),
                database.adsDao()
        );
    }

    public static ViewModelFactory provideViewModelFactory(FragmentActivity context, IBaseNavigator navigator) {
        IDbHelper dataSource = provideDataSource(context);
        return new ViewModelFactory(context, navigator, dataSource);
    }
}
