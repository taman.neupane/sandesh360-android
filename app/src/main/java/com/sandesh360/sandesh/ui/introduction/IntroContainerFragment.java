package com.sandesh360.sandesh.ui.introduction;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.content.container.VerticalViewPager;
import com.sandesh360.sandesh.ui.content.container.adapter.DepthPageTransformer;
import com.sandesh360.sandesh.ui.navdrawer.MainActivity;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroContainerFragment extends BaseFragment<IntroViewModel> implements IntroNavigator {
    private static final String TAG = IntroContainerFragment.class.getSimpleName();
    @BindView(R.id.view_pager)
    VerticalViewPager viewPager;
    private IntroViewModel mViewModel;

    private IntroPagerAdapter pagerAdapter;

    public IntroContainerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_intro_container, container, false);
        initializedView(view);
        setUpPager();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getViewModel();
    }

    @Override
    public IntroViewModel getViewModel() {
//        mViewModel = new IntroViewModel(getActivity(), this);
//        mViewModel = (IntroViewModel) new ViewModelFactory(mViewModel).create(IntroViewModel.class);
        return mViewModel;
    }

    private void setUpPager() {
        pagerAdapter = new IntroPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
//        viewPager.setPageTransformer(true, new DepthPageTransformer());
//        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//                if (ViewPager.SCROLL_STATE_DRAGGING == state) {
//                    handleLastPageScrolling();
//                }
//            }
//        });

    }

    private void handleLastPageScrolling() {
        int size = pagerAdapter.getCount();
        int currentPage = viewPager.getCurrentItem();
        if (currentPage == size - 1 && UtilityFunctions.isNetworkAvailable(getActivity())) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            getActivity().startActivity(intent);
            getActivity().finish();
        }
    }
}
