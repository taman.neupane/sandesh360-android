package com.sandesh360.sandesh.ui.introduction.splash;


import com.sandesh360.sandesh.data.db.entity.Language;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;

import java.util.List;

/**
 * Created by Mobarak on October 19, 2018
 *
 * @author Sandesh360
 */
public interface SplashNavigator extends IBaseNavigator {

    void gotoMainScreen();

    void gotoIntroScreen();

    void screenNavigation();

    void insertLanguages(List<Language> languages);

    void insertPreferences(List<Preference> preferences);

}
