package com.sandesh360.sandesh.ui.signin;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class SignInViewModel extends BaseViewModel<SignInNavigator> {

    public SignInViewModel(FragmentActivity context, SignInNavigator navigator) {
        super(context, navigator);
    }
}
