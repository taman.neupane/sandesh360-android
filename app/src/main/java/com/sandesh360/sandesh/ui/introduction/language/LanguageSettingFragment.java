package com.sandesh360.sandesh.ui.introduction.language;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;

public class LanguageSettingFragment extends BaseFragment<LanguageSettingViewModel> implements LanguageSettingNavigator {

    private LanguageSettingViewModel mViewModel;

    public static LanguageSettingFragment newInstance() {
        return new LanguageSettingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.language_setting_fragment, container, false);
        initializedView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        getViewModel();

    }

    @Override
    public LanguageSettingViewModel getViewModel() {
//        mViewModel = new LanguageSettingViewModel(getActivity(), this);
//        mViewModel = (LanguageSettingViewModel) new ViewModelFactory(mViewModel).create(LanguageSettingViewModel.class);
        return mViewModel;
    }

}
