package com.sandesh360.sandesh.ui.content.container;

/**
 * Created by Mobarak on November 08, 2018
 *
 * @author Sandesh360
 */
public enum ContentType {
    PREFERENCE,
    CATEGORY,
    BOOKMARKS,
    UNREAD,
    POLL
}
