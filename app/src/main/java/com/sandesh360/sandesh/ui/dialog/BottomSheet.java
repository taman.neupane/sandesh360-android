package com.sandesh360.sandesh.ui.dialog;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.ui.navdrawer.MainActivity;
import com.sandesh360.sandesh.ui.signin.SignInController;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.LocaleUtil;

/**
 * Created by Mobarak on November 13, 2018
 *
 * @author Sandesh360
 */
public class BottomSheet extends BottomSheetDialogFragment {

    private static final String BUNDLE_KEY_SHEET_TYPE = "bundle_key_sheet_type";
    private static final String TAG = BottomSheet.class.getSimpleName();
    private SheetType type;
    private String language = Constants.LOCALE_EN;


    public BottomSheet() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = (SheetType) getArguments().getSerializable(BUNDLE_KEY_SHEET_TYPE);
        }

    }

    public static BottomSheet newInstance(SheetType type) {
        BottomSheet bottomSheet = new BottomSheet();
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_KEY_SHEET_TYPE, type);
        bottomSheet.setArguments(args);
        return bottomSheet;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        switch (type) {
            case LANGUAGE_CHANGE:
                view = inflater.inflate(R.layout.language_change_bottom_sheet, container, false);
                changeLanguage(view);
                break;
            case AUTO_PLAY:
                view = inflater.inflate(R.layout.auto_play_bottom_sheet, container, false);
                break;
            case LOGOUT:
                view = inflater.inflate(R.layout.logout_bottom_sheet, container, false);
                logout(view);
                break;

        }
        return view;
    }

    private void logout(View view) {
        TextView tvLogout = view.findViewById(R.id.tv_logout);
        TextView tvCancel = view.findViewById(R.id.tv_cancel);
        tvLogout.setOnClickListener(v -> {
            if (DataManager.getInstance().isFbLogin()) {
                SignInController.getInstance(getActivity()).fbLogout((MainActivity) (getActivity()));
            } else {
                SignInController.getInstance(getActivity()).googleSignOut((MainActivity) (getActivity()));
            }
            dismiss();
        });
        tvCancel.setOnClickListener(v -> dismiss());
    }

    private void changeLanguage(View view) {
        RadioGroup rg = view.findViewById(R.id.rg_lang_setting);
        RadioButton rbEn = view.findViewById(R.id.rb_lang_en);
        RadioButton rbBn = view.findViewById(R.id.rb_lang_bn);

        if (LocaleUtil.isEnglish()) {
            rbEn.setChecked(true);
            rbBn.setChecked(false);
        } else {
            rbEn.setChecked(false);
            rbBn.setChecked(true);
        }
        rg.setOnCheckedChangeListener((radioGroup, i) -> {
            Log.i(TAG, "position" + i);
            if (R.id.rb_lang_en == i) {
                language = Constants.LOCALE_EN;
            } else {
                language = Constants.LOCALE_BN;
            }
            LocaleUtil.ChangeLanguage(getActivity(), language);
            dismiss();
            Intent intent = this.requireActivity().getIntent();
            this.requireActivity().finish();
            startActivity(intent);
        });
    }


}
