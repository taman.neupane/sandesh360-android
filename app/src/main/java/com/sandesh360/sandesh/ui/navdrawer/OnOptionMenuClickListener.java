package com.sandesh360.sandesh.ui.navdrawer;

/**
 * Created by Mobarak on December 08, 2018
 *
 * @author Sandesh360
 */
public interface OnOptionMenuClickListener {

    void onClickMenu(OptionMenuType menuType);
}
