package com.sandesh360.sandesh.ui.login;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;

public class LoginFragment extends BaseFragment<LoginViewModel> implements LoginNavigator {

    private LoginViewModel mViewModel;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public LoginViewModel getViewModel() {
//        mViewModel = new LoginViewModel(getActivity(), this);
//        mViewModel = (LoginViewModel) new ViewModelFactory(mViewModel).create(LoginViewModel.class);
        return mViewModel;
    }
}
