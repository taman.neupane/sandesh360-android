package com.sandesh360.sandesh.ui.base;

public interface OnItemClickListener<T> {
    void onItemClicked(T item);
}