package com.sandesh360.sandesh.ui.base;

import androidx.annotation.NonNull;

/**
 * Created by Mobarak on 13-Oct-17.
 *
 * @author Sandesh360
 */

public interface IWindowBarController {

    void setToolbarVisibility(boolean isVisible);

    boolean isToolbarVisible();

    void setTitleText(@NonNull String title);

    void initializedToolbar();

    void setBackArrowVisibility(boolean isVisible);
}
