package com.sandesh360.sandesh.ui.setting;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.dialog.BottomSheet;
import com.sandesh360.sandesh.ui.dialog.SheetType;
import com.sandesh360.sandesh.ui.signin.SignInFragment;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import butterknife.BindView;

public class SettingsFragment extends BaseFragment<SettingsViewModel> implements SettingsNavigator {

    private static final String TAG = SettingsFragment.class.getSimpleName();
    private SettingsViewModel mViewModel;

    @BindView(R.id.layout_sign_in)
    CardView layoutSignIn;
    @BindView(R.id.tv_language)
    TextView tvLanguageSetting;
    @BindView(R.id.tv_auto_play)
    TextView tvAutoPlaySetting;
    @BindView(R.id.sw_notification)
    Switch swNotificationSetting;
    @BindView(R.id.sw_hd_image)
    Switch swHdImageSetting;
    @BindView(R.id.sw_night_mode)
    Switch swNightModeSetting;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        initializedView(view);
        setClickListener();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public SettingsViewModel getViewModel() {
//        mViewModel = new SettingsViewModel(getActivity(), this);
//        mViewModel = (SettingsViewModel) new ViewModelFactory(mViewModel).create(SettingsViewModel.class);
        return mViewModel;
    }

    @Override
    protected void setClickListener() {
        layoutSignIn.setOnClickListener(v -> {
            UtilityFunctions.changeFragment(getActivity(), SignInFragment.newInstance(), true, true);
        });
        tvLanguageSetting.setOnClickListener(v -> {
            BottomSheet.newInstance(SheetType.LANGUAGE_CHANGE).show(getFragmentManager(), "test");
        });
        tvAutoPlaySetting.setOnClickListener(v -> {
            BottomSheet.newInstance(SheetType.AUTO_PLAY).show(getFragmentManager(), "test");
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleText(getString(R.string.nav_text_options));
        setToolbarVisibility(true);
        if (DataManager.getInstance().isLoggedin()) {
            layoutSignIn.setVisibility(View.GONE);
        } else {
            layoutSignIn.setVisibility(View.VISIBLE);
        }
    }
}
