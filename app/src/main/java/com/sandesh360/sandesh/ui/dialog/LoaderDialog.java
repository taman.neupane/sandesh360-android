package com.sandesh360.sandesh.ui.dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.custom_view.CircularAnimation;

/**
 * Created by Mobarak on November 17, 2018
 *
 * @author Sandesh360
 */
public class LoaderDialog extends DialogFragment {

    public LoaderDialog() {

    }

    public static LoaderDialog newInstance() {
        return new LoaderDialog();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loader_dialog, container, false);
        ImageView imageView = view.findViewById(R.id.iv_loading);
        Animation anim = new CircularAnimation(imageView, 100);
        anim.setDuration(3000);
        imageView.startAnimation(anim);
        return view;
    }
}
