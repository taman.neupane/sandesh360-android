package com.sandesh360.sandesh.ui.content.container.adapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Scroller;

import java.lang.reflect.Field;

import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

public class CustomVerticalViewPager extends ViewPager {

    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    private GestureDetector gestureDetector;
    public boolean isScrollEvent;
    public CustomVerticalViewPager(Context context) {
        super(context);
        init();
    }

    public CustomVerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    @Override
    public boolean canScrollHorizontally(int direction) {
        return false;
    }

//    private void init() {
//        gestureDetector=new GestureDetector(getContext(),new GestureListener());
////        setPageTransformer(true, new VerticalPageTransformer());
//        setOverScrollMode(OVER_SCROLL_NEVER);
//        try {
//            Field mScroller;
//            mScroller = ViewPager.class.getDeclaredField("mScroller");
//            mScroller.setAccessible(true);
//            FixedSpeedScroller scroller = new FixedSpeedScroller(getContext(), new DecelerateInterpolator());
//            scroller.setScrollDuration(250);
//            mScroller.set(this, scroller);
//        } catch (Exception e) {}
//    }
//
//
//    @Override
//    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        return gestureDetector.onTouchEvent((ev));
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//        boolean res= gestureDetector.onTouchEvent((ev));
//        if((ev.getAction() == MotionEvent.ACTION_CANCEL ||ev.getAction()==MotionEvent.ACTION_UP)) {
//            if(isScrollEvent) {
//                try {
//                    endFakeDrag();
//                } catch (Exception e) {}
//            }
//            return true;
//        }
//        // Log.d("event", "ontouch      " + res);
//        return res;
//    }
//
    private class FixedSpeedScroller extends Scroller {

        private int mDuration = 500;

        public FixedSpeedScroller(Context context) {
            super(context);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator) {
            super(context, interpolator);
        }

        public FixedSpeedScroller(Context context, Interpolator interpolator, boolean flywheel) {
            super(context, interpolator, flywheel);
        }

        public void setScrollDuration(int duration) {
            mDuration = duration;
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }
//
//    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
//        @Override
//        public boolean onSingleTapUp(MotionEvent e) {
//            isScrollEvent=false;
//            // Log.d("touch","singletap");
//            return false;
//        }
//        @Override
//        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
//            //   Log.d("touch","fling");
//            isScrollEvent=false;
//            try {
//                float diffY = e2.getY() - e1.getY();
//                float diffX = e2.getX() - e1.getX();
//                if (Math.abs(diffX) > Math.abs(diffY)) {
//                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
//                        if (diffX > 0) {
//                            onSwipeRight();
//                        } else {
//                            onSwipeLeft();
//                        }
//                        isScrollEvent = true;
//                    }
//                }else if (diffY > 20) {
//                    onSwipeDown();
//                    return  false;
//                } else if(diffY<-20){
//                    onSwipeUp();
//                    return  false;
//                }
//                else
//                {
//                    endFakeDrag();
//                }
//
//
//            } catch (Exception exception) {
//                exception.printStackTrace();
//            }
//            return false;
//
//        }
//        @Override
//        public boolean onScroll(MotionEvent e1, MotionEvent e2,
//                                float distanceX, float distanceY) {
//            // Log.d("touch", "scroll" + " " + distanceX);
//            beginFakeDrag();
//            fakeDragBy(-distanceY);
//            isScrollEvent=true;
//            return true;
//
//        }
//
//    }
//    public void onSwipeUp() {
//        setCurrentItem(getCurrentItem() + 1);
//    }
//    public void onSwipeDown() {
//        setCurrentItem(getCurrentItem() - 1);
//
//    }
//
//    public void onSwipeRight() {
//    }
//
//    public void onSwipeLeft() {
//    }

    private void init() {
        // The majority of the magic happens here
        setPageTransformer(false, new VerticalPageTransformer());
        // The easiest way to get rid of the overscroll drawing that happens on the left and right
        setOverScrollMode(OVER_SCROLL_NEVER);

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(getContext(), new DecelerateInterpolator());
            scroller.setScrollDuration(250);
            mScroller.set(this, scroller);
        } catch (Exception e) {}

        try {
            Class cls = this.getClass().getSuperclass();
            Field distanceField = cls.getDeclaredField("mFlingDistance");
            distanceField.setAccessible(true);
            distanceField.setInt(this, distanceField.getInt(this) / 40);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            Class cls = this.getClass().getSuperclass();
            Field minVelocityField = cls.getDeclaredField("mMinimumVelocity");
            minVelocityField.setAccessible(true);
            minVelocityField.setInt(this, minVelocityField.getInt(this) / 25);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            Class cls = this.getClass().getSuperclass();
            Field maxVelocityField = cls.getDeclaredField("mMaximumVelocity");
            maxVelocityField.setAccessible(true);
            maxVelocityField.setInt(this, maxVelocityField.getInt(this) * 10);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            Class cls = this.getClass().getSuperclass();
            Field slopField = cls.getDeclaredField("mTouchSlop");
            slopField.setAccessible(true);
            slopField.setInt(this, slopField.getInt(this) / 10);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            Class cls = this.getClass().getSuperclass();
            Field minHeightWidthRatioField = cls.getDeclaredField("minYXRatioForIntercept");
            minHeightWidthRatioField.setAccessible(true);
            minHeightWidthRatioField.setFloat(this, minHeightWidthRatioField.getFloat(this) * 8);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        try {
            Class cls = this.getClass().getSuperclass();
            Field minHeightWidthRatioField = cls.getDeclaredField("minYXRatioForTouch");
            minHeightWidthRatioField.setAccessible(true);
            minHeightWidthRatioField.setInt(this, minHeightWidthRatioField.getInt(this) * 4);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Swaps the X and Y coordinates of your touch event.
     */
    private MotionEvent swapXY(MotionEvent ev) {
        float width = getWidth();
        float height = getHeight();

        float y = ev.getY();
        float x = ev.getX();

        float newX = (y / height) * width;
        float newY = (x / width) * height;

        ev.setLocation(newX, newY);

        return ev;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean intercepted = super.onInterceptTouchEvent(swapXY(ev));
        swapXY(ev); // return touch coordinates to original reference frame for any child views
        return intercepted;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(swapXY(ev));
    }

    private class VerticalPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        @Override
        public void transformPage(View view, float position) {

            if (position <= -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left/top page
                view.setAlpha(1);
                ViewCompat.setElevation(view, 1);
                // Counteract the default slide transition
                view.setTranslationX(view.getWidth() * -position);
                view.setTranslationY(0);

                //set Y position to swipe in from top
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else if (position <= 1) { // [0,1]
                view.setAlpha(1);
                ViewCompat.setElevation(view, 2);

                // Counteract the default slide transition
                view.setTranslationX(view.getWidth() * -position);
                view.setTranslationY(position * view.getHeight());

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(1);
                view.setScaleY(1);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }

        }
    }

}
