package com.sandesh360.sandesh.ui.base;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.sandesh360.sandesh.BuildConfig;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.ui.update.ForceUpdateChecker;

import io.fabric.sdk.android.Fabric;

import java.util.HashMap;
import java.util.Map;


public class Sandesh360App extends Application {

    private static final String TAG = Sandesh360App.class.getSimpleName();

    private static Sandesh360App mInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mInstance = this;

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }

        final FirebaseRemoteConfig firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // set in-app defaults
        Map<String, Object> remoteConfigDefaults = new HashMap();
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_REQUIRED, false);
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_CURRENT_VERSION, "1.1.3");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_URL,
                "https://play.google.com/store/apps/details?id=com.sandesh360.sandesh");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_TITLE, getString(R.string.app_name) + " Update Available");
        remoteConfigDefaults.put(ForceUpdateChecker.KEY_UPDATE_DESCRIPTION, "Please update application to access new features available in updated application");

        firebaseRemoteConfig.setDefaults(remoteConfigDefaults);
        firebaseRemoteConfig.fetch(60) // fetch every minutes
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "remote config is fetched.");
                        DataManager.getInstance().setFirstLaunch(false);
                        firebaseRemoteConfig.fetchAndActivate();
                    }
                });


    }

    public static Sandesh360App getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

}