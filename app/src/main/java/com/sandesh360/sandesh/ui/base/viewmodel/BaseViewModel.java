/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.sandesh360.sandesh.ui.base.viewmodel;

import androidx.lifecycle.ViewModel;
import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.db.IDbHelper;

import java.lang.ref.WeakReference;

/**
 * Created by amitshekhar on 07/07/17.
 */

public class BaseViewModel<IBaseNavigator> extends ViewModel {


    protected FragmentActivity context;
    private WeakReference<IBaseNavigator> mNavigator;
    protected IDbHelper dataSource;

    public BaseViewModel(FragmentActivity context) {
        this.context = context;
    }

    public BaseViewModel(FragmentActivity context, IBaseNavigator navigator) {
        this(context);
        this.mNavigator = new WeakReference<>(navigator);
    }

    public BaseViewModel(FragmentActivity context, IBaseNavigator navigator, IDbHelper dataSource) {
        this(context, navigator);
        this.dataSource = dataSource;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }


    public IBaseNavigator getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(IBaseNavigator navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }


}
