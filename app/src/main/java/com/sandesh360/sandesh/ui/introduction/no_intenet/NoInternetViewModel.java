package com.sandesh360.sandesh.ui.introduction.no_intenet;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class NoInternetViewModel extends BaseViewModel<NoInternetNavigator> {
    public NoInternetViewModel(FragmentActivity context, NoInternetNavigator navigator) {
        super(context, navigator);
    }
}
