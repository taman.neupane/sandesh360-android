package com.sandesh360.sandesh.ui.referral

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.sandesh360.sandesh.R
import com.sandesh360.sandesh.data.DataManager
import com.sandesh360.sandesh.ui.base.BaseFragment
import com.sandesh360.sandesh.ui.base.IBaseNavigator
import com.sandesh360.sandesh.ui.base.viewmodel.Injection
import kotlinx.android.synthetic.main.referral_fragment.*

class ReferralFragment : BaseFragment<ReferralViewModel>(), IBaseNavigator, ReferralNavigator {


    override fun showSuccessMessage(message: String?) {
        Toast.makeText(this.requireContext(), message, Toast.LENGTH_SHORT).show()
        referralCodeNotSentLayout.visibility = View.GONE
        referralCodeSentLayout.visibility = View.VISIBLE
    }

    override fun showFailedMessage(message: String?) {
        Toast.makeText(this.requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    private var mViewModel: ReferralViewModel? = null

    override fun getViewModel(): ReferralViewModel {
        mViewModelFactory = Injection.provideViewModelFactory(activity, this)
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ReferralViewModel::class.java)
        return mViewModel!!
    }

    companion object {
        val TAG = ReferralFragment::class.java.simpleName
        fun newInstance() = ReferralFragment()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.referral_fragment, container, false)
        initializedView(view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (DataManager.getInstance().referralCode.isBlank()) {
            referralCodeSentLayout.visibility = View.GONE
            referralCodeNotSentLayout.visibility = View.VISIBLE
        } else {
            referralCodeNotSentLayout.visibility = View.GONE
            referralCodeSentLayout.visibility = View.VISIBLE
        }

        referralCodeET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(charSequence: CharSequence?, p1: Int, p2: Int, p3: Int) {
                submit_referral_btn.isEnabled = !charSequence.toString().isEmpty()
            }

        })

        submit_referral_btn.setOnClickListener {
            mViewModel?.submitReferral(referralCodeET.text.toString())
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        menu.clear()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel
    }

    override fun onResume() {
        super.onResume()
        setTitleText(getString(R.string.nav_text_referral))
        setToolbarVisibility(true)
    }
}
