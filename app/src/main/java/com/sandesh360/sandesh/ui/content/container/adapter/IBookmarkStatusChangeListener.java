package com.sandesh360.sandesh.ui.content.container.adapter;

/**
 * Created by Mobarak on November 10, 2018
 *
 * @author Sandesh360
 */
public interface IBookmarkStatusChangeListener {

    void onBookmarkStatusChanged(boolean isBookmark);
}
