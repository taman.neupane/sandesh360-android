package com.sandesh360.sandesh.ui.ads;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.NativeAds;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.webcontent.WebActivity;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsFragment extends BaseFragment<AdsViewModel> implements AdsNavigator {

    private static final String TAG = AdsFragment.class.getSimpleName();
    private static final String BUNDLE_KEY_ADS = "bundle_key_ads";
    private AdsViewModel mViewModel;
    private NativeAds ads;

    @BindView(R.id.iv_ads)
    ImageView ivAdsBanner;


    public AdsFragment() {
        // Required empty public constructor
    }

    public static AdsFragment newInstance(NativeAds ads) {
        AdsFragment fragment = new AdsFragment();
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_KEY_ADS, ads);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            ads = (NativeAds) getArguments().getSerializable(BUNDLE_KEY_ADS);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_ads, container, false);
        initializedView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarVisibility(false);
        if (ads != null) {
            Glide.with(getActivity()).load(ApiUrls.CATEGORY_ICON_URL + ads.getIcon())
                    .apply(new RequestOptions().placeholder(R.drawable.category_placeholder))
                    .into(ivAdsBanner);

            if(ads.getLink() != null){
                if(!ads.getLink().isEmpty()) {
                    ivAdsBanner.setOnClickListener(v -> {
                        Bundle bundle = new Bundle();
                        if (ads != null) {
                            bundle.putString(Constants.BUNDLE_KEY_SITE_URL, ads.getLink().trim());
                            bundle.putString(Constants.BUNDLE_KEY_CONTENT_URL, ads.getLink().trim());
                        }
                        UtilityFunctions.startActivity(getActivity(), WebActivity.class, bundle);
                    });
                }
            }
        }
    }

    @Override
    public AdsViewModel getViewModel() {
//        mViewModel = new AdsViewModel(getActivity(), this);
//        mViewModel = (AdsViewModel) new ViewModelFactory(mViewModel).create(AdsViewModel.class);
        return mViewModel;
    }


}
