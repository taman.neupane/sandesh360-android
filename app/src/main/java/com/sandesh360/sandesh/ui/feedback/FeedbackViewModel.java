package com.sandesh360.sandesh.ui.feedback;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.data.network.ApiError;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.BaseResponse;
import com.sandesh360.sandesh.data.network.model.FeedbackRequest;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.UtilityFunctions;

public class FeedbackViewModel extends BaseViewModel<FeedbackNavigator> implements IResponseCallback<BaseResponse> {

    public FeedbackViewModel(FragmentActivity context, FeedbackNavigator navigator, IDbHelper dbHelper) {
        super(context, navigator, dbHelper);
    }

    public void validateInput(String email, String comment) {

        if (UtilityFunctions.isEmpty(email)) {
            getNavigator().onEmailError(context.getString(R.string.email_required));
            return;
        } else if (!UtilityFunctions.isEmailValid(email)) {
            getNavigator().onEmailError(context.getString(R.string.valid_mail));
            return;
        }

        if (UtilityFunctions.isEmpty(comment)) {
            getNavigator().onCommentError(context.getString(R.string.comment_required));
            return;
        }

        // Invoke api
        invokeAPI(getBody(email, comment));

    }

    private FeedbackRequest getBody(String email, String comment) {
        return new FeedbackRequest().setFrom(email).setComment(comment);
    }

    private void invokeAPI(FeedbackRequest body) {
        UtilityFunctions.showProgressBar(context);
        DataManager.getInstance().invokeFeedbackApi(ApiUrls.FEEDBACK_API_URL, body, this);
    }

    @Override
    public void onSuccessResponse(BaseResponse response) {
        if (response != null && response.getStatus().equalsIgnoreCase(Constants.STATUS_OK)) {
            getNavigator().showSuccessMessage(context.getString(R.string.feedback_success_msg));
        }

        UtilityFunctions.stopProgressBar(context);

    }

    @Override
    public void onErrorResponse(ApiError error) {
        if (error != null) {
            getNavigator().showFailedMessage(error.getMessage());
        }

        UtilityFunctions.stopProgressBar(context);
    }
}
