package com.sandesh360.sandesh.ui.introduction;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class IntroViewModel extends BaseViewModel<IntroNavigator> {
    public IntroViewModel(FragmentActivity context, IntroNavigator navigator) {
        super(context, navigator);
    }
}
