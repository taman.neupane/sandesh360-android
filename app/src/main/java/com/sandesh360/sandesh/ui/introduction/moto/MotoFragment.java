package com.sandesh360.sandesh.ui.introduction.moto;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;

public class MotoFragment extends BaseFragment<MotoViewModel> implements MotoNavigator {

    private MotoViewModel mViewModel;

    public static MotoFragment newInstance() {
        return new MotoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.moto_fragment, container, false);
        initializedView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public MotoViewModel getViewModel() {
//        mViewModel = new MotoViewModel(getActivity(), this);
//        mViewModel = (MotoViewModel) new ViewModelFactory(mViewModel).create(MotoViewModel.class);
        return mViewModel;
    }

}
