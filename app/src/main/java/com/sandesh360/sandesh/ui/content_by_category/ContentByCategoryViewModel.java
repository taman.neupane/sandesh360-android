package com.sandesh360.sandesh.ui.content_by_category;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class ContentByCategoryViewModel extends BaseViewModel<ContentByCategoryNavigator> {
    public ContentByCategoryViewModel(FragmentActivity context, ContentByCategoryNavigator navigator) {
        super(context, navigator);
    }

}
