/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.sandesh360.sandesh.ui.base;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.sandesh360.sandesh.data.model.User;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;
import com.sandesh360.sandesh.ui.base.viewmodel.ViewModelFactory;
import com.sandesh360.sandesh.ui.navdrawer.OnMenuVisibilityListener;
import com.sandesh360.sandesh.ui.navdrawer.OptionMenuType;
import com.sandesh360.sandesh.utils.LocaleUtil;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by Mobarak on 19/10/2018.
 */

public abstract class BaseFragment<V extends BaseViewModel> extends Fragment {

    private V mViewModel;
    protected ViewModelFactory mViewModelFactory;



    /**
     * Override for set view model
     *
     * @return view model instance
     */
    public abstract V getViewModel();

    private Unbinder unbinder;

    protected void setButterKnife(View view) {
        if (view != null) {
            unbinder = ButterKnife.bind(this, view);
        }
    }

    protected void unbindButterKnife() {
        if (unbinder != null)
            unbinder.unbind();
    }

    /**
     * Invoke to back in previous fragment
     *
     * @return true|false
     */
    public boolean popFragment() {
        boolean isPop = false;
        if (getActivity() != null) {
            UtilityFunctions.hideVirtualKeyboard(getActivity());
            Log.e("getBackStackEntryCount ", "getBackStackEntryCount: "
                    + getActivity().getSupportFragmentManager().getBackStackEntryCount());
            if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                isPop = true;
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
        return isPop;
    }


    /**
     * Invoke to back parent fragment
     */
    public void popBackToFragment() {
        if (getActivity() != null) {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            int count = fm.getBackStackEntryCount();
            Log.e("getBackStackEntryCount ", "getBackStackEntryCount: " + count);
            for (int i = 0; i < count; ++i) {
                fm.popBackStack();
            }
        }
    }


    /**
     * @param view
     * @param activity
     */
    protected void hideSoftKeyboardByTouchingOutSideEditField(View view, final FragmentActivity activity) {

        if (view == null || activity == null) {
            return;
        }
        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    UtilityFunctions.hideVirtualKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideSoftKeyboardByTouchingOutSideEditField(innerView, activity);
            }
        }
    }


    /**
     * Invoke to controlling visibility of ActionBar (Toolbar)
     * If @isShow is true ActionBar is visible
     * Else ActionBar is Gone
     *
     * @param isVisible true| false
     */
    protected void setToolbarVisibility(boolean isVisible) {
        if (getActivity() instanceof IWindowBarController) {
            ((IWindowBarController) getActivity()).setToolbarVisibility(isVisible);
        }

    }

    /**
     * Invoke to controlling visibility of BackArrow (Toolbar)
     * If @isShow is true BackArrow is visible
     * Else BackArrow is Gone
     *
     * @param title String
     */
    protected void setTitleText(String title) {
        if (getActivity() instanceof IWindowBarController) {
            ((IWindowBarController) getActivity()).setTitleText(title);
        }
    }

    protected boolean isToolbarVisible() {
        if (getActivity() instanceof IWindowBarController) {
            return ((IWindowBarController) getActivity()).isToolbarVisible();
        }
        return false;
    }

    protected void initializedToolbar(boolean isSetDefault) {
        if (getActivity() instanceof IWindowBarController) {
            ((IWindowBarController) getActivity()).initializedToolbar();
        }
    }

    /**
     * Invoke to controlling visibility of BackArrow (Toolbar)
     * If @isShow is true BackArrow is visible
     * Else BackArrow is Gone
     *
     * @param isVisible
     */
    protected void setBackArrowVisibility(boolean isVisible) {
        if (getActivity() instanceof IWindowBarController) {
            ((IWindowBarController) getActivity()).setBackArrowVisibility(isVisible);
        }

    }


    protected void initializedView(View view) {
        setButterKnife(view);
        setLocale();
    }

    protected void setClickListener() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbindButterKnife();
    }

    protected void onLoginSuccessCallback(User user) {
        if (getActivity() instanceof ILoginCallback) {
            ((ILoginCallback) getActivity()).onLoginSuccess(user);
        }
    }

    protected void onLogoutSuccessCallback() {
        if (getActivity() instanceof ILoginCallback) {
            ((ILoginCallback) getActivity()).onLogoutSuccess();
        }
    }

    protected void onFailCallback(boolean isLogin) {
        if (getActivity() instanceof ILoginCallback) {
            ((ILoginCallback) getActivity()).onFail(isLogin);
        }
    }

    protected void onOptionMenuVisibility(OptionMenuType menuType) {
        if (getActivity() instanceof OnMenuVisibilityListener) {
            ((OnMenuVisibilityListener) getActivity()).onOptionMenuVisibility(menuType);
        }
    }

//    @Override
//    public void onAttach(@NonNull Context context) {
//        super.onAttach(context);
//    }

    protected void setLocale() {
        if (getActivity() != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleUtil.onAttach(getActivity());
        }
    }
}
