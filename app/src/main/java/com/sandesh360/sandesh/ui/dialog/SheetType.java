package com.sandesh360.sandesh.ui.dialog;

import java.io.Serializable;

/**
 * Created by Mobarak on November 17, 2018
 *
 * @author Sandesh360
 */
public enum SheetType implements Serializable {
    LANGUAGE_CHANGE, AUTO_PLAY, LOGOUT
}
