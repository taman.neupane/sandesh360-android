package com.sandesh360.sandesh.ui.introduction.moto;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class MotoViewModel extends BaseViewModel<MotoNavigator> {
    public MotoViewModel(FragmentActivity context, MotoNavigator navigator) {
        super(context, navigator);
    }
}
