package com.sandesh360.sandesh.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.model.User;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.ILoginCallback;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import butterknife.BindView;

public class SignInFragment extends BaseFragment<SignInViewModel> implements SignInNavigator, ILoginCallback {

    private static final String TAG = SignInFragment.class.getSimpleName();
    private SignInViewModel mViewModel;


    @BindView(R.id.btn_phone_sign_in)
    Button btnPhoneSignIn;

    @BindView(R.id.btn_google_sign_in)
    SignInButton btnGoogleSignIn;
    @BindView(R.id.btn_fb_sign_in)
    LoginButton btnFbSignIn;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    private User user;
    private boolean isFbLogin = false;

    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sign_in_fragment, container, false);
        SignInController.getInstance(getActivity());
        initializedView(view);
        setGooglePlusButtonText(btnGoogleSignIn, getString(R.string.sign_in_with_google));
        setClickListener();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }


    @Override
    protected void setClickListener() {
        btnGoogleSignIn.setOnClickListener(v -> {
            isFbLogin = false;
            SignInController.getInstance(getActivity()).googleSignIn(this);
        });

        btnFbSignIn.setOnClickListener(v -> {
            isFbLogin = true;
            SignInController.getInstance(getActivity()).fbSignIn(this);
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitleText(getString(R.string.text_sign_in));
        setToolbarVisibility(true);
        setBackArrowVisibility(true);
    }

    @Override
    public SignInViewModel getViewModel() {
//        mViewModel = new SignInViewModel(getActivity(), this);
//        mViewModel = (SignInViewModel) new ViewModelFactory(mViewModel).create(SignInViewModel.class);
        return mViewModel;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        setBackArrowVisibility(false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        SignInController.getInstance(getActivity()).onActivityResult(requestCode, resultCode, data);

    }

    protected void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        // Find the TextView that is inside of the SignInButton and set its text
        if (signInButton != null) {
            signInButton.setSize(SignInButton.SIZE_WIDE);
            for (int i = 0; i < signInButton.getChildCount(); i++) {
                View v = signInButton.getChildAt(i);

                if (v instanceof TextView) {
                    TextView tv = (TextView) v;
                    tv.setText(buttonText);
                    return;
                }
            }
        }
    }

    @Override
    public void onLoginSuccess(User user) {
        this.user = user;
        if (user != null) {
            DataManager.getInstance().setUserInfo(user, isFbLogin);
            UtilityFunctions.loadCircleImage(getActivity(), user.getImageUrl(), R.drawable.ic_newspaper, ivLogo);
        }
        onLoginSuccessCallback(user);

    }

    @Override
    public void onLogoutSuccess() {
        onLogoutSuccessCallback();
    }

    @Override
    public void onFail(boolean isLogin) {
        onFailCallback(isLogin);
    }
}
