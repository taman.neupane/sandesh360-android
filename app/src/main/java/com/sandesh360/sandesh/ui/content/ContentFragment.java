package com.sandesh360.sandesh.ui.content;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.PixelCopy;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.material.card.MaterialCardView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.data.db.entity.Poll;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.model.NewsBottomAds;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;
import com.sandesh360.sandesh.ui.content.container.OnSwipeTouchListener;
import com.sandesh360.sandesh.ui.content.container.adapter.IBookmarkClickListener;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;
import com.sandesh360.sandesh.ui.webcontent.WebActivity;
import com.sandesh360.sandesh.utils.AnimationUtil;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.FacebookPlayer;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.regex.Pattern;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.blurry.Blurry;

public class ContentFragment extends BaseFragment<ContentViewModel> implements ContentNavigator, FacebookPlayer.FacebookListener {

    private static final String TAG = ContentFragment.class.getSimpleName();
    private static final String BUNDLE_KEY_CONTENT = "bundle_key_content";
    private static final Pattern youtubePattern = Pattern.compile("^(http(s)?:\\/\\/)?((w){3}.)?youtu(be|.be)?(\\.com)?\\/.+");
    private static final Pattern facebookPattern = Pattern.compile("^(http(s)?:\\/\\/)?((w){3}.)?facebook?(\\.com)?\\/.+");
    private static final CookieManager DEFAULT_COOKIE_MANAGER;

    static {
        DEFAULT_COOKIE_MANAGER = new CookieManager();
        DEFAULT_COOKIE_MANAGER.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
    }

    private ContentViewModel mViewModel;

    @BindView(R.id.iv_content_images)
    ImageView ivContentImage;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_author)
    TextView tvAuthor;
    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.bottom_news_ads)
    CardView bottomNewsAds;

    @BindView(R.id.adsIcon)
    ImageView adsIcon;

    @BindView(R.id.adsCloseAnchor)
    ImageView adsCloseAnchor;

    @BindView(R.id.bottom_news_sub_detail)
    ConstraintLayout bottomNewsSubDetail;

    @BindView(R.id.tv_more_title)
    TextView tvMoreTextTitle;

    @BindView(R.id.tv_tap_here)
    TextView tvMoreTextTapHere;
    @BindView(R.id.tv_poll_question)
    TextView tvPollQuestion;
    @BindView(R.id.tv_bookmark_text)
    TextView tvBookMark;
    @BindView(R.id.tv_share_text)
    TextView tvShare;
    @BindView(R.id.btn_yes)
    Button btnYes;
    @BindView(R.id.btn_no)
    Button btnNo;
    @BindView(R.id.iv_bookmark_icon)
    Button btnBookmark;

    @BindView(R.id.layout_content)
    ConstraintLayout layoutContent;
    @BindView(R.id.content_footer)
    LinearLayout layoutFooter;
    @BindView(R.id.footerBlurImage)
    ImageView footerBlurImage;
    @BindView(R.id.layout_more)
    ConstraintLayout layoutTapToMore;
    @BindView(R.id.pollBlurImageView)
    ImageView pollBlurImageView;
    @BindView(R.id.layout_share)
    LinearLayout layoutShare;
    @BindView(R.id.layout_bookmark)
    LinearLayout layoutBookmark;
    @BindView(R.id.layout_content_poll)
    ConstraintLayout layoutPoll;
    @BindView(R.id.poll_progress)
    ProgressBar progressBar;
    @BindView(R.id.youtube_player_view)
    YouTubePlayerView youTubePlayerView;

    @BindView(R.id.otherVideoPlayer)
    PlayerView otherVideoPlayer;

    @BindView(R.id.facebookVideoPlayer)
    FacebookPlayer facebookVideoPlayer;

    @BindView(R.id.facebookVideoPlayerWrapper)
    FrameLayout facebookVideoPlayerWrapper;

    @BindView(R.id.facebookProgressBar)
    ProgressBar fbVideoProgressBar;

    private boolean isYesClick;

    private Content content;
    private NewsBottomAds newsBottomAds;
    private boolean isBookmark = false;
    private PreferenceType type;
    private final CompositeDisposable mDisposable = new CompositeDisposable();
    private IBookmarkClickListener bookmarkClickListener;

    private YouTubePlayer youTubePlayer;
    private SimpleExoPlayer player;


    public void setBookmarkClickListener(IBookmarkClickListener bookmarkClickListener) {
        this.bookmarkClickListener = bookmarkClickListener;
    }

    public ContentFragment() {

    }

    public static ContentFragment newInstance(Content content, PreferenceType type, NewsBottomAds newsBottomAds) {
        ContentFragment fragment = new ContentFragment();
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_KEY_CONTENT, content);
        args.putSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE, type);
        args.putSerializable(Constants.BUNDLE_KEY_NEWS_BOTTOM_ADS, newsBottomAds);
        fragment.setArguments(args);
        return fragment;
    }

//    https://www.facebook.com/facebook/videos/10153231379946729/

    private void initVideoPlayer() {
//        content.getImage().setVideoUrl("https://www.facebook.com/sastodeal/videos/845327048870405/");
        if (content.getImage().getVideoUrl().matches(youtubePattern.pattern())) {
            youTubePlayerView.setVisibility(View.VISIBLE);
            ivContentImage.setVisibility(View.GONE);
            otherVideoPlayer.setVisibility(View.GONE);

            youTubePlayerView.getPlayerUIController().showFullscreenButton(false);
            getLifecycle().addObserver(youTubePlayerView);

            youTubePlayerView.initialize(youTubePlayer -> {
                this.youTubePlayer = youTubePlayer;
                youTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        loadVideo(youTubePlayer, content.getImage().getYouTubeUrl());
                    }
                });

            }, true);
        } else if (content.getImage().getVideoUrl().matches(facebookPattern.pattern())) {
            ivContentImage.setVisibility(View.GONE);
            youTubePlayerView.setVisibility(View.GONE);
            facebookVideoPlayerWrapper.setVisibility(View.VISIBLE);
            fbVideoProgressBar.setVisibility(View.VISIBLE);

            facebookVideoPlayer.setAutoPlay(true);
            facebookVideoPlayer.setShowCaptions(false);
            facebookVideoPlayer.setShowText(false);

            facebookVideoPlayer.initialize(getString(R.string.facebook_app_id), content.getImage().getVideoUrl(), this);
            facebookVideoPlayer.setAutoPlayerHeight(this.requireContext());
        } else {
            youTubePlayerView.setVisibility(View.GONE);
            ivContentImage.setVisibility(View.GONE);
            otherVideoPlayer.setVisibility(View.VISIBLE);

            if (player == null) {
                player = ExoPlayerFactory.newSimpleInstance(
                        new DefaultRenderersFactory(this.requireContext()),
                        new DefaultTrackSelector());
            }

            otherVideoPlayer.setPlayer(player);

            player.setPlayWhenReady(true);
            player.seekTo(0, 0);

            Uri uri = Uri.parse(content.getImage().getVideoUrl());
            MediaSource mediaSource = buildMediaSource(uri);
            player.prepare(mediaSource, true, false);
        }
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource.Factory(
                new DefaultHttpDataSourceFactory("sandesh360")).
                createMediaSource(uri);
    }

    private void loadVideo(YouTubePlayer youTubePlayer, String videoId) {
        if (getLifecycle().getCurrentState() == Lifecycle.State.RESUMED && isVisible()) {
            youTubePlayer.cueVideo(videoId, 0);
        } else {
            youTubePlayer.pause();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser && youTubePlayer != null) {
            youTubePlayer.pause();

        }
        if (!isVisibleToUser && facebookVideoPlayer != null) {
            releasePlayer();
        } else if (isVisibleToUser && isResumed()) {
            if (isToolbarBottomBarVisible()) {
                setBottomBarVisibility(false);
                setToolbarBottomBarVisibility(false);
            }
            onResume();
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (CookieHandler.getDefault() != DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER);
        }
        if (getArguments() != null) {
            content = (Content) getArguments().getSerializable(BUNDLE_KEY_CONTENT);
            type = (PreferenceType) getArguments().getSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE);
            newsBottomAds = (NewsBottomAds) getArguments().getSerializable(Constants.BUNDLE_KEY_NEWS_BOTTOM_ADS);
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_fragment, container, false);
        initializedView(view);
        setClickListener();
        updateUI();
        return view;
    }

    private void updateUI() {
        try {
            if (content == null) return;
            tvTitle.setText(content.getTitle());
            tvDescription.setText(content.getDescription().trim());

            tvMoreTextTitle.setText(content.getShortDescription());

            if (newsBottomAds == null) {
                bottomNewsAds.setVisibility(View.GONE);
                bottomNewsSubDetail.setVisibility(View.VISIBLE);
                layoutTapToMore.setOnClickListener(v -> {
                    goWebActivity();
                });
            } else {
                bottomNewsSubDetail.setVisibility(View.GONE);
                bottomNewsAds.setVisibility(View.VISIBLE);
                UtilityFunctions.loadImage(getActivity(),
                        ApiUrls.BASE_IMAGE_URL + newsBottomAds.getIcon(),
                        R.drawable.category_placeholder, adsIcon);
                adsIcon.setOnClickListener(v -> {
                    Bundle bundle = new Bundle();
                    if (content != null) {
                        bundle.putString(Constants.BUNDLE_KEY_SITE_URL, newsBottomAds.getLink().trim());
                        bundle.putString(Constants.BUNDLE_KEY_CONTENT_URL, newsBottomAds.getLink().trim());
                    }
                    UtilityFunctions.startActivity(getActivity(), WebActivity.class, bundle);
                });
            }

            adsCloseAnchor.setOnClickListener(v -> {
                AnimationUtil.slideDown(bottomNewsAds, null);
//                bottomNewsAds.setVisibility(View.GONE);
                AnimationUtil.slideUp(bottomNewsSubDetail, null);
                layoutTapToMore.setOnClickListener(view -> {
                    goWebActivity();
                });
            });


            tvAuthor.setText(getString(R.string.swipe_left_for_more));
            tvDate.setText(UtilityFunctions.getFormattedDateTime(content.getUpdatedAt(), Constants.DATE_TIME_FORMAT, getActivity()));
            tvMoreTextTapHere.setText(R.string.tap_here_to_find_more);
            tvBookMark.setText(R.string.text_bookmark);
            tvShare.setText(R.string.text_share);

            if (content.getImage() != null && content.getImage().hasVideo()) {
                initVideoPlayer();
            } else {
                youTubePlayerView.setVisibility(View.GONE);
                ivContentImage.setVisibility(View.VISIBLE);
                UtilityFunctions.loadImage(getActivity(),
                        ApiUrls.CONTENT_ICON_URL + content.getImage().getImageUrl(),
                        R.drawable.category_placeholder, ivContentImage);

                UtilityFunctions.loadImageWithBitmap(this.requireContext(), ApiUrls.CONTENT_ICON_URL + content.getImage().getImageUrl(), new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        try {
                            Blurry.BitmapComposer bitmapComposer = Blurry.with(ContentFragment.this.requireContext())
                                    .radius(14)
                                    .sampling(8)
                                    .from(resource);
                            if (PreferenceType.POLL == type) {
                                bitmapComposer.into(pollBlurImageView);
                            } else {
                                bitmapComposer.into(footerBlurImage);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            if (PreferenceType.POLL == type && content.getQuestion() != null) {
                layoutTapToMore.setVisibility(View.GONE);
                layoutPoll.setVisibility(View.VISIBLE);
                tvPollQuestion.setText(content.getQuestion().getQuestionText());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void setClickListener() {
        layoutContent.setOnTouchListener(new OnSwipeTouchListener(getActivity(),
                new OnSwipeTouchListener.TouchListener() {
                    @Override
                    public void onSingleTap() {
                        Log.i(TAG, "onSingleTapConfirmed call");
                        setToolbarVisibility(!isToolbarVisible());
                        setBottomBarVisibility(!isToolbarBottomBarVisible());
                    }

                    @Override
                    public void onDoubleTap() {
                        Log.i(TAG, "onDoubleTap call");
                    }

                    @Override
                    public void onScroll() {
                        Log.i(TAG, "onScroll call");
                        if (isToolbarVisible()) {
                            setToolbarVisibility(false);
                        }
                    }

                    @Override
                    public void onSwipeRight() {
                        Log.i(TAG, "onSwipeRight call");

                    }

                    @Override
                    public void onSwipeLeft() {
                        Log.i(TAG, "onSwipeLeft call");
                        goWebActivity();

                    }

                    @Override
                    public void onSwipeTop() {
                        Log.i(TAG, "onSwipeTop call");

                    }

                    @Override
                    public void onSwipeBottom() {
                        Log.i(TAG, "onSwipeBottom call");

                    }
                }));

        tvTitle.setOnClickListener(v -> {
            Log.i(TAG, "title is clicked");
            updateBookmarkStatusByContentId();

        });

        layoutBookmark.setOnClickListener(v -> {
            Log.i(TAG, "bookmark is clicked");
            updateBookmarkStatusByContentId();

        });
        layoutShare.setOnClickListener(v -> {
            Log.i(TAG, "share is clicked");
            getScreenShotFromView(layoutContent, this.requireActivity());
        });
        btnYes.setOnClickListener(v -> {
            Log.i(TAG, "yes is clicked");
            if (content != null && content.getQuestion() != null) {
                isYesClick = true;
                btnNo.setClickable(false);
                mViewModel.invokePollApi(content.getQuestion().getId(), true);
            }
        });
        btnNo.setOnClickListener(v -> {
            Log.i(TAG, "no is clicked");
            if (content != null && content.getQuestion() != null) {
                isYesClick = false;
                btnYes.setClickable(false);
                mViewModel.invokePollApi(content.getQuestion().getId(), false);
            }
        });
    }

    @Override
    public void goWebActivity() {
        Bundle bundle = new Bundle();
        if (content != null) {
            bundle.putString(Constants.BUNDLE_KEY_SITE_URL, content.getSiteUrl().trim());
            bundle.putString(Constants.BUNDLE_KEY_CONTENT_URL, content.getContentUrl().trim());
        }
        UtilityFunctions.startActivity(getActivity(), WebActivity.class, bundle);
    }

    @Override
    public void updateUIAccordingToBookmarkStatus() {
        mDisposable.add(mViewModel.getBookmarkedStatus(content.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> setBookmarkStatus(aBoolean == null ? false : aBoolean),
                        throwable -> Log.e(TAG, "No content found", throwable))
        );
    }

    @Override
    public void updateReadStatus() {
        mDisposable.add(mViewModel.updateReadStatusByContentId(content.getId(), true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.i(TAG, "ContentLocalStatus status updated");
                        },
                        throwable -> Log.e(TAG, "No content found", throwable))
        );
    }


    @Override
    public void setBookmarkStatus(boolean isBookmark) {
        this.isBookmark = isBookmark;
        if (isBookmark) {
            tvTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
            btnBookmark.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ic_bookmark_fill));
        } else {
            tvTitle.setTextColor(ContextCompat.getColor(getActivity(), R.color.darkest_grey));
            btnBookmark.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.ic_bookmark_outline));
        }
    }

    @Override
    public boolean isBookmarked() {
        return isBookmark;
    }

    @Override
    public void updateBookmarkStatusByContentId() {
        mDisposable.add(mViewModel.updateBookmarkStatusByContentId(content.getId(), !isBookmarked())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.i(TAG, "ContentLocalStatus status updated");
                            if (!isBookmarked()) {
                                Toast.makeText(getActivity(), getString(R.string.news_bookmark), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.bookmark_removed), Toast.LENGTH_SHORT).show();
                            }
                        },
                        throwable -> Log.e(TAG, "No content found", throwable))
        );
    }

    @Override
    public void setVisibityOfPollProgress(boolean isVisible) {
        if (progressBar != null) progressBar.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void initPoll() {
        if (content == null || content.getQuestion() == null) return;
        mDisposable.add(mViewModel.getPollByQuestionId(content.getQuestion().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(poll -> {
                            btnYes.setClickable(false);
                            btnNo.setClickable(false);
                            btnYes.setText(getString(R.string.yes) + " (" + UtilityFunctions.getTwoDigitAfterDecimalPoint(poll.getYes()) + ")");
                            btnNo.setText(getString(R.string.no) + " (" + UtilityFunctions.getTwoDigitAfterDecimalPoint(poll.getNo()) + ")");
                            if (poll.isYes()) {
                                btnYes.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                                btnYes.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                            } else {
                                btnNo.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
                                btnNo.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                            }
                        },
                        throwable -> Log.e(TAG, "Couldn't vote yet", throwable))
        );

    }

    @Override
    public void insertPoll(Poll poll) {
        if (poll != null) {
            poll.setIsYes(isYesClick);
            poll.setQuestionId(content.getQuestion().getId());
            mDisposable.add(mViewModel.insertPoll(poll)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                                Log.i(TAG, "ContentLocalStatus status updated");
                                setToolbarVisibility(false);
                            },
                            throwable -> {
                                setToolbarVisibility(false);
                                Log.e(TAG, "No content found", throwable);
                            })
            );
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel();
    }

    @Override
    public ContentViewModel getViewModel() {
        mViewModelFactory = Injection.provideViewModelFactory(getActivity(), this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ContentViewModel.class);
        return mViewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            updateReadStatus();
            initPoll();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        updateUIAccordingToBookmarkStatus();
    }

    @Override
    public void onStop() {
        super.onStop();
        releasePlayer();
        mDisposable.clear();
    }

    @Override
    public void setBottomBarVisibility(boolean isVisible) {
        if (PreferenceType.POLL != type) {
            if (layoutFooter != null) {
                if (isVisible) {
                    layoutFooter.setVisibility(View.VISIBLE);
                    AnimationUtil.slideUp(layoutFooter, null);
                    AnimationUtil.slideDown(layoutTapToMore, null);
                    layoutTapToMore.setVisibility(View.GONE);
                } else {
                    layoutTapToMore.setVisibility(View.VISIBLE);
                    AnimationUtil.slideUp(layoutTapToMore, null);
                    AnimationUtil.slideDown(layoutFooter, null);
                    layoutFooter.setVisibility(View.GONE);
                }
            }
        } else {
            if (layoutFooter != null) {
                if (isVisible) {
                    AnimationUtil.slideDown(layoutPoll, null);
                    layoutPoll.setVisibility(View.GONE);
                    layoutFooter.setVisibility(View.VISIBLE);
                    AnimationUtil.slideUp(layoutFooter, null);
                } else {
                    layoutPoll.setVisibility(View.VISIBLE);
                    AnimationUtil.slideUp(layoutPoll, null);
                    AnimationUtil.slideDown(layoutFooter, null);
                    layoutFooter.setVisibility(View.GONE);
                }
            }
        }

    }

    @Override
    public boolean isBottomBarVisible() {
        return layoutFooter == null ? false : (layoutFooter.getVisibility() == View.VISIBLE);
    }

    @Override
    public boolean isToolbarBottomBarVisible() {
        return isBottomBarVisible() && isToolbarVisible();
    }

    @Override
    public void setToolbarBottomBarVisibility(boolean isVisible) {
        setToolbarVisibility(isVisible);
        setBottomBarVisibility(isVisible);
    }

    private void getScreenShotFromView(View view, Activity activity) {
        setToolbarBottomBarVisibility(false);
        new Handler().postDelayed(() -> {
            Window window = activity.getWindow();
            if (window != null) {
                Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
                int[] locationOfViewInWindow = new int[2];
                view.getLocationInWindow(locationOfViewInWindow);
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        PixelCopy.request(
                                window,
                                new Rect(
                                        locationOfViewInWindow[0],
                                        locationOfViewInWindow[1],
                                        locationOfViewInWindow[0] + view.getWidth(),
                                        locationOfViewInWindow[1] + view.getHeight()
                                ), bitmap, copyResult -> {
                                    if (copyResult == PixelCopy.SUCCESS) {
                                        Log.d(TAG, "Pixel Success");
                                        callback(bitmap);
                                    } else {
                                        Log.d(TAG, "Pixel Failure");
                                    }
                                }, new Handler());
                    } else {
                        Log.d(TAG, "Below O");
                        callback(getScreenShot(view));
                    }
                } catch (Exception e) {
                    Log.d(TAG, "Screenshot exception");
                    e.printStackTrace();
                }
            }
        }, 500);


    }

    private Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    private void callback(Bitmap bitmap) {
        Log.d(TAG, "Screen shot callback");
        mViewModel.takeAndShareScreenshot(bitmap, content.getTitle());
    }

    private void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
            otherVideoPlayer.setPlayer(null);
        }

        if (facebookVideoPlayer != null) {
            facebookVideoPlayer.mute();
            facebookVideoPlayer.pause();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releasePlayer();
    }


    @Override
    public void onStartBuffer() {

    }

    @Override
    public void onFinishBuffering() {
        fbVideoProgressBar.setVisibility(View.GONE);
        facebookVideoPlayer.unmute();
    }

    @Override
    public void onStartPlaying() {
        fbVideoProgressBar.setVisibility(View.GONE);
        facebookVideoPlayer.unmute();
    }

    @Override
    public void onFinishPlaying() {

    }

    @Override
    public void onPaused() {

    }

    @Override
    public void onError() {

    }
}
