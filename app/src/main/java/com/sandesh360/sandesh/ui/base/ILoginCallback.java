package com.sandesh360.sandesh.ui.base;

import com.sandesh360.sandesh.data.model.User;

/**
 * Created by Mobarak on November 16, 2018
 *
 * @author Sandesh360
 */
public interface ILoginCallback {

    void onLoginSuccess(User user);

    void onLogoutSuccess();

    void onFail(boolean isLogin);
}
