package com.sandesh360.sandesh.ui.introduction.splash;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.data.DataManager;
import com.sandesh360.sandesh.data.db.IDbHelper;
import com.sandesh360.sandesh.data.db.entity.Language;
import com.sandesh360.sandesh.data.db.entity.Preference;
import com.sandesh360.sandesh.data.network.ApiError;
import com.sandesh360.sandesh.data.network.ApiUrls;
import com.sandesh360.sandesh.data.network.IResponseCallback;
import com.sandesh360.sandesh.data.network.model.BaseResponse;
import com.sandesh360.sandesh.data.network.model.LanguageResponse;
import com.sandesh360.sandesh.data.network.model.PreferenceResponse;
import com.sandesh360.sandesh.ui.base.IBaseNavigator;
import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;
import com.sandesh360.sandesh.utils.LocaleUtil;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class SplashViewModel extends BaseViewModel<SplashNavigator> implements IResponseCallback<BaseResponse> {

    public SplashViewModel(FragmentActivity context, IBaseNavigator navigator, IDbHelper dataSource) {
        super(context, (SplashNavigator) navigator, dataSource);
    }

    public void invokeLanguageApi() {
        DataManager.getInstance().invokeLanguageApi(ApiUrls.LANGUAGE_URL, this);
    }

    public void invokePreferencesApi() {
        DataManager.getInstance().invokeActivePreferencesApi(ApiUrls.ACTIVE_PREFERENCE_URL + LocaleUtil.getLanguageId(), this);
    }

    public Completable insertLanguage(List<Language> languages) {
        return dataSource.insertLanguage(languages);
    }

    public Flowable<List<Language>> getLanguages() {
        return dataSource.getLanguages();
    }


    public Completable insertPreference(List<Preference> preferences) {
        return dataSource.insertPreference(preferences);
    }


    @Override
    public void onSuccessResponse(BaseResponse response) {

        if (response instanceof LanguageResponse) {
            LanguageResponse lResponse = (LanguageResponse) response;
            if (lResponse != null && lResponse.getLanguages() != null) {
                getNavigator().insertLanguages(lResponse.getLanguages());
            }
        } else if (response instanceof PreferenceResponse) {
            PreferenceResponse pResponse = (PreferenceResponse) response;
            if (pResponse != null && pResponse.getPreferences() != null) {
                getNavigator().insertPreferences(pResponse.getPreferences());
            }
        }

    }

    @Override
    public void onErrorResponse(ApiError error) {
        getNavigator().screenNavigation();
    }
}
