package com.sandesh360.sandesh.ui.webcontent;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class WebViewModel extends BaseViewModel<WebNavigator> {
    public WebViewModel(FragmentActivity context, WebNavigator navigator) {
        super(context, navigator);
    }

}
