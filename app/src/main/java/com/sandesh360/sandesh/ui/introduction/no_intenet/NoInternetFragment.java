package com.sandesh360.sandesh.ui.introduction.no_intenet;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;

public class NoInternetFragment extends BaseFragment<NoInternetViewModel> implements NoInternetNavigator {

    private NoInternetViewModel mViewModel;

    public static NoInternetFragment newInstance() {
        return new NoInternetFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.no_internet_fragment, container, false);
        initializedView(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel();
    }

    @Override
    public NoInternetViewModel getViewModel() {
//        mViewModel = new NoInternetViewModel(getActivity(), this);
//        mViewModel = (NoInternetViewModel) new ViewModelFactory(mViewModel).create(NoInternetViewModel.class);
        return mViewModel;
    }
}
