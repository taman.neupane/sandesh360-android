package com.sandesh360.sandesh.ui.content.container;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.data.db.entity.Content;
import com.sandesh360.sandesh.ui.ads.AdsFragment;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;
import com.sandesh360.sandesh.ui.content.ContentFragment;
import com.sandesh360.sandesh.ui.content.container.adapter.ContentPagerAdapter;
import com.sandesh360.sandesh.ui.navdrawer.OnOptionMenuClickListener;
import com.sandesh360.sandesh.ui.navdrawer.OptionMenuType;
import com.sandesh360.sandesh.ui.navdrawer.PreferenceType;
import com.sandesh360.sandesh.utils.Constants;
import com.sandesh360.sandesh.utils.UtilityFunctions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


public class ContentContainerFragment extends BaseFragment<ContentContainerViewModel> implements ContentContainerNavigator, OnOptionMenuClickListener {

    private static final String TAG = ContentContainerFragment.class.getSimpleName();


    private ContentContainerViewModel mViewModel;

    @BindView(R.id.view_pager)
    SandeshViewPager viewPager;
    @BindView(R.id.layout_no_internet)
    RelativeLayout layoutNoInternet;
    @BindView(R.id.tv_internet_setting)
    TextView tvInternetSetting;

    @BindView(R.id.layout_no_news_found)
    RelativeLayout layoutNoNewsFound;
    @BindView(R.id.iv_no_news_found)
    ImageView ivTryAgain;

    private ContentPagerAdapter pagerAdapter;
    private PreferenceType type;
    private Long categoryId;
    private List<Content> contentList;
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    private boolean isFromUpdate = false;

    private int notificationIdViewPagerPosition = 0;


    public ContentContainerFragment() {
    }

    public static ContentContainerFragment newInstance(PreferenceType type, Long categoryId) {
        ContentContainerFragment fragment = new ContentContainerFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE, type);
        args.putLong(Constants.BUNDLE_KEY_CATEGORY_ID, categoryId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ContentContainerFragment newInstance(PreferenceType type, Long categoryId, String notificationContentId) {
        ContentContainerFragment fragment = new ContentContainerFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE, type);
        args.putLong(Constants.BUNDLE_KEY_CATEGORY_ID, categoryId);
        args.putLong(Constants.BUNDLE_KEY_NOTIFICATION_CONTENT_ID, Long.parseLong(notificationContentId));
        fragment.setArguments(args);
        return fragment;
    }

    public static ContentContainerFragment newInstance(PreferenceType type, Long categoryId, Long contentId) {
        ContentContainerFragment fragment = new ContentContainerFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE, type);
        args.putLong(Constants.BUNDLE_KEY_CATEGORY_ID, categoryId);
        args.putLong(Constants.BUNDLE_KEY_NOTIFICATION, contentId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ContentContainerFragment newInstance(PreferenceType type, List<Content> contentList) {
        ContentContainerFragment fragment = new ContentContainerFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE, type);
        args.putSerializable(Constants.BUNDLE_KEY_CONTENT_LIST, (Serializable) contentList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = (PreferenceType) getArguments().getSerializable(Constants.BUNDLE_KEY_PREFERENCE_TYPE);
            categoryId = getArguments().getLong(Constants.BUNDLE_KEY_CATEGORY_ID);
            Long contentId = getArguments().getLong(Constants.BUNDLE_KEY_NOTIFICATION);
            contentList = (List<Content>) getArguments().getSerializable(Constants.BUNDLE_KEY_CONTENT_LIST);
        }
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_container_fragment, container, false);
        initializedView(view);
        if (!UtilityFunctions.isNetworkAvailable(this.requireContext())) {
            layoutNoInternet.setVisibility(View.VISIBLE);
        } else {
            layoutNoInternet.setVisibility(View.GONE);
        }
        setClickListener();
        return view;
    }

    @Override
    protected void setClickListener() {
        tvInternetSetting.setOnClickListener(v -> startActivityForResult(
                new Intent(android.provider.Settings.ACTION_SETTINGS),
                0));

        ivTryAgain.setOnClickListener(v -> mViewModel.invokeAPI());
    }

    private void setUpPager(List<Content> contents) {

        try {
            if (contents != null && contents.size() > 0) {
                layoutNoInternet.setVisibility(View.GONE);
                layoutNoNewsFound.setVisibility(View.GONE);
                mViewModel.setLastUpdated(contents.get(0).getUpdatedAt());
                List<Fragment> fragments = new ArrayList<>();

                if (!mViewModel.getAdsList().isEmpty()) {
                    int newsBottomAdsIndex = 0;
                    for (int contentIndex = 0; contentIndex < contents.size(); contentIndex++) {
                        ContentFragment fragment;
                        if(!mViewModel.getNewsBottomAds().isEmpty()){
                            if(newsBottomAdsIndex > mViewModel.getNewsBottomAds().size()-1){
                                newsBottomAdsIndex = 0;
                            }
                           fragment  = ContentFragment.newInstance(contents.get(contentIndex), type, mViewModel.getNewsBottomAds().get(newsBottomAdsIndex));
                            newsBottomAdsIndex += 1;
                        }else {
                            fragment  = ContentFragment.newInstance(contents.get(contentIndex), type, null);
                        }

                        if (contents.get(contentIndex).getId() == getArguments().getLong(Constants.BUNDLE_KEY_NOTIFICATION_CONTENT_ID)) {
                            notificationIdViewPagerPosition = contentIndex;
                        }
                        fragments.add(fragment);
                    }

                    int i = 0;
                    for (int adsIndex = mViewModel.getFrequency() - 1; adsIndex < contents.size(); adsIndex += mViewModel.getFrequency()) {
                        if (i > mViewModel.getAdsList().size() - 1) {
                            i = 0;
                        }
                        AdsFragment fragment = AdsFragment.newInstance(mViewModel.getAdsList().get(i));
                        fragments.add(adsIndex, fragment);
                        i = i + 1;
                    }

                    if (fragments.size() > 0) {
                        pagerAdapter = new ContentPagerAdapter(this.getChildFragmentManager(), fragments);
                        viewPager.setOffscreenPageLimit(5);
                        viewPager.setAdapter(pagerAdapter);
                        if (Objects.requireNonNull(getArguments()).getLong(Constants.BUNDLE_KEY_NOTIFICATION_CONTENT_ID) != 0L && !isFromUpdate) {
                            viewPager.setCurrentItem(notificationIdViewPagerPosition + (int) Math.floor(notificationIdViewPagerPosition / (mViewModel.getFrequency() - 1)));
                        }else {
                            isFromUpdate = false;
                        }
                    }
                } else {
                    int newsBottomAdsIndex = 0;
                    for (int contentIndex = 0; contentIndex < contents.size(); contentIndex++) {
                        ContentFragment fragment ;
                        if(!mViewModel.getNewsBottomAds().isEmpty()){
                            if(newsBottomAdsIndex == mViewModel.getNewsBottomAds().size()-1){
                                newsBottomAdsIndex = 0;
                            }
                            fragment  = ContentFragment.newInstance(contents.get(contentIndex), type, mViewModel.getNewsBottomAds().get(newsBottomAdsIndex));
                            newsBottomAdsIndex += 1;
                        }else {
                            fragment  = ContentFragment.newInstance(contents.get(contentIndex), type, null);
                        }
                        if (contents.get(contentIndex).getId() == getArguments().getLong(Constants.BUNDLE_KEY_NOTIFICATION_CONTENT_ID)) {
                            notificationIdViewPagerPosition = contentIndex;
                        }
                        fragments.add(fragment);
                    }
                    if (fragments.size() > 0) {
                        pagerAdapter = new ContentPagerAdapter(this.getChildFragmentManager(), fragments);
                        viewPager.setOffscreenPageLimit(5);
                        viewPager.setAdapter(pagerAdapter);
                        if (Objects.requireNonNull(getArguments()).getLong(Constants.BUNDLE_KEY_NOTIFICATION_CONTENT_ID) != 0L && !isFromUpdate) {
                            viewPager.setCurrentItem(notificationIdViewPagerPosition);
                        }else {
                            isFromUpdate = false;
                        }
                    }
                }

            } else {
                if (UtilityFunctions.isNetworkAvailable(this.requireContext())) {
                    layoutNoInternet.setVisibility(View.GONE);
                    layoutNoNewsFound.setVisibility(View.VISIBLE);
                } else {
                    layoutNoInternet.setVisibility(View.VISIBLE);
                    layoutNoNewsFound.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        setToolbarVisibility(true);
        if (PreferenceType.CATEGORY != type && PreferenceType.SEARCH != type)
            setTitleText(mViewModel.setTitleByPreferenceType(type));

    }

    @Override
    public void onStop() {
        super.onStop();
        mDisposable.clear();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel();
        if (PreferenceType.SEARCH == type) {
            loadPager(contentList);
        } else {
            mViewModel.invokeAPI();
        }
    }

    @Override
    public ContentContainerViewModel getViewModel() {
        mViewModelFactory = Injection.provideViewModelFactory(getActivity(), this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(ContentContainerViewModel.class);
        return mViewModel;
    }

    @Override
    public void loadPager(List<Content> contents) {
        if (isAdded())
            setUpPager(contents);
    }

    @Override
    public Long getCategoryId() {
        return categoryId;
    }

    @Override
    public PreferenceType getPreferenceType() {
        return type;
    }

    @Override
    public void searchPreference() {
        mViewModel.invokeContentApi(mViewModel.getUrl(ContentType.PREFERENCE),
                mViewModel.getRequestBody(type.getType(), ContentType.PREFERENCE));
    }

    @Override
    public void insertContent(List<Content> contents) {
        mDisposable.add(mViewModel.insertContent(contents)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.e(TAG, "Successfully inserted.");
                            mViewModel.getAllContentFromDb();
                            UtilityFunctions.stopProgressBar(getActivity());
                        },
                        throwable -> {
                            Log.e(TAG, "Insertion failed", throwable);
                            UtilityFunctions.stopProgressBar(getActivity());
                        }));
    }

    @Override
    public boolean isLoadFromDb() {
        return PreferenceType.MY_FEED == type
                || PreferenceType.ALL_NEWS == type
                || PreferenceType.UNREAD == type
                || PreferenceType.BOOKMARKS == type;
    }

    @Override
    public void onClickMenu(OptionMenuType menuType) {
        if (OptionMenuType.REFRESH == menuType) {
            if (mViewModel != null) {
                isFromUpdate = true;
                mViewModel.invokeAPI();
            }
        } else if (OptionMenuType.GO_UP == menuType) {
            if (viewPager != null) {
                viewPager.setCurrentItem(0, true);
            }
        }
    }
}
