package com.sandesh360.sandesh.ui.feedback;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.base.BaseFragment;
import com.sandesh360.sandesh.ui.base.viewmodel.Injection;

import butterknife.BindView;

public class FeedbackFragment extends BaseFragment<FeedbackViewModel> implements FeedbackNavigator {

    private static final String TAG = FeedbackFragment.class.getSimpleName();

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_comment)
    EditText etComment;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.tv_success)
    TextView tvSuccess;

    private FeedbackViewModel mViewModel;

    public static FeedbackFragment newInstance() {
        return new FeedbackFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedback_fragment, container, false);
        initializedView(view);
        setClickListener();
        return view;
    }

    @Override
    protected void setClickListener() {
        btnSubmit.setOnClickListener(v ->
                mViewModel.validateInput(
                        etEmail.getText().toString(),
                        etComment.getText().toString()
                )
        );
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getViewModel();

    }

    @Override
    public FeedbackViewModel getViewModel() {
        mViewModelFactory = Injection.provideViewModelFactory(getActivity(), this);
        mViewModel = ViewModelProviders.of(this, mViewModelFactory).get(FeedbackViewModel.class);
        return mViewModel;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitleText(getString(R.string.nav_text_feedback));
        setToolbarVisibility(true);
    }

    @Override
    public void onEmailError(String error) {
        if (etEmail != null) {
            etEmail.setError(error);
            etEmail.requestFocus();
        }
    }

    @Override
    public void onCommentError(String error) {
        etComment.setError(error);
        etComment.requestFocus();
    }

    @Override
    public void showSuccessMessage(String message) {
        if (etComment != null) {
            etComment.setText("");
            etEmail.setText("");
        }
        successFailVisibility(message);
    }

    @Override
    public void showFailedMessage(String message) {
        successFailVisibility(message);
    }

    @Override
    public void successFailVisibility(String message) {
        if (tvSuccess != null) {
            tvSuccess.setText(message);
            tvSuccess.setVisibility(View.VISIBLE);
        }
    }
}
