package com.sandesh360.sandesh.ui.custom_view;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.sandesh360.sandesh.R;


/**
 * Created by Mobarak on November 10, 2018
 *
 * @author Sandesh360
 */
public class BookmarkTextView extends AppCompatTextView {

    private static final int[] STATE_BOOKMARK = {R.attr.state_bookmark};

    private boolean isBookmark = false;

    public BookmarkTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isBookmark) {
            mergeDrawableStates(drawableState, STATE_BOOKMARK);
        }
        return drawableState;
    }

    public boolean isBookmark() {
        return isBookmark;
    }

    public void setBookmark(boolean bookmark) {
        isBookmark = bookmark;
    }
}
