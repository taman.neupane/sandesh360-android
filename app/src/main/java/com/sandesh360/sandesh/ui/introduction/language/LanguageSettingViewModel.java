package com.sandesh360.sandesh.ui.introduction.language;

import androidx.fragment.app.FragmentActivity;

import com.sandesh360.sandesh.ui.base.viewmodel.BaseViewModel;

public class LanguageSettingViewModel extends BaseViewModel<LanguageSettingNavigator> {
    public LanguageSettingViewModel(FragmentActivity context, LanguageSettingNavigator navigator) {
        super(context, navigator);
    }
}
