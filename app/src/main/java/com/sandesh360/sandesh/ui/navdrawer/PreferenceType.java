package com.sandesh360.sandesh.ui.navdrawer;

import com.sandesh360.sandesh.utils.LocaleUtil;

import java.io.Serializable;

/**
 * Created by Mobarak on November 08, 2018
 *
 * @author Sandesh360
 */
public enum PreferenceType implements Serializable {

    MY_FEED(2,6),
    ALL_NEWS(1,5),
    TOP_STORIES(3,7),
    TRENDING(4,8),
    BOOKMARKS(0,0),
    UNREAD(0,0),
    CATEGORY(0,0),
    POLL(0,0),
    SEARCH(0,0);

    private int typeBn;
    private int typeEn;

    PreferenceType(int typeEn, int typeBn) {
        this.typeEn = typeEn;
        this.typeBn = typeBn;
    }

    public int getType() {
        return LocaleUtil.isEnglish() ? this.typeEn : this.typeBn;
    }

}
