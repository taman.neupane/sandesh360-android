package com.sandesh360.sandesh.utils;

/**
 * Created by Mobarak on 27-Sep-17.
 *
 * Updated by Moontasirul on 12-26-2017
 */

public class ErrorCode {


    public static final String ERROR_CODE_10001 = "10001";
    public static final String ERROR_CODE_10002 = "10002";
    public static final String ERROR_CODE_10003 = "10003";

}
