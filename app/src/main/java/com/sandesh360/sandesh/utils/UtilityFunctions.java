package com.sandesh360.sandesh.utils;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.sandesh360.sandesh.R;
import com.sandesh360.sandesh.ui.update.ForceUpdateChecker;

import java.net.URI;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by user123 on 9/18/2017.
 */

public class UtilityFunctions {

    private static ProgressDialog progressDialog;


    /**
     * invoke to change fragment
     *
     * @param activity
     * @param fragment
     * @param isAddToBackStack
     * @param isHideSoftKeyboard
     */
    public static void changeFragment(FragmentActivity activity, Fragment fragment, boolean isAddToBackStack, boolean isHideSoftKeyboard) {

        if (activity == null || fragment == null) {
            return;
        }
        if (isHideSoftKeyboard) {
            hideVirtualKeyboard(activity);
        }

        FragmentTransaction transaction = activity.getSupportFragmentManager()
                .beginTransaction();
        if (isAddToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.replace(R.id.container, fragment, Constants.CURRENT_FRAGMENT);
        transaction.commitAllowingStateLoss();
        activity.getSupportFragmentManager().executePendingTransactions();
    }

    /**
     * invoke to hide keyboard
     *
     * @param activity
     */
    public static void hideVirtualKeyboard(FragmentActivity activity) {
        if (activity == null) {
            return;
        }

        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(null == activity
                .getCurrentFocus() ? null : activity.getCurrentFocus()
                .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public static void showSoftKeyboard(FragmentActivity activity, View view) {
        if (activity == null || view == null) {
            return;
        }

        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }


    @SuppressLint("MissingPermission")
    public static String getDeviceId(Context context) {
        if (context == null) {
            return "";
        }

        boolean isTelephonyService = false;
        PackageManager packageManager = context.getPackageManager();
        // Check the Package Manager isn't exists
        if (packageManager != null) {
            isTelephonyService = packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        }
        // Check it has Telephony Service
        if (isTelephonyService) {
            // Get the IMEI number
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                return "IMEI is :" + telephonyManager.getImei();
            } else {
                return "IMEI is :" + telephonyManager.getDeviceId();
            }
        } else {
            // Get the MAC Address
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            return "MAC Address: " + wifiManager.getConnectionInfo().getMacAddress();
        }

    }


    /**
     * This method convert dp to pixel
     *
     * @param dp
     * @param context
     * @return
     */
    public static float dpToPixel(float dp, Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static boolean isAlphaNumeric(String aString) {
        String reges = "[A-Za-z0-9]+";
        return aString.matches(reges);
    }

    public static boolean isNumeric(String aString) {
        String reges = "[0-9]+";
        return aString.matches(reges);
    }

    public static boolean isEmailValid(String email) {
        if (isEmpty(email)) return false;
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static void showProgressBar(final FragmentActivity aActivity) {

        try {
            if (progressDialog != null || aActivity == null) {
                return;
            }

            progressDialog = new ProgressDialog(aActivity);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(aActivity.getString(R.string.connecting));
            progressDialog.setCancelable(false);
            progressDialog.show();


        } catch (Exception ex) {

        }

    }

    public static void stopProgressBar(final FragmentActivity aActivity) {
        try {

            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception ex) {

        }
    }

    /**
     * This method is checked internet availability's
     *
     * @param context
     * @return true|false
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public static void setPlaceHolder(final View view, final String hintText) {
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (view instanceof AutoCompleteTextView) {

                        ((AutoCompleteTextView) view).setHint(hintText);
                    } else {
                        ((EditText) view).setHint(hintText);
                    }
                } else {
                    if (view instanceof AutoCompleteTextView) {
                        ((AutoCompleteTextView) view).setHint(null);
                    } else {
                        ((EditText) view).setHint(null);
                    }
                }
            }
        });
    }


    public static void setStatusBarTransparent(FragmentActivity activity, boolean isTransparent) {
        if (activity == null) {
            return;
        }
        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (isTransparent) {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {

            window.setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDark));
        }


//        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        window.setStatusBarColor(ContextCompat.getColor(activity, R.color.red_text_color));

    }

    public static void setStatusBarColor(FragmentActivity activity, int color) {
        if (activity == null) {
            return;
        }
        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        window.setStatusBarColor(color);
    }

    /**
     * Invoke to enable and disable view
     *
     * @param view
     * @param isEnable true|false
     */
    public static void enableDisableView(View view, final boolean isEnable) {
        view.setEnabled(isEnable);
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int i = 0; i < group.getChildCount(); i++) {
                enableDisableView(group.getChildAt(i), isEnable);
            }
        }
    }

//    public static void loadImage(Context context, String imagePath, int resId, ImageView imageView) {
//        if (context == null || imagePath == null || imageView == null) return;
//        Glide.with(context).load(imagePath)
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .skipMemoryCache(true)
//                .placeholder(resId)
//                .into(imageView);
//    }


    /**
     * Invoke get device width
     *
     * @param context
     * @return
     */
    public static int getDeviceWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    /**
     * Invoke to get device height
     *
     * @param context
     * @return
     */
    public static int getDeviceHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    /**
     * Invoke to convert dp to pixels
     *
     * @param dp
     * @param context
     * @return
     */
    public static int dpToPx(float dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

    /**
     * Invoke to make phone call
     *
     * @param number telephone or cell phone number
     */
    public static void makeCall(FragmentActivity activity, String number) {
        if (activity == null || number == null || TextUtils.isEmpty(number)) {
            return;
        }
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse(Constants.TEL + number.trim()));
        activity.startActivity(Intent.createChooser(callIntent, ""));
    }

    public static void loadImageWithBitmap(Context context, String imagePath,  SimpleTarget<Bitmap> customViewTarget) {
        if (context == null || imagePath == null ) return;
        Glide.with(context)
                .asBitmap()
                .load(imagePath)
                .into(customViewTarget);


    }

    public static void loadImage(Context context, String imagePath, int resId, ImageView imageView) {
        if (context == null || imagePath == null || imageView == null) return;
        Glide.with(context).load(imagePath)
                .apply(new RequestOptions().fitCenter().placeholder(resId).skipMemoryCache(false))
                .into(imageView);
    }


    public static void loadCircleImage(Context context, String imagePath, int resId, ImageView imageView) {
        if (context == null || imagePath == null || imageView == null) return;
        Glide.with(context)
                .load(imagePath)
                .apply(new RequestOptions().circleCropTransform().placeholder(resId))
                .into(imageView);
    }

    public static void startActivity(FragmentActivity activity, Class cls) {
        if (activity == null || cls == null) return;
        Intent intent = new Intent(activity, cls);
        activity.startActivity(intent);
    }

    public static void startActivity(FragmentActivity activity, Class cls, Bundle bundle) {
        if (activity == null || cls == null) return;
        Intent intent = new Intent(activity, cls);
        intent.putExtras(bundle);
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    /**
     * Check to the provided text is empty or not. if is it empty, return true.
     * Otherwise return false
     *
     * @param text
     * @return true|false
     */
    public static boolean isEmpty(String text) {
        return text == null || text.length() <= 0;
    }

    /**
     * invoke to get domain name from link
     *
     * @param url web url
     * @return {String}domain ane
     */
    public static String getDomainName(String url) {
        if (isEmpty(url)) {
            return "";
        }
        try {
            URI uri = new URI(url);
            String domain = uri.getHost();
            return domain.startsWith("www.") ? domain.substring(4) : domain;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Invoke to copy a text into clipboard
     *
     * @param context
     * @param copyText
     */
    public static void copyToClipboard(Context context, String copyText) {
        if (context == null || isEmpty(copyText)) return;
        if (!UtilityFunctions.isEmpty(copyText)) {
            ClipboardManager clipboard = (ClipboardManager)
                    context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText(Constants.SANDESH_360_COPY_TEXT, copyText);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(context, R.string.text_copied, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Invoke to open browser
     *
     * @param context
     * @param url
     */
    public static void openLinkInBrowser(Context context, String url) {
        if (context == null || isEmpty(url)) {
            return;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        String title = context.getString(R.string.chooser_title);
        Intent chooser = Intent.createChooser(browserIntent, title);
        if (browserIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(chooser);
        }
    }

    /**
     * Invoke to share app
     *
     * @param context
     */
    public static void shareApp(Context context) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
            String message = context.getString(R.string.share_message);
            message = message + FirebaseRemoteConfig.getInstance().getString(ForceUpdateChecker.KEY_UPDATE_URL) + "\n";
            shareIntent.putExtra(Intent.EXTRA_TEXT, message);
            Intent chooser = Intent.createChooser(shareIntent, context.getString(R.string.share_chooser_title));
            if (shareIntent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(chooser);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Invoke to rate app
     *
     * @param context
     */
    public static void rateApp(Context context) {
        try {
            Uri uri = Uri.parse("market://details?id=" + context.getPackageName());//"com.mobarakice.yourbabyname");
            Intent rateAppIntent = new Intent(Intent.ACTION_VIEW, uri);
            context.startActivity(rateAppIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, R.string.rate_app_error, Toast.LENGTH_LONG).show();
        }

    }

    public static String getTwoDigitAfterDecimalPoint(double number) {
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(number);
    }


    public static String getFormattedDateTime(Long time, String format, Context context) {
        if (time == null || context == null) {
            return "";
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
            String dateTime = dateFormat.format(time);

            if (!LocaleUtil.isEnglish()) {
                String amPm = dateTime.endsWith("pm") || dateTime.endsWith("PM") ? context.getString(R.string.pm) : context.getString(R.string.am);
                dateTime = dateTime.substring(0, dateTime.length() - 2) + amPm;
            }

            return dateTime;
        } catch (Exception e) {
            return "";
        }
    }
}
