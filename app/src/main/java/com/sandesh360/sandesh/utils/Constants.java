package com.sandesh360.sandesh.utils;

/**
 * Created by Mobarak on 9/18/2017.
 */

public class Constants {

    public static final String SHARE_APP_LINK = "https://play.google.com/store/apps/details?id=com.sandesh360.sandesh";
    // Error Response End Point
    /**
     * End point status
     */
    public static final String STATUS = "Status";
    /**
     * End point status ok
     */
    public static final String STATUS_OK = "Ok";
    /**
     * End point error code
     */
    public static final String ERROR_CODE = "Code";
    /**
     * End point error message
     */
    public static final String ERROR_DESCRIPTION = "Message";
    /**
     * End point Terminal Id
     */
    public static final String TERMINAL_ID = "TerminalId";

    /**
     * End point vendor name
     */
    public static final String NAME = "Name";

    /**
     * End point authentication token
     */
    public static final String TOKEN = "Token";


    /**
     * Current fragment tag
     */
    public static final String CURRENT_FRAGMENT = "current_fragment";

    /**
     * Request id for read phone state
     */
    public static final int REQUEST_READ_PHONE_STATE = 1;

    /**
     * Request id for access device location
     */
    public static final int REQUEST_LOCATION_ACCESS = 2;
    /**
     * Request id for access device storage
     */
    public static final int REQUEST_STORAGE_ACCESS = 3;

    /**
     * Request id for access device camera
     */
    public static final int REQUEST_CAMERA_ACCESS = 4;
    /**
     * Request id for read phone state
     */
    public static final int REQUEST_PHONE_CALL = 5;

    /**
     * End point ID
     */
    public static final String ID = "Id";


    /**
     * End point image
     */
    public static final String IMAGE = "Image";

    /**
     * Default date format
     */
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Default date range between start and end date
     */
    public static final int DEFAULT_DATE_RANGE = -3;

    /**
     * Call
     */
    public static final String TEL = "tel:";

    /**
     * End point language english
     */
    public static final String LOCALE_EN = "en";

    /**
     * End point language bangla
     */
//    public static final String LOCALE_BN = "bn";
    public static final String LOCALE_BN = "bn";

    public static final int LOCALE_EN_ID = 1;
    public static final int LOCALE_BN_ID = 2;

    public static final String USER = "user";
    public static final String BUNDLE_KEY_CATEGORY_ID = "bundle_key_category_id";
    public static final String BUNDLE_KEY_PREFERENCE_TYPE = "bundle_key_preference_type";
    public static final String BUNDLE_KEY_NEWS_BOTTOM_ADS = "bundle_key_news_bottom_ads";
    public static final String BUNDLE_KEY_CATEGORY_TITLE = "bundle_key_category_title";
    public static final String BUNDLE_KEY_NOTIFICATION_CONTENT_ID = "bundle_key_notification_content_id";
    public static final Long DEFAULT_CATEGORY_ID = -1l;
    public static final String BUNDLE_KEY_CONTENT_URL = "bundle_key_content_url";
    public static final String BUNDLE_KEY_SITE_URL = "bundle_key_site_url";
    public static final CharSequence SANDESH_360_COPY_TEXT = "sandesh360_copyToClipboard";
    public static final int RC_SIGN_IN = 111;
    public static final String BUNDLE_KEY_QUERY = "query";
    public static final String BUNDLE_KEY_CONTENT_LIST = "bundle_key_contlist";
    public static final String BUNDLE_KEY_FROM_PRIVACY = "bundle_key_from_privacy";
    public static final String BUNDLE_KEY_NOTIFICATION = "bundle_key_notifications";
    public static final String DATE_TIME_FORMAT = "dd-MM-yy hh:mm a";
    public static final String DATE_TIME_FORMAT_24 = "dd-MM-yy HH:mm";
    public static String REFERRAL_CODE = "referral_code";
}
