package com.sandesh360.sandesh.utils;

/**
 * Created by Mobarak on 27-Sep-17.
 * <p>
 * Updated by Moontasirul on 12-26-2017
 */

public class ErrorHandler {


//    /**
//     * Invoke to get error message besd on the status code and error code
//     *
//     * @param context
//     * @param statusCode
//     * @return
//     */
//    public static String getErrorMessage(Context context, int statusCode, String errorDescription) {
//        String errorMsg = "";
//        switch (statusCode) {
//            case 400:
//                errorMsg = context.getString(R.string.error_code_40001);
//                break;
//            case 401:
//                errorMsg = context.getString(R.string.error_code_40008);
//                break;
//            case 403:
//                errorMsg = context.getString(R.string.error_code_40003);
//                break;
//            case 404:
//                errorMsg = context.getString(R.string.error_code_40007);
//                break;
//            case 408:
//                errorMsg = context.getString(R.string.error_code_40003);
//                break;
//            case 500:
//                errorMsg = context.getString(R.string.error_code_40001);
//                break;
//            case 503:
//                errorMsg = context.getString(R.string.error_code_40002);
//                break;
//            default:
//                errorMsg = errorDescription;
//        }
//        return errorMsg;
//    }

//    /**
//     * Invoke to get error message besd on the error code
//     *
//     * @param context
//     * @param errorCode
//     * @return
//     */
//    public static String getErrorMessage(Context context, int statusCode, String errorCode, String errorDescription) {
//        String errorMsg = "";
//        switch (errorCode) {
//            case ErrorCode.ERROR_CODE_10001:
//                errorMsg = context.getString(R.string.error_code_10001);
//                break;
//            default:
//                errorMsg = errorDescription;
//        }
//        return errorMsg;
//    }



//    /**
//     * Invoke to showing dialog for showing error message
//     *
//     * @param context
//     * @param jsonString
//     */
//    public static void showDialog(final Context context, String jsonString) {
//
//        if (context == null) {
//            return;
//        }
//        final String errorMsg = getErrorMessage(context, jsonString);
//        Log.i("Error-MSG:", " " + errorMsg);
//
//        View dialogView = LayoutInflater.from(context).inflate(R.layout.success_pop_up, null);
//        TextView tvShowError = (TextView) dialogView.findViewById(R.id.tv_show_error);
//        TextView btnOk = (TextView) dialogView.findViewById(R.id.btn_error_ok);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        final Dialog errorDialog = builder.setView(dialogView).create();
//
//        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
//        params.copyFrom(errorDialog.getWindow().getAttributes());
//        params.width = WindowManager.LayoutParams.MATCH_PARENT;
//        params.height = WindowManager.LayoutParams.MATCH_PARENT;
//        tvShowError.setText(errorMsg);
//        errorDialog.setCancelable(false);
//        errorDialog.getWindow().setAttributes(params);
//        btnOk.setOnClickListener(v -> {
//            if (errorMsg.contains(context.getString(R.string.error_code_10007))
//                    || errorMsg.contains(context.getString(R.string.error_code_40002))
//                    || errorMsg.contains(context.getString(R.string.error_code_40014))) {
//                DataManager.getInstance().removeTokenAndInfo();
//                Fragment fragment = new LoginFragment();
//                UtilityFunctions.changeFragment((FragmentActivity) context, fragment, false, false);
//            }
//            errorDialog.dismiss();
//        });
//
//        errorDialog.show();
//
//    }
}
