// Copyright 2014 Google Inc. All Rights Reserved.

package com.sandesh360.sandesh.utils;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class DeveloperKey {

  /**
   * YouTube Data API v3 service. Go to the
   * <a href="https://console.developers.google.com/">Google Developers Console</a>
   * to register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AIzaSyBfNsqJVqjo25jBf4KiIgfEmadnteQnDsA";

}
