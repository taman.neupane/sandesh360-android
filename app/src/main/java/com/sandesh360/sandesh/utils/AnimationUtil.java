package com.sandesh360.sandesh.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;

/**
 * Created by Mobarak on November 03, 2018
 *
 * @author Sandesh360
 */
public class AnimationUtil {
    private static final int DEFAULT_DURATION = 200;

    // slide the view from below itself to the current position
    public static void slideUp(View view, Animation.AnimationListener animationListener) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0f,                 // fromXDelta
                0f,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0f);                // toYDelta
        animate.setDuration(DEFAULT_DURATION);
        animate.setAnimationListener(animationListener);
        animate.setFillAfter(false);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public static void slideDown(View view, Animation.AnimationListener animationListener) {
        TranslateAnimation animate = new TranslateAnimation(
                0f,                 // fromXDelta
                0f,                 // toXDelta
                0f,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(DEFAULT_DURATION);
        animate.setAnimationListener(animationListener);
        animate.setFillAfter(false);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }


    public static void appBarShow(View view) {
        view.animate()
                .translationY(0)
                .setInterpolator(new LinearInterpolator())
                .setDuration(DEFAULT_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        view.setVisibility(View.VISIBLE);
                    }
                });
    }

    public static void appBarHide(View view) {
        view.animate()
                .translationY(-view.getHeight())
                .setInterpolator(new LinearInterpolator())
                .setDuration(DEFAULT_DURATION)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        view.setElevation(0);
                        view.setVisibility(View.GONE);
                    }
                });
    }
}
